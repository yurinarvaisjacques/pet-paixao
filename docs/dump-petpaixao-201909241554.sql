-- MySQL dump 10.13  Distrib 5.5.62, for Win64 (AMD64)
--
-- Host: localhost    Database: petpaixao
-- ------------------------------------------------------
-- Server version	5.7.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `atendimentos`
--

DROP TABLE IF EXISTS `atendimentos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `atendimentos` (
  `ID_Atendimento` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `FK_ID_Servico` int(10) unsigned DEFAULT NULL,
  `fk_id_pet` int(10) NOT NULL,
  `horario_atendimento` varchar(100) NOT NULL,
  `observacao` varchar(100) DEFAULT NULL,
  `Data_Cadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID_Atendimento`),
  KEY `FK_Atendimentos_1` (`FK_ID_Servico`),
  CONSTRAINT `FK_Atendimentos_1` FOREIGN KEY (`FK_ID_Servico`) REFERENCES `servicos` (`ID_Servico`) ON DELETE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `atendimentos`
--

LOCK TABLES `atendimentos` WRITE;
/*!40000 ALTER TABLE `atendimentos` DISABLE KEYS */;
INSERT INTO `atendimentos` VALUES (1,3,1,'24-09-2019 - 08:30:00 - Terça-feira','','2019-09-23 14:37:04'),(2,3,2,'24-09-2019 - 08:30:00 - Terça-feira','','2019-09-23 14:51:12'),(3,3,2,'24-09-2019 - 15:00:00 - Terça-feira','','2019-09-23 17:42:07'),(4,3,3,'25-09-2019 - 08:30:00 - Quarta-feira','','2019-09-24 13:03:34'),(5,1,1,'25-09-2019 - 08:30:00 - Quarta-feira','','2019-09-24 13:39:14'),(6,2,3,'25-09-2019 - 08:30:00 - Quarta-feira','','2019-09-24 14:10:21'),(7,2,2,' 28-09-2019 - 08:30:00 - Sábado','','2019-09-24 14:21:55'),(8,3,2,'24-09-2019 - 10:30:00 - Terça-feira','','2019-09-23 14:51:12');
/*!40000 ALTER TABLE `atendimentos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `estoque`
--

DROP TABLE IF EXISTS `estoque`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estoque` (
  `ID_Estoque` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Quantidade` decimal(6,2) NOT NULL,
  `Data_Vencimento` date DEFAULT NULL,
  PRIMARY KEY (`ID_Estoque`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estoque`
--

LOCK TABLES `estoque` WRITE;
/*!40000 ALTER TABLE `estoque` DISABLE KEYS */;
/*!40000 ALTER TABLE `estoque` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `estoque_produtos`
--

DROP TABLE IF EXISTS `estoque_produtos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estoque_produtos` (
  `ID_Estoque_Produto` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `FK_ID_Produto` int(10) unsigned DEFAULT NULL,
  `FK_ID_Estoque` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`ID_Estoque_Produto`),
  KEY `FK_Estoque_Produtos_1` (`FK_ID_Produto`),
  KEY `FK_Estoque_Produtos_2` (`FK_ID_Estoque`),
  CONSTRAINT `FK_Estoque_Produtos_1` FOREIGN KEY (`FK_ID_Produto`) REFERENCES `produtos` (`ID_Produto`) ON DELETE NO ACTION,
  CONSTRAINT `FK_Estoque_Produtos_2` FOREIGN KEY (`FK_ID_Estoque`) REFERENCES `estoque` (`ID_Estoque`) ON DELETE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estoque_produtos`
--

LOCK TABLES `estoque_produtos` WRITE;
/*!40000 ALTER TABLE `estoque_produtos` DISABLE KEYS */;
/*!40000 ALTER TABLE `estoque_produtos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `feriados`
--

DROP TABLE IF EXISTS `feriados`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `feriados` (
  `ID_Feriado` int(11) NOT NULL AUTO_INCREMENT,
  `Data_Feriado` date NOT NULL,
  `Status` enum('A','I') NOT NULL DEFAULT 'A',
  `Data_Cadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID_Feriado`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `feriados`
--

LOCK TABLES `feriados` WRITE;
/*!40000 ALTER TABLE `feriados` DISABLE KEYS */;
INSERT INTO `feriados` VALUES (1,'2019-09-26','A','2019-09-23 17:48:36');
/*!40000 ALTER TABLE `feriados` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fornecedor_produtos`
--

DROP TABLE IF EXISTS `fornecedor_produtos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fornecedor_produtos` (
  `ID_Fornecedores_Produtos` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `FK_ID_Produto` int(10) unsigned DEFAULT NULL,
  `FK_ID_Fornecedor` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`ID_Fornecedores_Produtos`),
  KEY `FK_Fornecedor_Produtos_1` (`FK_ID_Produto`),
  KEY `FK_Fornecedor_Produtos_2` (`FK_ID_Fornecedor`),
  CONSTRAINT `FK_Fornecedor_Produtos_1` FOREIGN KEY (`FK_ID_Produto`) REFERENCES `produtos` (`ID_Produto`) ON DELETE NO ACTION,
  CONSTRAINT `FK_Fornecedor_Produtos_2` FOREIGN KEY (`FK_ID_Fornecedor`) REFERENCES `fornecedores` (`ID_Fornecedor`) ON DELETE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fornecedor_produtos`
--

LOCK TABLES `fornecedor_produtos` WRITE;
/*!40000 ALTER TABLE `fornecedor_produtos` DISABLE KEYS */;
/*!40000 ALTER TABLE `fornecedor_produtos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fornecedores`
--

DROP TABLE IF EXISTS `fornecedores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fornecedores` (
  `ID_Fornecedor` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `NomeFantasia` varchar(100) NOT NULL,
  `RazaoSocial` varchar(100) NOT NULL,
  `CNPJ` varchar(14) NOT NULL,
  `InscricaoEstadual` varchar(30) DEFAULT NULL,
  `Email` varchar(100) NOT NULL,
  `Rua` text NOT NULL,
  `Numero` varchar(20) NOT NULL,
  `Bairro` text NOT NULL,
  `Cidade` text NOT NULL,
  `CEP` char(11) NOT NULL,
  `Data_Cadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID_Fornecedor`),
  UNIQUE KEY `CNPJ` (`CNPJ`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fornecedores`
--

LOCK TABLES `fornecedores` WRITE;
/*!40000 ALTER TABLE `fornecedores` DISABLE KEYS */;
INSERT INTO `fornecedores` VALUES (1,'Atualizar Fornecedor','Fornecedores','0221552','1212','yuri@gmail.com','Marcilio dias','997464296','1','','1','2019-08-29 23:40:21'),(2,'Teste Yuri Fornecedor','Teste Teste','002001155','022','0@gmail.com','teste','021','0','0','95520000','2019-09-04 16:30:50'),(3,'Inserir Novo Fornecedor','Fornecedor Novo','0200125','','fornecedor@gmail.com','02','0','02','0','95520000','2019-09-06 17:06:32'),(4,'ad','ad','2000','0','0@gmail.com','0','0','0','0','0','2019-09-06 17:07:24');
/*!40000 ALTER TABLE `fornecedores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `horarios`
--

DROP TABLE IF EXISTS `horarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `horarios` (
  `ID_Horario` int(11) NOT NULL AUTO_INCREMENT,
  `Dia` varchar(3) NOT NULL,
  `Hora` varchar(8) NOT NULL,
  `Status` enum('A','I') NOT NULL DEFAULT 'A',
  `Data_Cadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID_Horario`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `horarios`
--

LOCK TABLES `horarios` WRITE;
/*!40000 ALTER TABLE `horarios` DISABLE KEYS */;
INSERT INTO `horarios` VALUES (1,'SEG','08:30:00','A','2019-09-23 12:03:43'),(2,'TER','08:30:00','A','2019-09-23 12:03:43'),(3,'QUA','08:30:00','A','2019-09-23 12:03:43'),(4,'QUI','08:30:00','A','2019-09-23 12:03:44'),(5,'SEX','08:30:00','A','2019-09-23 12:03:44'),(6,'SAB','08:30:00','A','2019-09-23 12:03:44'),(7,'SEG','09:00:00','A','2019-09-23 12:03:44'),(8,'SEG','10:00:00','A','2019-09-23 12:03:44'),(9,'SAB','09:00:00','A','2019-09-23 12:46:58'),(10,'SAB','10:00:00','A','2019-09-23 12:46:58'),(11,'SAB','11:00:00','A','2019-09-23 12:46:58'),(12,'SAB','12:00:00','A','2019-09-23 12:46:58'),(13,'SEG','13:30:00','A','2019-09-23 12:46:58'),(14,'TER','15:00:00','A','2019-09-23 17:39:46'),(15,'TER','16:00:00','A','2019-09-23 17:39:46');
/*!40000 ALTER TABLE `horarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `observacoes`
--

DROP TABLE IF EXISTS `observacoes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `observacoes` (
  `ID_Observacoes` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Observacoes` text NOT NULL,
  `FK_ID_Atendimento` int(10) unsigned DEFAULT NULL,
  `Data_Cadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID_Observacoes`),
  KEY `FK_Observacoes_1` (`FK_ID_Atendimento`),
  CONSTRAINT `FK_Observacoes_1` FOREIGN KEY (`FK_ID_Atendimento`) REFERENCES `atendimentos` (`ID_Atendimento`) ON DELETE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `observacoes`
--

LOCK TABLES `observacoes` WRITE;
/*!40000 ALTER TABLE `observacoes` DISABLE KEYS */;
/*!40000 ALTER TABLE `observacoes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `perfis`
--

DROP TABLE IF EXISTS `perfis`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `perfis` (
  `ID_Perfis` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Nome` varchar(100) NOT NULL,
  `Descricao` text,
  `Data_Cadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID_Perfis`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `perfis`
--

LOCK TABLES `perfis` WRITE;
/*!40000 ALTER TABLE `perfis` DISABLE KEYS */;
INSERT INTO `perfis` VALUES (1,'Administrador','Controle de todo Sistema','2019-08-22 14:47:19'),(2,'Funcionário','Controla parte do Sistema','2019-08-22 14:47:19'),(3,'Cliente','Sem acesso ao Sistema','2019-08-22 14:47:19');
/*!40000 ALTER TABLE `perfis` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `perfis_pessoas`
--

DROP TABLE IF EXISTS `perfis_pessoas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `perfis_pessoas` (
  `ID_Perfis_Pessoas` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `FK_ID_Pessoa` int(10) unsigned DEFAULT NULL,
  `FK_ID_Perfis` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`ID_Perfis_Pessoas`),
  KEY `FK_Perfis_Pessoas_1` (`FK_ID_Pessoa`),
  KEY `FK_Perfis_Pessoas_2` (`FK_ID_Perfis`),
  CONSTRAINT `FK_Perfis_Pessoas_1` FOREIGN KEY (`FK_ID_Pessoa`) REFERENCES `pessoas` (`ID_Pessoa`) ON DELETE NO ACTION,
  CONSTRAINT `FK_Perfis_Pessoas_2` FOREIGN KEY (`FK_ID_Perfis`) REFERENCES `perfis` (`ID_Perfis`) ON DELETE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `perfis_pessoas`
--

LOCK TABLES `perfis_pessoas` WRITE;
/*!40000 ALTER TABLE `perfis_pessoas` DISABLE KEYS */;
INSERT INTO `perfis_pessoas` VALUES (1,1,1),(2,16,3),(3,2,3),(4,4,3),(5,5,3),(6,6,3),(7,7,3),(8,8,3),(9,9,3),(10,10,3),(11,11,3),(12,14,3),(13,15,3),(14,17,3),(15,18,3),(16,19,3),(17,20,3),(18,21,3),(19,22,3),(20,24,3),(21,25,3),(22,26,3),(23,27,3),(24,28,3),(25,29,3);
/*!40000 ALTER TABLE `perfis_pessoas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pessoas`
--

DROP TABLE IF EXISTS `pessoas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pessoas` (
  `ID_Pessoa` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Nome` varchar(100) NOT NULL,
  `Login` varchar(20) DEFAULT NULL,
  `Password` text,
  `CPF` varchar(11) NOT NULL,
  `RG` text NOT NULL,
  `Email` varchar(100) NOT NULL,
  `DataNascimento` date DEFAULT NULL,
  `Rua` text NOT NULL,
  `Numero` varchar(20) NOT NULL,
  `Bairro` text NOT NULL,
  `Cidade` text NOT NULL,
  `CEP` char(11) NOT NULL,
  `Data_Cadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID_Pessoa`),
  UNIQUE KEY `CPF` (`CPF`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pessoas`
--

LOCK TABLES `pessoas` WRITE;
/*!40000 ALTER TABLE `pessoas` DISABLE KEYS */;
INSERT INTO `pessoas` VALUES (1,'Yuri Rafael Narvais Jacques','Jacques','123456','02646179081','2098506121','yurinarvaisjacques@gmil.com','1991-12-02','Marcelino de Freitas','364','Pitangas','Osório','95520000','2019-08-22 14:48:53'),(2,'Alteração Table',NULL,NULL,'2','1','yuri@gmail.com','0000-00-00','0','0','0','0','95520000','2019-08-27 20:45:52'),(4,'teste dois',NULL,NULL,'2333','2','salvabd@gmail.com','1991-02-12','1','1','1','1','1','2019-08-27 20:54:01'),(5,'Teste tres',NULL,NULL,'46500','2','yuri@gmail.com','1991-02-12','0','0','0','0','0','2019-08-27 21:05:38'),(6,'Teste Quatro',NULL,NULL,'4','4','yuri@gmail.com','1991-02-12','2','2','2','2','2','2019-08-27 21:11:28'),(7,'teste cinco',NULL,NULL,'111','1','1@gmail.com','1991-12-02','2','2','2','2','2','2019-08-27 21:13:08'),(8,'Teste seis',NULL,NULL,'12552','02111','yuri@gmail.com','2199-01-21','0','0','0','0','0','2019-08-27 21:14:04'),(9,'teste seis',NULL,NULL,'06','06','yuri@gmail.com','1991-12-02','02','02','02','02','02','2019-08-27 21:22:48'),(10,'teste sete',NULL,NULL,'07','07','yuri@gmail.com','1991-12-02','07','07','070','07','07','2019-08-27 21:23:58'),(11,'Teste Celular',NULL,NULL,'1','1','yuri@gmail.com','1991-01-01','1','1','1','1','1','2019-08-28 02:49:50'),(14,'cliente novo celular',NULL,NULL,'1255','1','yuri@gmail.com','0000-00-00','0','0','0','0','0','2019-08-28 02:53:14'),(15,'Yuri teste 10',NULL,NULL,'1111111','1','y@gmail.com','0000-00-00','a','2','2','2','2','2019-08-28 11:29:57'),(16,'Teste do Perfil',NULL,NULL,'02202202','02','y@gmail.com','1991-01-01','01','01','01','01','01','2019-08-28 14:31:20'),(17,'Y',NULL,NULL,'2552525','1','a@da.com','1991-02-12','a','a','a','a','a','2019-08-28 21:26:04'),(18,'Teste re',NULL,NULL,'025236','0212','yuri@gmail.com','0000-00-00','2','2','2','2','2','2019-08-28 21:32:31'),(19,'tyhh',NULL,NULL,'0255888','02','y@g.com','0000-00-00','0','0','0','0','0','2019-08-28 21:33:08'),(20,'sa',NULL,NULL,'12344','1','yuri@g.com','0000-00-00','0','0','0','0','0','2019-08-28 21:35:34'),(21,'as',NULL,NULL,'02505050','02','0@g.com','0000-00-00','02','02','02','02','02','2019-08-28 21:37:55'),(22,'mom',NULL,NULL,'02','02','y@g.com','0000-00-00','03','03','03','03','03','2019-08-28 21:38:57'),(24,'aaaa',NULL,NULL,'110','111','a@a.aco','2019-08-13','aa','aa','a','a','aa','2019-08-28 21:45:46'),(25,'aasa',NULL,NULL,'15151','15151a','A@d.ds','2019-08-05','aa','a','ll','kkk','77','2019-08-28 21:47:02'),(26,'ffff',NULL,NULL,'54218','5858','A@FF.MM','2019-01-06','dfkfdkg','7','4çç','rewr','88','2019-08-28 21:48:51'),(27,'teste Yuri',NULL,NULL,'000000','02002','0@gmail.com','0000-00-00','0','0','0','0','0','2019-09-04 01:12:12'),(28,'test',NULL,NULL,'002220002','002','0@gmail.com','1991-12-02','0','0','0','0','0','2019-09-04 17:10:10'),(29,'yuri',NULL,NULL,'0200000','2000','y@gmail.com','1991-01-12','0','0','0','0','0','2019-09-23 14:50:07');
/*!40000 ALTER TABLE `pessoas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pets`
--

DROP TABLE IF EXISTS `pets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pets` (
  `ID_Pet` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Nome` varchar(100) NOT NULL,
  `Porte` enum('P','M','G') NOT NULL DEFAULT 'M',
  `Especie` text NOT NULL,
  `Idade` int(11) DEFAULT NULL,
  `Pelagem` enum('C','M','L') NOT NULL DEFAULT 'M',
  `Cor` varchar(50) DEFAULT NULL,
  `Castrado` enum('S','N') NOT NULL DEFAULT 'N',
  `Sexo` enum('M','F','I') NOT NULL DEFAULT 'I',
  `Data_Cadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID_Pet`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pets`
--

LOCK TABLES `pets` WRITE;
/*!40000 ALTER TABLE `pets` DISABLE KEYS */;
INSERT INTO `pets` VALUES (1,'Teste Pet','P','Atualizar BD',1,'C','we','S','F','2019-09-08 18:31:44'),(2,'teste','P','dog',2,'C','cinza','N','M','2019-09-23 14:50:49'),(3,'teste teste','P','25',1,'C','2','S','F','2019-09-24 13:03:12');
/*!40000 ALTER TABLE `pets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pets_pessoas`
--

DROP TABLE IF EXISTS `pets_pessoas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pets_pessoas` (
  `ID_Pets_Pessoas` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `FK_ID_Pet` int(10) unsigned DEFAULT NULL,
  `FK_ID_Pessoa` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`ID_Pets_Pessoas`),
  KEY `FK_Pets_Pessoas_1` (`FK_ID_Pet`),
  KEY `FK_Pets_Pessoas_2` (`FK_ID_Pessoa`),
  CONSTRAINT `FK_Pets_Pessoas_1` FOREIGN KEY (`FK_ID_Pet`) REFERENCES `pets` (`ID_Pet`) ON DELETE NO ACTION,
  CONSTRAINT `FK_Pets_Pessoas_2` FOREIGN KEY (`FK_ID_Pessoa`) REFERENCES `pessoas` (`ID_Pessoa`) ON DELETE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pets_pessoas`
--

LOCK TABLES `pets_pessoas` WRITE;
/*!40000 ALTER TABLE `pets_pessoas` DISABLE KEYS */;
INSERT INTO `pets_pessoas` VALUES (1,1,4),(2,2,29),(3,3,16);
/*!40000 ALTER TABLE `pets_pessoas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `produtos`
--

DROP TABLE IF EXISTS `produtos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `produtos` (
  `ID_Produto` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Nome_Completo` varchar(100) NOT NULL,
  `Nome_Abreviado` varchar(50) NOT NULL,
  `Descricao` text NOT NULL,
  `Tipo` text NOT NULL,
  `Categoria` text NOT NULL,
  `Medida` enum('KG','M','L','UNI') DEFAULT NULL,
  `Preco` decimal(6,2) NOT NULL,
  `Valor_Venda` decimal(6,2) NOT NULL,
  `Data_Cadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID_Produto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `produtos`
--

LOCK TABLES `produtos` WRITE;
/*!40000 ALTER TABLE `produtos` DISABLE KEYS */;
/*!40000 ALTER TABLE `produtos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `representante`
--

DROP TABLE IF EXISTS `representante`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `representante` (
  `ID_Representante` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `FK_ID_Fornecedor` int(10) unsigned DEFAULT NULL,
  `Representante` varchar(100) DEFAULT NULL,
  `Telefone` char(12) DEFAULT NULL,
  `Email` varchar(100) DEFAULT NULL,
  `Data_Cadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID_Representante`),
  KEY `FK_Representante_1` (`FK_ID_Fornecedor`),
  CONSTRAINT `FK_Representante_1` FOREIGN KEY (`FK_ID_Fornecedor`) REFERENCES `fornecedores` (`ID_Fornecedor`) ON DELETE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `representante`
--

LOCK TABLES `representante` WRITE;
/*!40000 ALTER TABLE `representante` DISABLE KEYS */;
INSERT INTO `representante` VALUES (1,1,'Teste Representante','0519997585','yuri@gmail.com','2019-08-29 23:40:21'),(2,2,'Yuri Teste','051997464296','yuri@gmail.com','2019-09-04 16:30:50');
/*!40000 ALTER TABLE `representante` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `servicos`
--

DROP TABLE IF EXISTS `servicos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `servicos` (
  `ID_Servico` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Nome` varchar(50) NOT NULL,
  `Descricao` text NOT NULL,
  `Valor` decimal(6,2) NOT NULL,
  `Tipo_Servico` enum('C','E','V') NOT NULL,
  `Data_Cadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID_Servico`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `servicos`
--

LOCK TABLES `servicos` WRITE;
/*!40000 ALTER TABLE `servicos` DISABLE KEYS */;
INSERT INTO `servicos` VALUES (1,'Banho Pequeno Porte','Lavar , Secar , Cortar Unhas e colocar perfume',30.00,'E','2019-09-19 17:04:52'),(2,'Vacina Rabica','Vacinação contra raiva',30.00,'V','2019-09-19 17:24:03'),(3,'Castração de Gato Macho','Castração de Gato Macho',280.00,'C','2019-09-23 13:46:29'),(4,'Banho Gato','Banho Gatos',60.00,'E','2019-09-24 13:36:54');
/*!40000 ALTER TABLE `servicos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `telefone_fornecedor`
--

DROP TABLE IF EXISTS `telefone_fornecedor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `telefone_fornecedor` (
  `ID_Telefone` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Telefone` char(11) NOT NULL,
  `FK_ID_Fornecedor` int(10) unsigned DEFAULT NULL,
  `Data_Cadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID_Telefone`),
  KEY `FK_Telefone_Fornecedor_1` (`FK_ID_Fornecedor`),
  CONSTRAINT `FK_Telefone_Fornecedor_1` FOREIGN KEY (`FK_ID_Fornecedor`) REFERENCES `fornecedores` (`ID_Fornecedor`) ON DELETE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `telefone_fornecedor`
--

LOCK TABLES `telefone_fornecedor` WRITE;
/*!40000 ALTER TABLE `telefone_fornecedor` DISABLE KEYS */;
INSERT INTO `telefone_fornecedor` VALUES (1,'1',1,'2019-08-29 23:40:21'),(2,'2',1,'2019-08-29 23:40:21'),(3,'3',1,'2019-08-29 23:40:21'),(4,'4',1,'2019-08-29 23:40:21'),(5,'0',2,'2019-09-04 16:30:50'),(6,'02',3,'2019-09-06 17:06:32'),(7,'0',4,'2019-09-06 17:07:24');
/*!40000 ALTER TABLE `telefone_fornecedor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `telefone_pessoa`
--

DROP TABLE IF EXISTS `telefone_pessoa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `telefone_pessoa` (
  `ID_Telefone` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Telefone` char(12) NOT NULL,
  `FK_ID_Pessoa` int(10) unsigned DEFAULT NULL,
  `Data_Cadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID_Telefone`),
  KEY `FK_Telefone_Pessoa_1` (`FK_ID_Pessoa`),
  CONSTRAINT `FK_Telefone_Pessoa_1` FOREIGN KEY (`FK_ID_Pessoa`) REFERENCES `pessoas` (`ID_Pessoa`) ON DELETE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `telefone_pessoa`
--

LOCK TABLES `telefone_pessoa` WRITE;
/*!40000 ALTER TABLE `telefone_pessoa` DISABLE KEYS */;
INSERT INTO `telefone_pessoa` VALUES (1,'051997464296',11,'2019-08-28 02:49:50'),(2,'051997464296',14,'2019-08-28 02:53:14'),(3,'01',15,'2019-08-28 11:29:58'),(4,'02',15,'2019-08-28 11:29:58'),(5,'03',15,'2019-08-28 11:29:58'),(6,'01',16,'2019-08-28 14:31:20'),(7,'01',16,'2019-08-28 14:31:20'),(8,'01',16,'2019-08-28 14:31:20'),(9,'04',16,'2019-08-28 14:31:20'),(10,'a',17,'2019-08-28 21:26:04'),(11,'2',18,'2019-08-28 21:32:31'),(12,'0',19,'2019-08-28 21:33:08'),(13,'0',20,'2019-08-28 21:35:34'),(14,'02',21,'2019-08-28 21:37:55'),(15,'03',22,'2019-08-28 21:38:57'),(16,'77',24,'2019-08-28 21:45:46'),(17,'1818',25,'2019-08-28 21:47:02'),(18,'--7',26,'2019-08-28 21:48:51'),(19,'0',27,'2019-09-04 01:12:12'),(20,'0',28,'2019-09-04 17:10:10'),(21,'0',29,'2019-09-23 14:50:07');
/*!40000 ALTER TABLE `telefone_pessoa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vacinas`
--

DROP TABLE IF EXISTS `vacinas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vacinas` (
  `ID_Vacinas` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `FK_ID_Pet` int(10) unsigned DEFAULT NULL,
  `Vacina` varchar(50) NOT NULL,
  `Tipo` varchar(50) NOT NULL,
  `Proxima_Vacina` date NOT NULL,
  `Data_Aplicacao` date NOT NULL,
  `Data_Cadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID_Vacinas`),
  KEY `FK_Vacinas_1` (`FK_ID_Pet`),
  CONSTRAINT `FK_Vacinas_1` FOREIGN KEY (`FK_ID_Pet`) REFERENCES `pets` (`ID_Pet`) ON DELETE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vacinas`
--

LOCK TABLES `vacinas` WRITE;
/*!40000 ALTER TABLE `vacinas` DISABLE KEYS */;
/*!40000 ALTER TABLE `vacinas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vendas`
--

DROP TABLE IF EXISTS `vendas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vendas` (
  `ID_Venda` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `FK_ID_Pessoa` int(10) unsigned DEFAULT NULL,
  `FK_ID_Estoque` int(10) unsigned DEFAULT NULL,
  `FK_ID_Pet` int(10) unsigned DEFAULT NULL,
  `FK_ID_Atendimento` int(10) unsigned DEFAULT NULL,
  `Data_Cadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID_Venda`),
  KEY `FK_Vendas_1` (`FK_ID_Pessoa`),
  KEY `FK_Vendas_2` (`FK_ID_Estoque`),
  KEY `FK_Vendas_3` (`FK_ID_Pet`),
  KEY `FK_Vendas_4` (`FK_ID_Atendimento`),
  CONSTRAINT `FK_Vendas_1` FOREIGN KEY (`FK_ID_Pessoa`) REFERENCES `pessoas` (`ID_Pessoa`) ON DELETE NO ACTION,
  CONSTRAINT `FK_Vendas_2` FOREIGN KEY (`FK_ID_Estoque`) REFERENCES `estoque` (`ID_Estoque`) ON DELETE NO ACTION,
  CONSTRAINT `FK_Vendas_3` FOREIGN KEY (`FK_ID_Pet`) REFERENCES `pets` (`ID_Pet`) ON DELETE NO ACTION,
  CONSTRAINT `FK_Vendas_4` FOREIGN KEY (`FK_ID_Atendimento`) REFERENCES `atendimentos` (`ID_Atendimento`) ON DELETE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vendas`
--

LOCK TABLES `vendas` WRITE;
/*!40000 ALTER TABLE `vendas` DISABLE KEYS */;
/*!40000 ALTER TABLE `vendas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'petpaixao'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-09-24 15:54:15
