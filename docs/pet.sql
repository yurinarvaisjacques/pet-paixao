-- MySQL dump 10.13  Distrib 5.5.62, for Win64 (AMD64)
--
-- Host: localhost    Database: petpaixao
-- ------------------------------------------------------
-- Server version	5.7.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `atendimentos`
--

DROP TABLE IF EXISTS `atendimentos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `atendimentos` (
  `ID_Atendimento` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `FK_ID_Servico` int(10) unsigned DEFAULT NULL,
  `fk_id_pet` int(10) NOT NULL,
  `horario_atendimento` varchar(100) NOT NULL,
  `observacao` varchar(100) DEFAULT NULL,
  `atendido` enum('S','N') NOT NULL DEFAULT 'N',
  `ativo` enum('S','N') NOT NULL DEFAULT 'S',
  `Data_Cadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID_Atendimento`),
  KEY `FK_Atendimentos_1` (`FK_ID_Servico`),
  CONSTRAINT `FK_Atendimentos_1` FOREIGN KEY (`FK_ID_Servico`) REFERENCES `servicos` (`ID_Servico`) ON DELETE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `estoque`
--

DROP TABLE IF EXISTS `estoque`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estoque` (
  `ID_Estoque` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Quantidade` decimal(6,2) NOT NULL,
  `Data_Vencimento` date DEFAULT NULL,
  PRIMARY KEY (`ID_Estoque`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `estoque_produtos`
--

DROP TABLE IF EXISTS `estoque_produtos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estoque_produtos` (
  `ID_Estoque_Produto` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `FK_ID_Produto` int(10) unsigned DEFAULT NULL,
  `FK_ID_Estoque` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`ID_Estoque_Produto`),
  KEY `FK_Estoque_Produtos_1` (`FK_ID_Produto`),
  KEY `FK_Estoque_Produtos_2` (`FK_ID_Estoque`),
  CONSTRAINT `FK_Estoque_Produtos_1` FOREIGN KEY (`FK_ID_Produto`) REFERENCES `produtos` (`ID_Produto`) ON DELETE NO ACTION,
  CONSTRAINT `FK_Estoque_Produtos_2` FOREIGN KEY (`FK_ID_Estoque`) REFERENCES `estoque` (`ID_Estoque`) ON DELETE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `feriados`
--

DROP TABLE IF EXISTS `feriados`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `feriados` (
  `ID_Feriado` int(11) NOT NULL AUTO_INCREMENT,
  `Data_Feriado` date NOT NULL,
  `Status` enum('A','I') NOT NULL DEFAULT 'A',
  `Data_Cadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID_Feriado`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fornecedor_produtos`
--

DROP TABLE IF EXISTS `fornecedor_produtos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fornecedor_produtos` (
  `ID_Fornecedores_Produtos` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `FK_ID_Produto` int(10) unsigned DEFAULT NULL,
  `FK_ID_Fornecedor` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`ID_Fornecedores_Produtos`),
  KEY `FK_Fornecedor_Produtos_1` (`FK_ID_Produto`),
  KEY `FK_Fornecedor_Produtos_2` (`FK_ID_Fornecedor`),
  CONSTRAINT `FK_Fornecedor_Produtos_1` FOREIGN KEY (`FK_ID_Produto`) REFERENCES `produtos` (`ID_Produto`) ON DELETE NO ACTION,
  CONSTRAINT `FK_Fornecedor_Produtos_2` FOREIGN KEY (`FK_ID_Fornecedor`) REFERENCES `fornecedores` (`ID_Fornecedor`) ON DELETE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fornecedores`
--

DROP TABLE IF EXISTS `fornecedores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fornecedores` (
  `ID_Fornecedor` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `NomeFantasia` varchar(100) NOT NULL,
  `RazaoSocial` varchar(100) NOT NULL,
  `CNPJ` varchar(14) NOT NULL,
  `InscricaoEstadual` varchar(30) DEFAULT NULL,
  `Email` varchar(100) NOT NULL,
  `Rua` text NOT NULL,
  `Numero` varchar(20) NOT NULL,
  `Bairro` text NOT NULL,
  `Cidade` text NOT NULL,
  `CEP` char(11) NOT NULL,
  `Data_Cadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID_Fornecedor`),
  UNIQUE KEY `CNPJ` (`CNPJ`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `horarios`
--

DROP TABLE IF EXISTS `horarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `horarios` (
  `ID_Horario` int(11) NOT NULL AUTO_INCREMENT,
  `Dia` varchar(3) NOT NULL,
  `Hora` varchar(8) NOT NULL,
  `Status` enum('A','I') NOT NULL DEFAULT 'A',
  `Data_Cadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID_Horario`)
) ENGINE=InnoDB AUTO_INCREMENT=3679 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `observacoes`
--

DROP TABLE IF EXISTS `observacoes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `observacoes` (
  `ID_Observacoes` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Observacoes` text NOT NULL,
  `FK_ID_Atendimento` int(10) unsigned DEFAULT NULL,
  `Data_Cadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID_Observacoes`),
  KEY `FK_Observacoes_1` (`FK_ID_Atendimento`),
  CONSTRAINT `FK_Observacoes_1` FOREIGN KEY (`FK_ID_Atendimento`) REFERENCES `atendimentos` (`ID_Atendimento`) ON DELETE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `perfis`
--

DROP TABLE IF EXISTS `perfis`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `perfis` (
  `ID_Perfis` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Nome` varchar(100) NOT NULL,
  `Descricao` text,
  `Data_Cadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID_Perfis`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `perfis_pessoas`
--

DROP TABLE IF EXISTS `perfis_pessoas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `perfis_pessoas` (
  `ID_Perfis_Pessoas` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `FK_ID_Pessoa` int(10) unsigned DEFAULT NULL,
  `FK_ID_Perfis` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`ID_Perfis_Pessoas`),
  KEY `FK_Perfis_Pessoas_1` (`FK_ID_Pessoa`),
  KEY `FK_Perfis_Pessoas_2` (`FK_ID_Perfis`),
  CONSTRAINT `FK_Perfis_Pessoas_1` FOREIGN KEY (`FK_ID_Pessoa`) REFERENCES `pessoas` (`ID_Pessoa`) ON DELETE NO ACTION,
  CONSTRAINT `FK_Perfis_Pessoas_2` FOREIGN KEY (`FK_ID_Perfis`) REFERENCES `perfis` (`ID_Perfis`) ON DELETE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pessoas`
--

DROP TABLE IF EXISTS `pessoas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pessoas` (
  `ID_Pessoa` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Nome` varchar(100) NOT NULL,
  `Login` varchar(20) DEFAULT NULL,
  `Password` text,
  `CPF` varchar(11) NOT NULL,
  `RG` text NOT NULL,
  `Email` varchar(100) NOT NULL,
  `DataNascimento` date DEFAULT NULL,
  `Rua` text NOT NULL,
  `Numero` varchar(20) NOT NULL,
  `Bairro` text NOT NULL,
  `Cidade` text NOT NULL,
  `CEP` char(11) NOT NULL,
  `Data_Cadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID_Pessoa`),
  UNIQUE KEY `CPF` (`CPF`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pets`
--

DROP TABLE IF EXISTS `pets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pets` (
  `ID_Pet` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Nome` varchar(100) NOT NULL,
  `Porte` enum('P','M','G') NOT NULL DEFAULT 'M',
  `Especie` text NOT NULL,
  `Idade` int(11) DEFAULT NULL,
  `Pelagem` enum('C','M','L') NOT NULL DEFAULT 'M',
  `Cor` varchar(50) DEFAULT NULL,
  `Castrado` enum('S','N') NOT NULL DEFAULT 'N',
  `Sexo` enum('M','F','I') NOT NULL DEFAULT 'I',
  `Data_Cadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID_Pet`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pets_pessoas`
--

DROP TABLE IF EXISTS `pets_pessoas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pets_pessoas` (
  `ID_Pets_Pessoas` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `FK_ID_Pet` int(10) unsigned DEFAULT NULL,
  `FK_ID_Pessoa` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`ID_Pets_Pessoas`),
  KEY `FK_Pets_Pessoas_1` (`FK_ID_Pet`),
  KEY `FK_Pets_Pessoas_2` (`FK_ID_Pessoa`),
  CONSTRAINT `FK_Pets_Pessoas_1` FOREIGN KEY (`FK_ID_Pet`) REFERENCES `pets` (`ID_Pet`) ON DELETE NO ACTION,
  CONSTRAINT `FK_Pets_Pessoas_2` FOREIGN KEY (`FK_ID_Pessoa`) REFERENCES `pessoas` (`ID_Pessoa`) ON DELETE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `produtos`
--

DROP TABLE IF EXISTS `produtos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `produtos` (
  `ID_Produto` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Nome_Completo` varchar(100) NOT NULL,
  `Nome_Abreviado` varchar(50) NOT NULL,
  `Descricao` text NOT NULL,
  `Tipo` text NOT NULL,
  `Categoria` text NOT NULL,
  `Medida` enum('KG','M','L','UNI') DEFAULT NULL,
  `Preco` decimal(6,2) NOT NULL,
  `Valor_Venda` decimal(6,2) NOT NULL,
  `Data_Cadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID_Produto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `representante`
--

DROP TABLE IF EXISTS `representante`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `representante` (
  `ID_Representante` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `FK_ID_Fornecedor` int(10) unsigned DEFAULT NULL,
  `Representante` varchar(100) DEFAULT NULL,
  `Telefone` char(12) DEFAULT NULL,
  `Email` varchar(100) DEFAULT NULL,
  `Data_Cadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID_Representante`),
  KEY `FK_Representante_1` (`FK_ID_Fornecedor`),
  CONSTRAINT `FK_Representante_1` FOREIGN KEY (`FK_ID_Fornecedor`) REFERENCES `fornecedores` (`ID_Fornecedor`) ON DELETE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `servicos`
--

DROP TABLE IF EXISTS `servicos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `servicos` (
  `ID_Servico` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Nome` varchar(50) NOT NULL,
  `Descricao` text NOT NULL,
  `Valor` decimal(6,2) NOT NULL,
  `Tipo_Servico` enum('C','E','V') NOT NULL,
  `Data_Cadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID_Servico`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `telefone_fornecedor`
--

DROP TABLE IF EXISTS `telefone_fornecedor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `telefone_fornecedor` (
  `ID_Telefone` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Telefone` char(11) NOT NULL,
  `FK_ID_Fornecedor` int(10) unsigned DEFAULT NULL,
  `Data_Cadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID_Telefone`),
  KEY `FK_Telefone_Fornecedor_1` (`FK_ID_Fornecedor`),
  CONSTRAINT `FK_Telefone_Fornecedor_1` FOREIGN KEY (`FK_ID_Fornecedor`) REFERENCES `fornecedores` (`ID_Fornecedor`) ON DELETE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `telefone_pessoa`
--

DROP TABLE IF EXISTS `telefone_pessoa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `telefone_pessoa` (
  `ID_Telefone` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Telefone` char(12) NOT NULL,
  `FK_ID_Pessoa` int(10) unsigned DEFAULT NULL,
  `Data_Cadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID_Telefone`),
  KEY `FK_Telefone_Pessoa_1` (`FK_ID_Pessoa`),
  CONSTRAINT `FK_Telefone_Pessoa_1` FOREIGN KEY (`FK_ID_Pessoa`) REFERENCES `pessoas` (`ID_Pessoa`) ON DELETE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vacinas`
--

DROP TABLE IF EXISTS `vacinas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vacinas` (
  `ID_Vacinas` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `FK_ID_Pet` int(10) unsigned DEFAULT NULL,
  `Vacina` varchar(50) NOT NULL,
  `Proxima_Vacina` date NOT NULL,
  `Data_Aplicacao` date NOT NULL,
  `Data_Cadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID_Vacinas`),
  KEY `FK_Vacinas_1` (`FK_ID_Pet`),
  CONSTRAINT `FK_Vacinas_1` FOREIGN KEY (`FK_ID_Pet`) REFERENCES `pets` (`ID_Pet`) ON DELETE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vendas`
--

DROP TABLE IF EXISTS `vendas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vendas` (
  `ID_Venda` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `FK_ID_Pessoa` int(10) unsigned DEFAULT NULL,
  `FK_ID_Estoque` int(10) unsigned DEFAULT NULL,
  `FK_ID_Pet` int(10) unsigned DEFAULT NULL,
  `FK_ID_Atendimento` int(10) unsigned DEFAULT NULL,
  `Data_Cadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID_Venda`),
  KEY `FK_Vendas_1` (`FK_ID_Pessoa`),
  KEY `FK_Vendas_2` (`FK_ID_Estoque`),
  KEY `FK_Vendas_3` (`FK_ID_Pet`),
  KEY `FK_Vendas_4` (`FK_ID_Atendimento`),
  CONSTRAINT `FK_Vendas_1` FOREIGN KEY (`FK_ID_Pessoa`) REFERENCES `pessoas` (`ID_Pessoa`) ON DELETE NO ACTION,
  CONSTRAINT `FK_Vendas_2` FOREIGN KEY (`FK_ID_Estoque`) REFERENCES `estoque` (`ID_Estoque`) ON DELETE NO ACTION,
  CONSTRAINT `FK_Vendas_3` FOREIGN KEY (`FK_ID_Pet`) REFERENCES `pets` (`ID_Pet`) ON DELETE NO ACTION,
  CONSTRAINT `FK_Vendas_4` FOREIGN KEY (`FK_ID_Atendimento`) REFERENCES `atendimentos` (`ID_Atendimento`) ON DELETE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping routines for database 'petpaixao'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-10-05  9:28:53
