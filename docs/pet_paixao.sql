-- MySQL dump 10.13  Distrib 5.7.27, for Linux (x86_64)
--
-- Host: localhost    Database: petpaixao
-- ------------------------------------------------------
-- Server version	5.7.27-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `atendimentos`
--

DROP TABLE IF EXISTS `atendimentos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `atendimentos` (
  `ID_Atendimento` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `FK_ID_Servico` int(10) unsigned DEFAULT NULL,
  `fk_id_pet` int(10) NOT NULL,
  `horario_atendimento` varchar(100) NOT NULL,
  `observacao` varchar(100) DEFAULT NULL,
  `atendido` enum('S','N') NOT NULL DEFAULT 'N',
  `ativo` enum('S','N') NOT NULL DEFAULT 'S',
  `Data_Cadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID_Atendimento`),
  KEY `FK_Atendimentos_1` (`FK_ID_Servico`),
  CONSTRAINT `FK_Atendimentos_1` FOREIGN KEY (`FK_ID_Servico`) REFERENCES `servicos` (`ID_Servico`) ON DELETE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `atendimentos`
--

LOCK TABLES `atendimentos` WRITE;
/*!40000 ALTER TABLE `atendimentos` DISABLE KEYS */;
INSERT INTO `atendimentos` VALUES (5,2,1,'14-10-2019 - 10:00:00 - Segunda-feira','teste um','S','S','2019-10-14 01:18:35'),(6,1,2,'Segunda-feira - 14-10-2019 - 10:00:00','teste banho','S','S','2019-10-14 01:18:57'),(7,4,4,'Quarta-feira - 16-10-2019 - 10:00:00','vacina','N','S','2019-10-14 01:19:19'),(8,3,4,'16-10-2019 - 14:00:00 - Quarta-feira','','S','S','2019-10-14 01:38:51'),(9,1,2,'Quinta-feira - 17-10-2019 - 08:00:00','','N','S','2019-10-14 16:27:34'),(10,1,2,'Quarta-feira - 16-10-2019 - 08:00:00','','N','S','2019-10-14 20:52:19'),(11,3,1,'19-11-2019 - 14:00:00 - Terça-feira','','N','S','2019-11-15 13:41:56');
/*!40000 ALTER TABLE `atendimentos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `estoque`
--

DROP TABLE IF EXISTS `estoque`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estoque` (
  `ID_Estoque` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Quantidade` decimal(6,2) NOT NULL,
  `Data_Vencimento` date DEFAULT NULL,
  PRIMARY KEY (`ID_Estoque`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estoque`
--

LOCK TABLES `estoque` WRITE;
/*!40000 ALTER TABLE `estoque` DISABLE KEYS */;
/*!40000 ALTER TABLE `estoque` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `estoque_produtos`
--

DROP TABLE IF EXISTS `estoque_produtos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estoque_produtos` (
  `ID_Estoque_Produto` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `FK_ID_Produto` int(10) unsigned DEFAULT NULL,
  `FK_ID_Estoque` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`ID_Estoque_Produto`),
  KEY `FK_Estoque_Produtos_1` (`FK_ID_Produto`),
  KEY `FK_Estoque_Produtos_2` (`FK_ID_Estoque`),
  CONSTRAINT `FK_Estoque_Produtos_1` FOREIGN KEY (`FK_ID_Produto`) REFERENCES `produtos` (`ID_Produto`) ON DELETE NO ACTION,
  CONSTRAINT `FK_Estoque_Produtos_2` FOREIGN KEY (`FK_ID_Estoque`) REFERENCES `estoque` (`ID_Estoque`) ON DELETE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estoque_produtos`
--

LOCK TABLES `estoque_produtos` WRITE;
/*!40000 ALTER TABLE `estoque_produtos` DISABLE KEYS */;
/*!40000 ALTER TABLE `estoque_produtos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `feriados`
--

DROP TABLE IF EXISTS `feriados`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `feriados` (
  `ID_Feriado` int(11) NOT NULL AUTO_INCREMENT,
  `Data_Feriado` date NOT NULL,
  `Status` enum('A','I') NOT NULL DEFAULT 'A',
  `Data_Cadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID_Feriado`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `feriados`
--

LOCK TABLES `feriados` WRITE;
/*!40000 ALTER TABLE `feriados` DISABLE KEYS */;
/*!40000 ALTER TABLE `feriados` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fornecedor_produtos`
--

DROP TABLE IF EXISTS `fornecedor_produtos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fornecedor_produtos` (
  `ID_Fornecedores_Produtos` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `FK_ID_Produto` int(10) unsigned DEFAULT NULL,
  `FK_ID_Fornecedor` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`ID_Fornecedores_Produtos`),
  KEY `FK_Fornecedor_Produtos_1` (`FK_ID_Produto`),
  KEY `FK_Fornecedor_Produtos_2` (`FK_ID_Fornecedor`),
  CONSTRAINT `FK_Fornecedor_Produtos_1` FOREIGN KEY (`FK_ID_Produto`) REFERENCES `produtos` (`ID_Produto`) ON DELETE NO ACTION,
  CONSTRAINT `FK_Fornecedor_Produtos_2` FOREIGN KEY (`FK_ID_Fornecedor`) REFERENCES `fornecedores` (`ID_Fornecedor`) ON DELETE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fornecedor_produtos`
--

LOCK TABLES `fornecedor_produtos` WRITE;
/*!40000 ALTER TABLE `fornecedor_produtos` DISABLE KEYS */;
/*!40000 ALTER TABLE `fornecedor_produtos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fornecedores`
--

DROP TABLE IF EXISTS `fornecedores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fornecedores` (
  `ID_Fornecedor` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `NomeFantasia` varchar(100) NOT NULL,
  `RazaoSocial` varchar(100) NOT NULL,
  `CNPJ` varchar(14) NOT NULL,
  `InscricaoEstadual` varchar(30) DEFAULT NULL,
  `Email` varchar(100) NOT NULL,
  `Rua` text NOT NULL,
  `Numero` varchar(20) NOT NULL,
  `Bairro` text NOT NULL,
  `Cidade` text NOT NULL,
  `CEP` char(11) NOT NULL,
  `Data_Cadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID_Fornecedor`),
  UNIQUE KEY `CNPJ` (`CNPJ`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fornecedores`
--

LOCK TABLES `fornecedores` WRITE;
/*!40000 ALTER TABLE `fornecedores` DISABLE KEYS */;
INSERT INTO `fornecedores` VALUES (1,'Fornecedor Um','Um Fornecedor','01','01','01@gmail.com','01','01','01','01','01','2019-10-03 23:26:28'),(2,'Fornecedor Dois','Dois','02','01','01@gmail.com','01','01','01','01','01','2019-10-03 23:29:17');
/*!40000 ALTER TABLE `fornecedores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `horarios`
--

DROP TABLE IF EXISTS `horarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `horarios` (
  `ID_Horario` int(11) NOT NULL AUTO_INCREMENT,
  `Dia` varchar(3) NOT NULL,
  `Hora` varchar(8) NOT NULL,
  `Status` enum('A','I') NOT NULL DEFAULT 'A',
  `Data_Cadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID_Horario`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `horarios`
--

LOCK TABLES `horarios` WRITE;
/*!40000 ALTER TABLE `horarios` DISABLE KEYS */;
INSERT INTO `horarios` VALUES (1,'SEG','08:00:00','A','2019-10-03 22:37:48'),(2,'TER','08:00:00','A','2019-10-03 22:37:48'),(3,'QUA','08:00:00','A','2019-10-03 22:37:48'),(4,'QUI','08:00:00','A','2019-10-03 22:37:48'),(5,'SEX','08:00:00','A','2019-10-03 22:37:48'),(6,'SAB','08:00:00','A','2019-10-03 22:37:48'),(7,'SEG','10:00:00','A','2019-10-03 22:37:48'),(8,'TER','10:00:00','A','2019-10-03 22:37:48'),(9,'QUA','10:00:00','A','2019-10-03 22:37:48'),(10,'QUI','10:00:00','A','2019-10-03 22:37:48'),(11,'SEX','10:00:00','A','2019-10-03 22:37:48'),(12,'SAB','10:00:00','A','2019-10-03 22:37:48'),(13,'SEG','14:00:00','A','2019-10-03 22:37:48'),(14,'TER','14:00:00','A','2019-10-03 22:37:48'),(15,'QUA','14:00:00','A','2019-10-03 22:37:48'),(16,'QUI','14:00:00','A','2019-10-03 22:37:48'),(17,'SEX','14:00:00','A','2019-10-03 22:37:48'),(18,'SAB','12:00:00','A','2019-10-03 22:37:48'),(19,'QUI','20:00:00','A','2019-10-03 22:47:10');
/*!40000 ALTER TABLE `horarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `observacoes`
--

DROP TABLE IF EXISTS `observacoes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `observacoes` (
  `ID_Observacoes` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Observacoes` text NOT NULL,
  `FK_ID_Atendimento` int(10) unsigned DEFAULT NULL,
  `Data_Cadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID_Observacoes`),
  KEY `FK_Observacoes_1` (`FK_ID_Atendimento`),
  CONSTRAINT `FK_Observacoes_1` FOREIGN KEY (`FK_ID_Atendimento`) REFERENCES `atendimentos` (`ID_Atendimento`) ON DELETE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `observacoes`
--

LOCK TABLES `observacoes` WRITE;
/*!40000 ALTER TABLE `observacoes` DISABLE KEYS */;
INSERT INTO `observacoes` VALUES (1,'Medicação 3x ao dia',8,'2019-10-14 17:28:57');
/*!40000 ALTER TABLE `observacoes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `perfis`
--

DROP TABLE IF EXISTS `perfis`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `perfis` (
  `ID_Perfis` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Nome` varchar(100) NOT NULL,
  `Descricao` text,
  `Data_Cadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID_Perfis`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `perfis`
--

LOCK TABLES `perfis` WRITE;
/*!40000 ALTER TABLE `perfis` DISABLE KEYS */;
INSERT INTO `perfis` VALUES (1,'Administrador','Possui acesso total ao sistema','2019-10-03 22:21:46'),(2,'Veterinário','Possui acesso restrito as funções do sistema','2019-10-03 22:21:46'),(3,'Cliente','Não possui nenhum acesso ao sistema','2019-10-03 22:21:46'),(4,'Atendente','Possui acesso restrito as funções do sistema','2019-10-06 19:22:05');
/*!40000 ALTER TABLE `perfis` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `perfis_pessoas`
--

DROP TABLE IF EXISTS `perfis_pessoas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `perfis_pessoas` (
  `ID_Perfis_Pessoas` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `FK_ID_Pessoa` int(10) unsigned DEFAULT NULL,
  `FK_ID_Perfis` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`ID_Perfis_Pessoas`),
  KEY `FK_Perfis_Pessoas_1` (`FK_ID_Pessoa`),
  KEY `FK_Perfis_Pessoas_2` (`FK_ID_Perfis`),
  CONSTRAINT `FK_Perfis_Pessoas_1` FOREIGN KEY (`FK_ID_Pessoa`) REFERENCES `pessoas` (`ID_Pessoa`) ON DELETE NO ACTION,
  CONSTRAINT `FK_Perfis_Pessoas_2` FOREIGN KEY (`FK_ID_Perfis`) REFERENCES `perfis` (`ID_Perfis`) ON DELETE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `perfis_pessoas`
--

LOCK TABLES `perfis_pessoas` WRITE;
/*!40000 ALTER TABLE `perfis_pessoas` DISABLE KEYS */;
INSERT INTO `perfis_pessoas` VALUES (1,1,1),(2,2,2),(3,3,3),(4,4,4),(5,5,1),(6,6,4),(7,7,3);
/*!40000 ALTER TABLE `perfis_pessoas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pessoas`
--

DROP TABLE IF EXISTS `pessoas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pessoas` (
  `ID_Pessoa` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Nome` varchar(100) NOT NULL,
  `Login` varchar(20) DEFAULT NULL,
  `Password` text,
  `CPF` varchar(11) NOT NULL,
  `RG` text NOT NULL,
  `Email` varchar(100) NOT NULL,
  `DataNascimento` date DEFAULT NULL,
  `Rua` text NOT NULL,
  `Numero` varchar(20) NOT NULL,
  `Bairro` text NOT NULL,
  `Cidade` text NOT NULL,
  `CEP` char(11) NOT NULL,
  `Data_Cadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID_Pessoa`),
  UNIQUE KEY `CPF` (`CPF`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pessoas`
--

LOCK TABLES `pessoas` WRITE;
/*!40000 ALTER TABLE `pessoas` DISABLE KEYS */;
INSERT INTO `pessoas` VALUES (1,'Yuri Narvais Jacques','jacques','e10adc3949ba59abbe56e057f20f883e','02646179081','000','yurinarvaisjacques@gmail.com','1991-12-02','Marcelino de Freitas','364','Pitangas','Osório','95520000','2019-10-02 23:46:53'),(2,'Teste Um','veterinario','e10adc3949ba59abbe56e057f20f883e','01','01','01@gmail.com','1991-12-02','01','01','01','01','01','2019-10-03 22:28:36'),(3,'Teste Dois','cliente','e10adc3949ba59abbe56e057f20f883e','02','01','02@gmail.com','1991-12-02','02','02','02','02','02','2019-10-03 22:29:51'),(4,'Teste Salvar Error','atendente','e10adc3949ba59abbe56e057f20f883e','0212','020','salvarerror@gmail.com','0000-00-00','Error','0212','ceu','teste','955555','2019-10-10 23:27:03'),(5,'Narvais Jacques','narvais','cfa970af1c7913d4410b3c6975cf5c40','050550','05505','narvaisj@gmail.com','0000-00-00','05','02','02','02','020','2019-10-13 21:22:57'),(6,'Teste Dois N','teste','cfa970af1c7913d4410b3c6975cf5c40','022020202','2020','a@gmail.com','0000-00-00','02','02','02','02','02','2019-10-13 21:25:37'),(7,'Teste',NULL,NULL,'0200202','002','0@gmail.com','0000-00-00','0','0','0','0','0','2019-11-14 01:04:50');
/*!40000 ALTER TABLE `pessoas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pets`
--

DROP TABLE IF EXISTS `pets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pets` (
  `ID_Pet` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Nome` varchar(100) NOT NULL,
  `Porte` enum('P','M','G') NOT NULL DEFAULT 'M',
  `Especie` text NOT NULL,
  `Idade` int(11) DEFAULT NULL,
  `Pelagem` enum('C','M','L') NOT NULL DEFAULT 'M',
  `Cor` varchar(50) DEFAULT NULL,
  `Castrado` enum('S','N') NOT NULL DEFAULT 'N',
  `Sexo` enum('M','F','I') NOT NULL DEFAULT 'I',
  `Data_Cadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID_Pet`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pets`
--

LOCK TABLES `pets` WRITE;
/*!40000 ALTER TABLE `pets` DISABLE KEYS */;
INSERT INTO `pets` VALUES (1,'Um','P','tre',1,'C','fff','S','M','2019-10-03 22:30:25'),(2,'dois','P','02',2,'M','0215','N','F','2019-10-03 22:30:52'),(3,'Teste','M','Labrador',0,'M','','S','F','2019-10-11 00:32:42'),(4,'Teste','G','teste',0,'C','','S','M','2019-10-11 00:33:14'),(5,'teste','G','te',0,'C','','N','F','2019-10-11 00:33:55');
/*!40000 ALTER TABLE `pets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pets_pessoas`
--

DROP TABLE IF EXISTS `pets_pessoas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pets_pessoas` (
  `ID_Pets_Pessoas` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `FK_ID_Pet` int(10) unsigned DEFAULT NULL,
  `FK_ID_Pessoa` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`ID_Pets_Pessoas`),
  KEY `FK_Pets_Pessoas_1` (`FK_ID_Pet`),
  KEY `FK_Pets_Pessoas_2` (`FK_ID_Pessoa`),
  CONSTRAINT `FK_Pets_Pessoas_1` FOREIGN KEY (`FK_ID_Pet`) REFERENCES `pets` (`ID_Pet`) ON DELETE NO ACTION,
  CONSTRAINT `FK_Pets_Pessoas_2` FOREIGN KEY (`FK_ID_Pessoa`) REFERENCES `pessoas` (`ID_Pessoa`) ON DELETE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pets_pessoas`
--

LOCK TABLES `pets_pessoas` WRITE;
/*!40000 ALTER TABLE `pets_pessoas` DISABLE KEYS */;
INSERT INTO `pets_pessoas` VALUES (1,1,2),(2,2,3),(3,3,4),(4,4,2),(5,5,3);
/*!40000 ALTER TABLE `pets_pessoas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `produtos`
--

DROP TABLE IF EXISTS `produtos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `produtos` (
  `ID_Produto` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Nome_Completo` varchar(100) NOT NULL,
  `Nome_Abreviado` varchar(50) NOT NULL,
  `Descricao` text NOT NULL,
  `Tipo` text NOT NULL,
  `Categoria` text NOT NULL,
  `Medida` enum('KG','M','L','UNI') DEFAULT NULL,
  `Preco` decimal(6,2) NOT NULL,
  `Valor_Venda` decimal(6,2) NOT NULL,
  `Data_Cadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID_Produto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `produtos`
--

LOCK TABLES `produtos` WRITE;
/*!40000 ALTER TABLE `produtos` DISABLE KEYS */;
/*!40000 ALTER TABLE `produtos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `representante`
--

DROP TABLE IF EXISTS `representante`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `representante` (
  `ID_Representante` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `FK_ID_Fornecedor` int(10) unsigned DEFAULT NULL,
  `Representante` varchar(100) DEFAULT NULL,
  `Telefone` char(12) DEFAULT NULL,
  `Email` varchar(100) DEFAULT NULL,
  `Data_Cadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID_Representante`),
  KEY `FK_Representante_1` (`FK_ID_Fornecedor`),
  CONSTRAINT `FK_Representante_1` FOREIGN KEY (`FK_ID_Fornecedor`) REFERENCES `fornecedores` (`ID_Fornecedor`) ON DELETE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `representante`
--

LOCK TABLES `representante` WRITE;
/*!40000 ALTER TABLE `representante` DISABLE KEYS */;
INSERT INTO `representante` VALUES (1,1,'Teste Fornecedor','02152','025@gmail.com','2019-10-03 23:26:28'),(2,2,'01','01','01@gmail.com','2019-10-03 23:29:17');
/*!40000 ALTER TABLE `representante` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `servicos`
--

DROP TABLE IF EXISTS `servicos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `servicos` (
  `ID_Servico` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Nome` varchar(50) NOT NULL,
  `Descricao` text NOT NULL,
  `Valor` decimal(6,2) NOT NULL,
  `Tipo_Servico` enum('C','E','V') NOT NULL,
  `Data_Cadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID_Servico`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `servicos`
--

LOCK TABLES `servicos` WRITE;
/*!40000 ALTER TABLE `servicos` DISABLE KEYS */;
INSERT INTO `servicos` VALUES (1,'Tosa e Banho Pequeno Porte','Tosar pelo dog e depois dar banho',90.00,'E','2019-10-03 22:31:49'),(2,'Castração Gato Macho','Castração de Gato Macho sem internação',180.00,'C','2019-10-13 23:52:57'),(3,'Castração de Gato Femea','Com internação de uma semana.',280.00,'C','2019-10-13 23:53:32'),(4,'Vacina Antirabica Importada','vacina da contra raiva',60.00,'V','2019-10-13 23:54:22'),(5,'Vacina Antirabica Nacional','vacina contra raiva',30.00,'V','2019-10-13 23:54:47');
/*!40000 ALTER TABLE `servicos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `telefone_fornecedor`
--

DROP TABLE IF EXISTS `telefone_fornecedor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `telefone_fornecedor` (
  `ID_Telefone` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Telefone` char(11) NOT NULL,
  `FK_ID_Fornecedor` int(10) unsigned DEFAULT NULL,
  `Data_Cadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID_Telefone`),
  KEY `FK_Telefone_Fornecedor_1` (`FK_ID_Fornecedor`),
  CONSTRAINT `FK_Telefone_Fornecedor_1` FOREIGN KEY (`FK_ID_Fornecedor`) REFERENCES `fornecedores` (`ID_Fornecedor`) ON DELETE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `telefone_fornecedor`
--

LOCK TABLES `telefone_fornecedor` WRITE;
/*!40000 ALTER TABLE `telefone_fornecedor` DISABLE KEYS */;
INSERT INTO `telefone_fornecedor` VALUES (1,'01',1,'2019-10-03 23:26:28'),(2,'01',2,'2019-10-03 23:29:17'),(3,'01',2,'2019-10-03 23:29:17'),(4,'01',2,'2019-10-03 23:29:17'),(5,'01',2,'2019-10-03 23:29:17');
/*!40000 ALTER TABLE `telefone_fornecedor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `telefone_pessoa`
--

DROP TABLE IF EXISTS `telefone_pessoa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `telefone_pessoa` (
  `ID_Telefone` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Telefone` char(12) NOT NULL,
  `FK_ID_Pessoa` int(10) unsigned DEFAULT NULL,
  `Data_Cadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID_Telefone`),
  KEY `FK_Telefone_Pessoa_1` (`FK_ID_Pessoa`),
  CONSTRAINT `FK_Telefone_Pessoa_1` FOREIGN KEY (`FK_ID_Pessoa`) REFERENCES `pessoas` (`ID_Pessoa`) ON DELETE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `telefone_pessoa`
--

LOCK TABLES `telefone_pessoa` WRITE;
/*!40000 ALTER TABLE `telefone_pessoa` DISABLE KEYS */;
INSERT INTO `telefone_pessoa` VALUES (1,'997464296',2,'2019-10-03 22:28:36'),(2,'01',3,'2019-10-03 22:29:51'),(3,'012',4,'2019-10-10 23:27:03'),(4,'0',7,'2019-11-14 01:04:50');
/*!40000 ALTER TABLE `telefone_pessoa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vacinas`
--

DROP TABLE IF EXISTS `vacinas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vacinas` (
  `ID_Vacinas` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `FK_ID_Pet` int(10) unsigned DEFAULT NULL,
  `Vacina` varchar(50) NOT NULL,
  `Proxima_Vacina` date NOT NULL,
  `Data_Aplicacao` date NOT NULL,
  `Data_Cadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID_Vacinas`),
  KEY `FK_Vacinas_1` (`FK_ID_Pet`),
  CONSTRAINT `FK_Vacinas_1` FOREIGN KEY (`FK_ID_Pet`) REFERENCES `pets` (`ID_Pet`) ON DELETE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vacinas`
--

LOCK TABLES `vacinas` WRITE;
/*!40000 ALTER TABLE `vacinas` DISABLE KEYS */;
/*!40000 ALTER TABLE `vacinas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vendas`
--

DROP TABLE IF EXISTS `vendas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vendas` (
  `ID_Venda` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `FK_ID_Atendimento` int(10) unsigned DEFAULT NULL,
  `FK_ID_Pessoa` int(10) unsigned NOT NULL,
  `Valor_Parcela` double NOT NULL,
  `Valor_Total` double NOT NULL,
  `Parcela` int(11) NOT NULL,
  `Numero_Parcelas` int(11) NOT NULL,
  `Forma_Pagamento` enum('C','CH','D') NOT NULL DEFAULT 'D',
  `Pago` enum('S','N') NOT NULL DEFAULT 'N',
  `Data_Pagamento` timestamp NULL DEFAULT NULL,
  `Data_Cadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID_Venda`),
  KEY `FK_Vendas_4` (`FK_ID_Atendimento`),
  KEY `vendas_FK` (`FK_ID_Pessoa`),
  CONSTRAINT `FK_Vendas_4` FOREIGN KEY (`FK_ID_Atendimento`) REFERENCES `atendimentos` (`ID_Atendimento`) ON DELETE NO ACTION,
  CONSTRAINT `vendas_FK` FOREIGN KEY (`FK_ID_Pessoa`) REFERENCES `pessoas` (`ID_Pessoa`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vendas`
--

LOCK TABLES `vendas` WRITE;
/*!40000 ALTER TABLE `vendas` DISABLE KEYS */;
INSERT INTO `vendas` VALUES (1,6,3,45,90,1,2,'C','S','2019-10-14 19:10:05','2019-10-14 16:16:24'),(2,6,3,45,90,2,2,'C','S','2019-10-14 20:56:02','2019-10-14 16:16:24'),(3,5,2,60,180,1,3,'CH','S','2019-10-14 19:10:20','2019-10-14 16:26:24'),(4,5,2,60,180,2,3,'CH','N',NULL,'2019-10-14 16:26:24'),(5,5,2,60,180,3,3,'CH','N',NULL,'2019-10-14 16:26:24'),(6,8,2,280,280,1,1,'D','N',NULL,'2019-10-14 17:28:57');
/*!40000 ALTER TABLE `vendas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'petpaixao'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-11-16  7:08:10
