<?php

class Login_google_model extends CI_Model {

	/**
	 * Objeto do Google.
	 */
	var $client = false;

	/**
	 * Carrega a API do Google.
	 */
	function __construct() {
		
		require_once BASEPATH .'libraries/Google/autoload.php';
		
		require_once BASEPATH .'libraries/Google/Client.php';
		require_once BASEPATH .'libraries/Google/Service/Oauth2.php';
		
		$this->client = new Google_Client();
		$this->client->setClientId("283554439859-l5djqa254636ilso540faffqbhdhfem6.apps.googleusercontent.com");
		$this->client->setClientSecret("p2sk1f2uH70yvfB8okZXzkiY");
		$this->client->setRedirectUri(system_url ."login/login_google");
		
		$this->client->addScope("https://www.googleapis.com/auth/plus.me");
		$this->client->addScope("https://www.googleapis.com/auth/userinfo.email");	
		
	}
	
	/**
	 * Cria uma url de redirecionamento para login do google.
	 */
	public function createUrl() {
	
		return $this->client->createAuthUrl();
		
	}
	
	/**
	 * Autentica o $code pelo google.
	*/
	public function checkLogin($code) {

		if (isset($code)) {
		
			try {
				$this->client->authenticate($code);
				$_SESSION[session_prefix]["access_token"] = $this->client->getAccessToken();
				
			} catch(Exception $exc) {
				return false;
			}
			
		}
		
		if (isset($_SESSION[session_prefix]["access_token"])) {

			$service = new Google_Service_Oauth2($this->client);
			
			$data = $service->userinfo->get();	

		}
		
		return isset($data) ? $data : false;
		
	}
	
}

?>