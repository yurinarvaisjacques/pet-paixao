<?php

/**
 * Apl Model
 *
 * Utilizado na manipulação do app.
 * @author Yuri Jacques
 */
class Apl_model extends CI_Model {

	/**
	 * Verifica se usuário possui acesso.
	 * @return bool
	 */
	function logar($username, $password){

		$ds = $this->db->select(" ID_Pessoa, p.Nome, Login, Password, ID_Perfis, pe.Nome AS Perfil")
						->from('pessoas as p')
						->join('perfis_pessoas',"FK_ID_Pessoa = ID_Pessoa")
						->join('perfis as pe', "ID_Perfis = FK_ID_Perfis")
						->where('Login', $username)
						->where('Password', $password)
						->get()
						->row_array();
	
		$newdata = array(
			'CodigoUsuario' => $ds['ID_Pessoa'],
			'NomeUsuario'   => $ds['Nome'],
			'Perfil' 		=> $ds['Perfil'],
			'ID_Perfil'		=> $ds['ID_Perfis']
		);
		
		if($ds['Login'] == $username && $ds['Password'] == $password){
			$this->session->set_userdata($newdata);
			return true;
		}else{
			return false;
		}
	}

	/**
	 * Verifica se o usuário esta logado.
	 * @return bool
	 */
	function chkLogin() {
		return isset($this->session->CodigoUsuario);
	}

	/**
	 * Insere Pessoa
	 * @return int
	 */
	function insertPessoa($dados){
		$this->db->insert('pessoas', $dados);
		return $this->db->insert_id();
	}

	/**
	 * Insere telefone Pessoa
	 */
	function insertTelefonePessoa($celular){
		$this->db->insert('telefone_pessoa', $celular);
	}

	/**
	 * Insere perfil Cliente
	 */
	function insertPerfilPessoa($cliente){
		$this->db->insert('perfis_pessoas',$cliente);
	}

	/**
	 * Retorna o ID da Pessoa
	 * @return int
	 */
	public function getID_Pessoa($cpf){
		$ds = $this->db->select('ID_Pessoa')->where('CPF', $cpf)->from('pessoas')->get()->row_array();
		return $ds['ID_Pessoa'];
	}
	
	/**
	 * Atualiza informações da Pessoa
	 */
	function updatePessoa($cliente, $id){
		$this->db->update('pessoas', $cliente, array('ID_Pessoa' => $id));
	}

	/**
	 * Atualiza telefone da Pessoa
	 */
	function updateTelefonePessoa($cliente, $id){
		$this->db->where('ID_Telefone', $id);
		$this->db->update('telefone_pessoa', $cliente);
	}

	/**
	 * Retorna todos os clientes.
	 * @return array
	 */
	function getClientes() {
		
		$ds = $this->db->select(" ID_Pessoa, p.Nome, Email, ID_Perfis, pe.Nome AS Perfil")
						->from('pessoas as p')
						->join('perfis_pessoas',"FK_ID_Pessoa = ID_Pessoa")
						->join('perfis as pe', "ID_Perfis = FK_ID_Perfis")
						->where('ID_Perfis = 3')
						->order_by('ID_Pessoa', 'asc')
						->get()
						->result_array();

		$new = array();
		foreach ($ds as $d) {
			
			$new[$d['ID_Pessoa']] = $d;
			
		}
		return $new;
		
	}

	/**
	 * Retorna informações de uma Pessoa específica.
	 * @return array
	 */
	function getCliente($ID_Pessoa) {
		return $this->db->where('ID_Pessoa', $ID_Pessoa)->get('pessoas')->row_array();
	}

	/**
	 * Retorna telefone da Pessoa específca.
	 */
	function getTelefonePessoa($ID_Pessoa){
		return $this->db->select('Telefone')->where('FK_ID_Pessoa', $ID_Pessoa)->get('telefone_pessoa')->result_array();
	}


	/**
	 * Insere Fornecedor
	 * @return int
	 */
	function insertFornecedor($dados){
		$this->db->insert('fornecedores', $dados);
		return $this->db->insert_id();
	}

	/**
	 * Insere telefone Fornecedor
	 */
	function insertTelefoneFornecedor($celular){
		$this->db->insert('telefone_fornecedor', $celular);
	}

	/**
	 * Insere o representante do forncedor
	 */
	function insertRepresentante($dados){
		$this->db->insert('representante', $dados);
	}

	/**
	 * Retorna todos os fornecedores
	 * @return array
	 */
	function getFornecedores(){

		$ds = $this->db->select(" ID_Fornecedor,NomeFantasia, RazaoSocial, Email")
						->from('fornecedores')
						->get()
						->result_array();
		
		$new = array();
		
		foreach ($ds as $d) {
			$new[$d['ID_Fornecedor']] = $d;
		}
		return $new;
	}

	/**
	 * Retorna informações de um fornecedor específico.
	 * @return array
	 */
	function getFornecedor($ID_Fornecedor) {
		return $this->db->where('ID_Fornecedor', $ID_Fornecedor)->get('fornecedores')->row_array();
	}

	/**
	 * Retorna telefone do Fornecedor específco.
	 */
	function getTelefoneFornecedor($ID_Fornecedor){
		return $this->db->select('Telefone')->where('FK_ID_Fornecedor', $ID_Fornecedor)->get('telefone_fornecedor')->result_array();
	}

	/**
	 * Retorna representante do Fornecedor específco.
	 */
	function getRepresentanteFornecedor($ID_Fornecedor){
		return $this->db->where('FK_ID_Fornecedor', $ID_Fornecedor)->get('representante')->row_array();
	}

	/**
	 * Retorna ID do Fornecedor
	 * @return int
	 */
   public function getID_Fornecedor($cnpj){
	   $ds = $this->db->select('ID_Fornecedor')->where('CNPJ', $cnpj)->from('fornecedores')->get()->row_array();
	   return $ds['ID_Fornecedor'];
   }

   /**
	 * Atualiza informações do Fornecedor
	 */
	function updateFornecedor($fornecedor, $id){
		$this->db->update('fornecedores', $fornecedor, array('ID_Fornecedor' => $id));
	}

	/**
	 * Atualiza telefone do Fornecedor
	 */
	function updateTelefoneFornecedor($telefone,$id){
		$this->db->update('telefone_fornecedor', $telefone, array('FK_ID_Fornecedor' => $id));
	}

	/**
	 * Atualiza representante Fornecedor
	 */
	function updateRepresentante($representante, $id){
		$this->db->update('representante', $representante, array('ID_Representante' => $id));
	}

	/**
	 * Retorna id do Representante
	 * @return int
	 */
	function getID_Representante($representante){
		$ds = $this->db->select('ID_Representante')->where('FK_ID_Fornecedor', $representante)->from('representante')->get()->row_array();
		return $ds['ID_Representante']; 
	}

	/**
	 * Insere Pets
	 * @return int
	 */
	function insertPet($dados){
		$this->db->insert('pets', $dados);
		return $this->db->insert_id();
	}

	/**
	 * Insere proprierario do Pet
	 */
	function insertProprietarioPet($dados){
		$this->db->insert('pets_pessoas', $dados);
	}

	/**
	 * Buscar Proprietario do Pet
	 * @return array
	 */
	function BuscarProprietario(){
		return $this->db->select('ID_Pessoa, Nome')->from('pessoas')->get()->result_array();
	}

	/**
	 * Buscar Fornecedor
	 * @return array
	 */
	function BuscarFornecedor(){
		return $this->db->select('ID_Fornecedor, NomeFantasia')->from('fornecedores')->get()->result_array();
	}

	/**
	 * Retorna todos os Pets
	 * @return array
	 */
	function getPets(){
		
		$ds = $this->db->select("ID_Pet, pet.Nome as Nome, Especie, Idade, Sexo, p.Nome as Proprietario")
						->from('pets as pet')
						->join('pets_pessoas','ID_Pet = FK_ID_Pet')
						->join('pessoas as p','ID_Pessoa = FK_ID_Pessoa')
						->get()
						->result_array();
		
		$new = array();
		
		foreach ($ds as $d) {
			$new[$d['ID_Pet']] = $d;
		}
		return $new;
	}

	/**
	 * Retorna informações de um pet específico.
	 * @return array
	 */
	function getPet($ID_Pet) {
		return $this->db->where('ID_Pet', $ID_Pet)->get('pets')->row_array();
	}

	/**
	 * Atualiza informações do Pet
	 */
	function updatePet($dados, $ID_Pet){
		$this->db->update('pets', $dados, array('ID_Pet' => $ID_Pet));
	}

	/**
	 * Retorna todos os usuários.
	 * @return array
	 */
	function getUsuarios() {
		
		$ds = $this->db->select(" ID_Pessoa, p.Nome, Email, ID_Perfis, pe.Nome AS Perfil")
						->from('pessoas as p')
						->join('perfis_pessoas',"FK_ID_Pessoa = ID_Pessoa")
						->join('perfis as pe', "ID_Perfis = FK_ID_Perfis")
						->where('pe.ID_Perfis <> 3')
						->get()
						->result_array();

		$new = array();
		foreach ($ds as $d) {
			
			$new[$d['ID_Pessoa']] = $d;
			
		}
		return $new;
		
	}

	/**
	 * Retorna informações de um usuário específico.
	 * @return array
	 */
	function getUsuario($ID_Pessoa) {
		return $this->db->where('ID_Pessoa', $ID_Pessoa)->get('pessoas')->row_array();
	}

	/**
	 * Retorna todos os perfis.
	 * @return array
	 */
	function getPerfis() {
		return $this->db->order_by('ID_Perfis')->get('perfis')->result_array();
	}

	/**
	 * Retorna informações de um perfil específico.
	 * @return array
	 */
	function getPerfil($ID_Perfis) {
		return $this->db->where('ID_Perfis', $ID_Perfis)->get('perfis')->row_array();
	}

	/**
	 * Retorna informações de um perfil específico.
	 * @return array
	 */
	function getPerfilUsuario($ID_Pessoa) {
		return $this->db->where('FK_ID_Pessoa', $ID_Pessoa)->get('perfis_pessoas')->row_array();
	}

	/**
	 * Retorna todos os produtos.
	 * @return array
	 */
	function getProdutos() {
		
		$ds = $this->db->select(" ID_Produto, Nome_Completo, Descricao, Tipo")
						->from('produtos')
						->get()
						->result_array();

		$new = array();
		foreach ($ds as $d) {
			
			$new[$d['ID_Produto']] = $d;
			
		}
		return $new;
		
	}

	/**
	 * Retorna informações de um produto específico.
	 * @return array
	 */
	function getProduto($ID_Produto) {
		return $this->db->where('ID_Produto', $ID_Produto)->get('produtos')->row_array();
	}

	/**
	 * Insere informações sobre os serviços
	 */
	function insertServico($dados){
		$this->db->insert('servicos',$dados);
	}
	
	/** 
	* Insere informações sobre os serviços
	*/
	function updateServico($dados, $ID_Servico){
		$this->db->update('servicos', $dados, array('ID_Servico' => $ID_Servico));
	}

	/**
	 * Retorna todos os serviços.
	 * @return array
	 */
	function getServicos() {
		
		$ds = $this->db->select("ID_Servico, Nome, Descricao, Valor ,case 
		when Tipo_Servico = 'C' then 'Clínico'
		when Tipo_Servico = 'V' then 'Vacina'
		when Tipo_Servico = 'E' then 'Estético'
		end as Tipo_Servico")
						->from('servicos')
						->get()
						->result_array();

		$new = array();
		foreach ($ds as $d) {
			
			$new[$d['ID_Servico']] = $d;
			
		}
		return $new;
		
	}

	/**
	 * Retorna informações de um serviço específico.
	 * @return array
	 */
	function getServico($ID_Servico) {
		return $this->db->where('ID_Servico', $ID_Servico)->get('servicos')->row_array();
	}

	/**
	 * Retorna serviços clínicos.
	 * @return array
	 */
	function getServicoClinico(){
		return $this->db->select('ID_Servico, Nome')->where('Tipo_Servico', 'C')->get('servicos')->result_array();
	}

	/**
	 * Retorna serviços Estéticos.
	 * @return array
	 */
	function getServicoEstetico(){
		return $this->db->select('ID_Servico, Nome')->where('Tipo_Servico', 'E')->get('servicos')->result_array();
	}

	/**
	 * Retorna serviços Vacinação.
	 * @return array
	 */
	function getServicoVacinacao(){
		return $this->db->select('ID_Servico, Nome')->where('Tipo_Servico', 'V')->get('servicos')->result_array();
	}
	function getAtendimentosMarcados($tipo){
		return $this->db->select('trim(substring(horario_atendimento,1,22)) as horario_atendimento')
		->from('atendimentos as a')
		->join('servicos as s', 'a.FK_ID_Servico = s.ID_Servico')
		->where('s.Tipo_Servico', $tipo)
		->get()
		->result_array();
	}

	/**
	 * Retorna Horarios disponíveis para Prova.
	 * @return array
	 */
	function getLocalHorariosConsulta($tipo) {
		
		$limite = 20;


		$des = $this->getFeriados();
		$feriados = array();
		foreach ($des as $d) {
			$feriados[$d['data_feriado_br']] = true;
		}

		$ds = $this->getAtendimentosMarcados($tipo);
		$marcados = array();
		foreach ($ds as $d) {
			$marcados[$d['horario_atendimento']] = true;
		}

		$horarios = $this->getLocalHorarios();
		$dias_da_semana = array( );
		foreach ($horarios as $h) {
			$dia = $h['ordem']+1;
			$dias_da_semana[$dia][] = $h;
		}
		
		$data = date('d-m-Y').' 00:00:00';

		if (date('w', strtotime($data)) == 5) { // Sexta 
			$data = date('d-m-Y', strtotime($data. ' +1 day')).' 00:00:00';
		}
		
		if (isset($_SESSION[session_prefix])) {
			$data = date('d-m-Y', strtotime($data. ' -1 day')).' 00:00:00';
		}
		
		$new = array();



		for ($i=0;$i < $limite; $i++) {
			
			$data = date('d-m-Y', strtotime($data. ' +1 day')).' 00:00:00';
			
			$w = date('w', strtotime($data));

			if (isset($dias_da_semana[$w])) {
				
				list($sdata, $lixo) = explode(' ', $data, 2);
				
				if (!isset($feriados[$sdata])) {
					
					foreach ($dias_da_semana[$w] as $h) {
						
						$data_hora_prova = $sdata.' - '.$h['Hora'];
						
						if(!isset($marcados[$data_hora_prova])){
							$new[] = array(
								'data_consulta' => $sdata,
								'data_br' => date('d/m/Y', strtotime($data)),
								'dia' => $h['Dia'],
								'hora' => $h['Hora'],
								'dia_nome' => $h['dia_nome'],
								'em' => ($i+1)
							);
						}
	
						
					}
					
				}
				
			}
			
			
		}


		return $new;
		
	}


	/**
	 * Retorna Horarios do Local.
	 * @return array
	 */
	function getLocalHorarios() {
		
		return $this->db->select("
			ID_Horario
			,Dia
			,Hora
			,CASE
			WHEN dia = 'SEG' THEN 0
			WHEN dia = 'TER' THEN 1
			WHEN dia = 'QUA' THEN 2
			WHEN dia = 'QUI' THEN 3
			WHEN dia = 'SEX' THEN 4
			WHEN dia = 'SAB' THEN 5
			END AS ordem
			,CASE
			WHEN dia = 'SEG' THEN 'Segunda-feira'
			WHEN dia = 'TER' THEN 'Terça-feira'
			WHEN dia = 'QUA' THEN 'Quarta-feira'
			WHEN dia = 'QUI' THEN 'Quinta-feira'
			WHEN dia = 'SEX' THEN 'Sexta-feira'
			WHEN dia = 'SAB' THEN 'Sábado'
			END AS dia_nome
			")
		->where('Status', 'A')
		->order_by('ordem')
		->order_by('Hora')
		->get('horarios')->result_array();
		
	}

	/**
	 * Insere Atendimento
	 */
	public function insertAtendimento($dados){
		$this->db->insert('atendimentos', $dados);
	}

	/**
	 * Busca todos os atendimentos
	 * @return arrray 
	 */
	public function getAtendimentosConsultas(){
		return $this->db->select('ID_Atendimento, horario_atendimento, s.Nome as Servico,  p.Nome as Pet, pe.Nome as Proprietario, (select telefone from telefone_pessoa where FK_ID_Pessoa = pe.ID_Pessoa limit 1) as Telefone')
		->from('atendimentos')
		->join('pets as p', 'fk_id_pet = p.ID_Pet')
		->join('servicos as s','FK_ID_Servico = s.ID_Servico')
		->join('pets_pessoas as pp','p.ID_Pet = pp.FK_ID_Pet')
		->join('pessoas as pe','FK_ID_Pessoa = pe.ID_Pessoa')
		->where('s.Tipo_Servico', 'C')
		->where('atendido','N')
		->where('ativo','S')
		->get()->result_array();
	}

	/**
	 * Busca todos os banhos e tosa
	 * @return arrray 
	 */
	public function getAtendimentosBanhoTosa(){
		return $this->db->select('ID_Atendimento, horario_atendimento, s.Nome as Servico,  p.Nome as Pet, pe.Nome as Proprietario, (select telefone from telefone_pessoa where FK_ID_Pessoa = pe.ID_Pessoa limit 1) as Telefone')
		->from('atendimentos')
		->join('pets as p', 'fk_id_pet = p.ID_Pet')
		->join('servicos as s','FK_ID_Servico = s.ID_Servico')
		->join('pets_pessoas as pp','p.ID_Pet = pp.FK_ID_Pet')
		->join('pessoas as pe','FK_ID_Pessoa = pe.ID_Pessoa')
		->where('s.Tipo_Servico', 'E')
		->where('atendido','N')
		->where('ativo','S')
		->get()->result_array();
	}


	/**
	 * Busca todas as vacinações
	 * @return arrray 
	 */
	public function getAtendimentosVacinas(){
		return $this->db->select('ID_Atendimento, horario_atendimento, s.Nome as Servico,  p.Nome as Pet, pe.Nome as Proprietario, (select telefone from telefone_pessoa where FK_ID_Pessoa = pe.ID_Pessoa limit 1) as Telefone')
		->from('atendimentos')
		->join('pets as p', 'fk_id_pet = p.ID_Pet')
		->join('servicos as s','FK_ID_Servico = s.ID_Servico')
		->join('pets_pessoas as pp','p.ID_Pet = pp.FK_ID_Pet')
		->join('pessoas as pe','FK_ID_Pessoa = pe.ID_Pessoa')
		->where('s.Tipo_Servico', 'V')
		->where('atendido','N')
		->where('ativo','S')
		->get()->result_array();
	}

	/**
	 * Busca atendimento específico
	 * @return arrray 
	 */
	public function getAtendimento($ID_Atendimento){
		return $this->db->select('ID_Atendimento, horario_atendimento, s.Nome as Servico,  p.Nome as Pet, pe.ID_Pessoa as ID_Pessoa, 
		pe.Nome as Proprietario, (select telefone from telefone_pessoa where FK_ID_Pessoa = pe.ID_Pessoa limit 1) as Telefone, s.Valor, observacao')
		->from('atendimentos')
		->join('pets as p', 'fk_id_pet = p.ID_Pet')
		->join('servicos as s','FK_ID_Servico = s.ID_Servico')
		->join('pets_pessoas as pp','p.ID_Pet = pp.FK_ID_Pet')
		->join('pessoas as pe','FK_ID_Pessoa = pe.ID_Pessoa')
		->where('ID_Atendimento', $ID_Atendimento)
		->get()->row_array();
	}

	/**
	 * Busca todos os atendimentos do dia atual
	 * @return arrray 
	 */
	public function getAtendimentosDiaAtual(){
		return $this->db->select('ID_Atendimento, horario_atendimento, s.Nome as Servico,  p.Nome as Pet, pe.Nome as Proprietario, (select telefone from telefone_pessoa where FK_ID_Pessoa = pe.ID_Pessoa limit 1) as Telefone')
		->from('atendimentos')
		->join('pets as p', 'fk_id_pet = p.ID_Pet')
		->join('servicos as s','FK_ID_Servico = s.ID_Servico')
		->join('pets_pessoas as pp','p.ID_Pet = pp.FK_ID_Pet')
		->join('pessoas as pe','FK_ID_Pessoa = pe.ID_Pessoa')
		->where("trim(substring(horario_atendimento,1,11)) = (select date_format(trim(substring(now(),1,10)), '%d-%m-%Y'))")
		->where('atendido','N')
		->where('ativo','S')
		->get()->result_array();
	}

	/**
	 * Retorna Feriados do Local.
	 * @return array
	 */
	function getFeriados() {
		
		return $this->db->select("
			ID_Feriado
			,Data_Feriado
			,DATE_FORMAT(Data_Feriado, '%d-%m-%Y') AS data_feriado_br
			,DATE_FORMAT(Data_Feriado, '%Y') AS ano
			,DATE_FORMAT(Data_Feriado, '%m') AS mes
			,UPPER(DATE_FORMAT(Data_Feriado, '%M')) AS mes_nome
			,DATE_FORMAT(Data_Feriado, '%d') AS dia
			")
		->where('Status', 'A')
		->order_by('YEAR(Data_Feriado)')
		->order_by('MONTH(Data_Feriado)')
		->order_by('DAY(Data_Feriado)')
		->get('feriados')->result_array();
		
	}

	function updateAtendimentosInativar($dados, $ID_Atendimento){
		$result = $this->db->update('atendimentos', $dados, array('ID_Atendimento' => $ID_Atendimento));
		if($result){
			return true;
		}else{
			return false;
		}
	}

	function getPetProprietario(){
		return $this->db->select("pet.ID_Pet, CONCAT('Pet: ',pet.Nome,'  Espécie: ', pet.Especie ,'    Proprietário: ', p.Nome) as Pet")
		->from('pets as pet')
		->join('pets_pessoas as pp','pet.ID_Pet = pp.FK_ID_Pet')
		->join('pessoas as  p','pp.FK_ID_Pessoa = p.ID_Pessoa')
		->get()->result_array();
	}
	
	function insertRegistroAtendimento($dados){
		$this->db->insert('vendas',$dados);
	}

	function updateAtendido($id, $dados){
		$this->db->update('atendimentos',$dados, array('ID_Atendimento' => $id));
	}

	function insertObservacao($dados){
		$this->db->insert('observacoes',$dados);
	}

	function getVendas(){
		return $this->db->select("v.ID_Venda, p.Nome as Cliente, v.Valor_Parcela, v.Valor_Total, v.Parcela, v.Numero_Parcelas, 
		case when v.Forma_Pagamento = 'D' then 'Dinheiro'
		when v.Forma_Pagamento = 'C' then 'Cartão'
		when v.Forma_Pagamento = 'CH' then 'Cheque'
		end as Forma_Pagamento, v.Pago, v.Data_Pagamento, o.Observacoes")
		->from('vendas as v')
		->join('observacoes as o','v.FK_ID_Atendimento = o.FK_ID_Atendimento','left')
		->join('pessoas as p', 'v.FK_ID_Pessoa = p.ID_Pessoa')
		->get()->result_array();
	}

	function updateVendas($dados, $id){
		$result = $this->db->update('vendas', $dados, array('ID_Venda' => $id));

		if($result){
			return true;
		}else{
			return false;
		}
	}

	function receber(){
		return $this->db->select('SUM(Valor_Parcela) as receber')
		->from('vendas')
		->where('Pago','N')
		->get()
		->row_array();
	}

	function caixa(){
		return $this->db->select('SUM(Valor_Parcela) as caixa')
		->from('vendas')
		->where('Pago','S')
		->get()
		->row_array();
	}
}