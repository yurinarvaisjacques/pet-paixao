<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Financeiro
 * 
 * Página principal do sistema utilizado para apresentar informações.
 * @author Yuri Jacques
 */
class  Financeiro extends CI_Controller{
	
	function __construct(){
		parent::__construct();
		if (!$this->Apl_model->chkLogin()) {
			redirect("login");
		}
		
	}

    public function index(){
        $dados = array();
		$dados['lembrete'] = $this->Apl_model->getAtendimentosDiaAtual();	
		$this->load->view('default/top');
		$this->load->view('default/navbar', $dados);
		$this->load->view('financeiro', $dados);
		$this->load->view('default/bot');
    }

    public function Registrar(){
        $this->load->library('form_validation');
		$this->form_validation->set_rules('atendimento', 'ID_Atendimento', 'trim|required');
		$this->form_validation->set_rules('idpessoa', 'ID_Pessoa', 'trim|required');
		$this->form_validation->set_rules('valor_parcela', 'Valor da Parcela', 'trim|required');
        $this->form_validation->set_rules('valor_total', 'Valor Total', 'trim|required');
        $this->form_validation->set_rules('numero_parcelas', 'Numero de Parcelas', 'trim|required');
        $this->form_validation->set_rules('forma_pagamento', 'Forma de Pagamento', 'trim|required');
		$this->form_validation->set_rules('obs', 'Observações', 'trim');
        
        

		if ($this->form_validation->run() == false) {
            $response = array(
                'status' => 'error',
                'message' => validation_errors()
            );
        }
        else {
            $i = $this->input->post('numero_parcelas');
            $j = 1;
            for($j; $j <= $i; $j++ ){
                $dados = array(
                    'FK_ID_Atendimento' => $_POST['atendimento'],
                    'FK_ID_Pessoa' => $_POST['idpessoa'],
                    'Valor_Parcela' => $this->input->post('valor_parcela'),
                    'Valor_Total' => $this->input->post('valor_total'),
                    'Parcela' => $j,
                    'Numero_Parcelas' => $this->input->post('numero_parcelas'),
                    'Forma_Pagamento' => $this->input->post('forma_pagamento')
                );
                $this->Apl_model->insertRegistroAtendimento($dados);
            }

            $atendido = array(
                'atendido' => 'S'
            );
            
            $this->Apl_model->updateAtendido($_POST['atendimento'], $atendido);

            $obs = array(
                'Observacoes' => $this->input->post('obs'),
                'FK_ID_Atendimento' => $_POST['atendimento']
            );

            $this->Apl_model->insertObservacao($obs);

			$response = array(
                'status' => 'success',
                'message' => 'Atendimento Registrado com sucesso'
			);
		}

		$this->output
		->set_content_type('application/json')
		->set_output(json_encode($response));
    }
    

    public function Buscar(){
        $dados = array();
        $dados['lembrete'] = $this->Apl_model->getAtendimentosDiaAtual();
        $dados['vendas'] = 	$this->Apl_model->getVendas();
		$this->load->view('default/top');
		$this->load->view('default/navbar', $dados);
		$this->load->view('financeiro', $dados);
		$this->load->view('default/bot');
    }

    public function json_ConfirmarPagamento($ID_Venda){
		$dados = array(
            'Pago' => 'S',
            'Data_Pagamento' => date('Y-m-d H:i:s')
		);

		$verificacao = $this->Apl_model->updateVendas($dados, $ID_Venda);

		if($verificacao != true){
			$response = array(
				'status' => 'error',
				'message' => 'Status de pagamento não foi atualizado'
			);
		}else{
			$response = array(
				'status' => 'success',
				'message' => 'Status de pagamento foi atualizado'
			);
		}
		

		$this->output
		->set_content_type('application/json')
		->set_output(json_encode($response));
	}

}