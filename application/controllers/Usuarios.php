<?php 
defined('BASEPATH') OR exit('No direct script access allowed');


/**
 * Usuarios
 * 
 * Página principal do sistema utilizado para apresentar informações.
 * @author Yuri Jacques
 */
class  Usuarios extends CI_Controller{
	
	function __construct(){
		parent::__construct();
		if (!$this->Apl_model->chkLogin()) {
			redirect("login");
		}
		
	}

	public function index(){
        $dados = array();
		$dados['usuarios'] = $this->Apl_model->getUsuarios();
		$dados['lembrete'] = $this->Apl_model->getAtendimentosDiaAtual();
        $this->load->view('default/top');
        $this->load->view('default/navbar', $dados);
        $this->load->view('modal/infoUsuario');
        $this->load->view('modal/addusuario');
		$this->load->view('modal/editarUsuario');
		$this->load->view('usuarios', $dados);
		$this->load->view('default/bot');
	}

	public function Cadastrar(){
		$this->load->library('form_validation');
		$this->form_validation->set_rules('1nome', 'Nome', 'trim|required');
		$this->form_validation->set_rules('1rg', 'Rg', 'trim|required');
		$this->form_validation->set_rules('1cpf', 'Cpf', 'trim|required');
		$this->form_validation->set_rules('1email', 'E-mail', 'trim|required|valid_email');
		$this->form_validation->set_rules('1nascimento', 'Nascimento', 'trim');
		$this->form_validation->set_rules('1rua', 'Endereco', 'trim|required');
		$this->form_validation->set_rules('1numero', 'Numero', 'trim|required');
		$this->form_validation->set_rules('1cep', 'Cep', 'trim|required');
		$this->form_validation->set_rules('1bairro', 'Bairro', 'trim|required');
		$this->form_validation->set_rules('1cidade', 'Municipio', 'trim|required');
		$this->form_validation->set_rules('1celular1', '1° Celular', 'trim|required');
		$this->form_validation->set_rules('1celular2', '2° Celular', 'trim');
		$this->form_validation->set_rules('1celular3', '3° Celular', 'trim');
        $this->form_validation->set_rules('1celular4', '4° Celular', 'trim');
        $this->form_validation->set_rules('1login', 'Login', 'trim|required');
        $this->form_validation->set_rules('1senha', 'Senha', 'trim|required');
        $this->form_validation->set_rules('1perfil', 'Perfil', 'trim|required');


		if ($this->form_validation->run() == false) {
            $response = array(
                'status' => 'error',
                'message' => validation_errors()
            );
        }
        else {
            $dados = array(
				'Nome' => $this->input->post('1nome'),
				'CPF' => $this->input->post('1cpf'),
				'RG' => $this->input->post('1rg'),
				'Email' => $this->input->post('1email'),
				'DataNascimento' => $this->input->post('1nascimento'),
				'Rua' => $this->input->post('1rua'),
				'Numero' => $this->input->post('1numero'),
				'Bairro' => $this->input->post('1bairro'),
				'Cidade' => $this->input->post('1cidade'),
                'CEP' => $this->input->post('1cep'),
                'Login' => $this->input->post('1login'),
                'Password' => md5($this->input->post('1senha'))
            );

			$id = $this->Apl_model->insertPessoa($dados);
			
			$usuario = array(
				'FK_ID_Pessoa' => $id, 
				'FK_ID_Perfis' => $this->input->post('1perfil')
			);

			$this->Apl_model->insertPerfilPessoa($usuario);
			
			if($this->input->post('celular') != NULL){
				$celular = array(
					'Telefone' => $this->input->post('celular'), 
					'FK_ID_Pessoa' => $id
				);
				$this->Apl_model->insertTelefonePessoa($celular);
			}

			if($this->input->post('celular1') != NULL){
				$celular = array(
					'Telefone' => $this->input->post('celular1'), 
					'FK_ID_Pessoa' => $id
				);
				$this->Apl_model->insertTelefonePessoa($celular);
			}

			if($this->input->post('celular2') != NULL){
				$celular = array(
					'Telefone' => $this->input->post('celular2'), 
					'FK_ID_Pessoa' => $id
				);
				$this->Apl_model->insertTelefonePessoa($celular);
			}

			if($this->input->post('celular3') != NULL){
				$celular = array(
					'Telefone' => $this->input->post('celular3'), 
					'FK_ID_Pessoa' => $id
				);
				$this->Apl_model->insertTelefonePessoa($celular);
			}

            $response = array(
                'status' => 'success',
                'message' => 'Informações salvas com sucesso'
            );
		}

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($response));
	}

    public function Atualizar(){

		$this->load->library('form_validation');
		$this->form_validation->set_rules('nome1', 'Nome', 'trim|required');
		$this->form_validation->set_rules('rg1', 'Rg', 'trim|required');
		$this->form_validation->set_rules('cpf1', 'Cpf', 'trim|required');
		$this->form_validation->set_rules('email1', 'E-mail', 'trim|required|valid_email');
		$this->form_validation->set_rules('nascimento1', 'Nascimento', 'trim');
		$this->form_validation->set_rules('rua1', 'Rua', 'trim|required');
		$this->form_validation->set_rules('numero1', 'Numero', 'trim|required');
		$this->form_validation->set_rules('cep1', 'Cep', 'trim|required');
		$this->form_validation->set_rules('bairro1', 'Bairro', 'trim|required');
		$this->form_validation->set_rules('cidade1', 'Cidade', 'trim|required');
		$this->form_validation->set_rules('celular11', '1° Celular', 'trim');
		$this->form_validation->set_rules('celular21', '2° Celular', 'trim');
		$this->form_validation->set_rules('celular31', '3° Celular', 'trim');
        $this->form_validation->set_rules('celular41', '4° Celular', 'trim');
        $this->form_validation->set_rules('login1', 'Login', 'trim|required');
        $this->form_validation->set_rules('senha1', 'Senha', 'trim|required');
        $this->form_validation->set_rules('perfil1', 'Perfil', 'trim|required');

		$id = $this->Apl_model->getID_Pessoa($this->input->post('cpf1'));
		
		if ($this->form_validation->run() == false) {
            $response = array(
                'status' => 'error',
                'message' => validation_errors()
            );
        }
        else {
            $dados = array(
				'Nome' => $this->input->post('nome1'),
				'CPF' => $this->input->post('cpf1'),
				'RG' => $this->input->post('rg1'),
				'Email' => $this->input->post('email1'),
				'DataNascimento' => $this->input->post('nascimento1'),
				'Rua' => $this->input->post('rua1'),
				'Numero' => $this->input->post('numero1'),
				'Bairro' => $this->input->post('bairro1'),
				'Cidade' => $this->input->post('cidade1'),
				'CEP' => $this->input->post('cep1'),
                'Login' => $this->input->post('login'),
                'Password' => $this->input->post('senha')
			);

			$this->Apl_model->updatePessoa($dados, $id);

            $response = array(
                'status' => 'success',
                'message' => 'Informações atualizadas com sucesso'
			);
			
		}


        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($response));
	}
    
    public function json_visualizarusuario($ID_Pessoa){
		$usuario = $this->Apl_model->getUsuario($ID_Pessoa);
		$i = 1;
		foreach ($this->Apl_model->getTelefonePessoa($ID_Pessoa) as $value) {
			$telefone[$i] = $value['Telefone'];
			$i++;
        }
        
        $perfil = $this->Apl_model->getPerfilUsuario($usuario['ID_Pessoa']);

		$dados = array(
			'Nome' => isset($usuario['Nome']) ? $usuario['Nome'] : '',
			'CPF' => isset($usuario['CPF']) ? $usuario['CPF'] : '',
			'RG' => isset($usuario['RG']) ? $usuario['RG'] : '',
			'Email' => isset($usuario['Email']) ? $usuario['Email'] : '',
			'DataNascimento' => isset($usuario['DataNascimento']) ? $usuario['DataNascimento'] : '',
			'Rua' => isset($usuario['Rua']) ? $usuario['Rua'] : '',
			'Numero' => isset($usuario['Numero']) ? $usuario['Numero'] : '',
			'Bairro' => isset($usuario['Bairro']) ? $usuario['Bairro'] : '',
			'Cidade' => isset($usuario['Cidade']) ? $usuario['Cidade'] : '',
			'CEP' => isset($usuario['CEP']) ? $usuario['CEP'] : '',
			'Celular1' => isset($telefone[1]) ? $telefone[1] : '',
			'Celular2' => isset($telefone[2]) ? $telefone[2] : '',
			'Celular3' => isset($telefone[3]) ? $telefone[3] : '',
            'Celular4' => isset($telefone[4]) ? $telefone[4] : '',
            'Login'    => isset($usuario['Login']) ? $usuario['Login'] : '',
            'Senha'    => isset($usuario['Password']) ? $usuario['Password'] : '',
            'Perfil'   => isset($perfil['FK_ID_Perfis']) ? $perfil['FK_ID_Perfis'] : ''
		);

		 echo json_encode($dados);
	}

	public function perfil(){
		
	}
}