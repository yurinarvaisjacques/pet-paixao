<?php 
defined('BASEPATH') OR exit('No direct script access allowed');


/**
 * Produtos
 * 
 * Utilizado na manipulação de informações dos produtos.
 * @author Yuri Jacques
 */
class  Produtos extends CI_Controller{
	
	function __construct(){
		parent::__construct();
		if (!$this->Apl_model->chkLogin()) {
			redirect("login");
		}
	}

	public function index(){
		$dados = array();
		$dados['lembrete'] = $this->Apl_model->getAtendimentosDiaAtual();
		$dados['fornecedor'] = $this->Apl_model->BuscarFornecedor();
		$this->load->view('default/top');
		$this->load->view('default/navbar', $dados);
		$this->load->view('produtos', $dados);
		$this->load->view('default/bot');
	}

	public function Cadastrar(){
		$this->load->library('form_validation');
		$this->form_validation->set_rules('nomeproduto', 'Nome do Produto', 'trim|required');
		$this->form_validation->set_rules('nomeabreviado', 'Nome Abreviado', 'trim');
		$this->form_validation->set_rules('quantidade', 'Quantidade', 'trim|required');
		$this->form_validation->set_rules('valorunitario', 'Valor Unitário', 'trim|required');
		$this->form_validation->set_rules('valortotal', 'Valor Total', 'trim|required');
		$this->form_validation->set_rules('valorvenda', 'Valor Venda', 'trim');
		$this->form_validation->set_rules('tipo', 'Tipo', 'trim|required');
		$this->form_validation->set_rules('categoria', 'Categoria', 'trim');
		$this->form_validation->set_rules('medida', 'Medida', 'trim|required');
		$this->form_validation->set_rules('descricao', 'Descrição', 'trim|required');

		if ($this->form_validation->run() == false) {
            $response = array(
                'status' => 'error',
                'message' => validation_errors()
            );
        }
        else {
            $dados = array(
				'Nome' => $this->input->post('nome'),
				'Porte' => $this->input->post('porte'),
				'Especie' => $this->input->post('especie'),
				'Idade' => $this->input->post('idade'),
				'Pelagem' => $this->input->post('pelagem'),
				'Cor' => $this->input->post('cor'),
				'Castrado' => $this->input->post('castrado'),
				'Sexo' => $this->input->post('sexo')
			);
			
			$id = $this->Apl_model->insertPet($dados);

			$proprietarios = array(
				'FK_ID_Pet' => $id,
				'FK_ID_Pessoa' => $this->input->post('proprietario')
			);

			$this->Apl_model->insertProprietarioPet($proprietarios);


            $response = array(
                'status' => 'success',
                'message' => 'Informações salvas com sucesso'
            );
		}

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($response));
	}

	public function Atualizar(){

	}

	public function Inativar(){

	}

	public function Buscar(){
		
	}

}