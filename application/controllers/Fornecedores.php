<?php 
defined('BASEPATH') OR exit('No direct script access allowed');


/**
 * Fornecedores
 * 
 * Utilizado na manipulação de informações dos fornecedores.
 * @author Yuri Jacques
 */
class Fornecedores extends CI_Controller{
	
	function __construct(){
		parent::__construct();
		if (!$this->Apl_model->chkLogin()) {
			redirect("login");
		}
	}

	public function index(){
		$dados = array();
		$dados['lembrete'] = $this->Apl_model->getAtendimentosDiaAtual();
		$this->load->view('default/top');
		$this->load->view('default/navbar', $dados);
		$this->load->view('fornecedores');
		$this->load->view('default/bot');
	}

	public function Cadastrar(){
		$this->load->library('form_validation');
		$this->form_validation->set_rules('nomefantasia', 'Nome Fantasia', 'trim|required');
		$this->form_validation->set_rules('razaosocial', 'Razão Social', 'trim|required');
		$this->form_validation->set_rules('cnpj', 'Cnpj', 'trim|required');
		$this->form_validation->set_rules('inscricaoestadual', 'Inscrição Estadual', 'trim');
		$this->form_validation->set_rules('email', 'E-mail', 'trim|required');
		$this->form_validation->set_rules('endereco', 'Endereço', 'trim|required');
		$this->form_validation->set_rules('numero', 'Número', 'trim|required');
		$this->form_validation->set_rules('cep', 'Cep', 'trim|required');
		$this->form_validation->set_rules('bairro', 'Bairro', 'trim|required');
		$this->form_validation->set_rules('municipio', 'Municipio', 'trim|required');
		$this->form_validation->set_rules('celular', '1° Celular', 'trim|required');
		$this->form_validation->set_rules('celular1', '2° Celular', 'trim');
		$this->form_validation->set_rules('celular2', '3° Celular', 'trim');
		$this->form_validation->set_rules('celular3', '4° Celular', 'trim');
		$this->form_validation->set_rules('representante', 'Representante', 'trim');
		$this->form_validation->set_rules('celularrepresentante', 'Celular do Representante', 'trim');
		$this->form_validation->set_rules('emailrepresentante', 'E-mail do Representante', 'trim');

		if ($this->form_validation->run() == false) {
            $response = array(
                'status' => 'error',
                'message' => validation_errors()
            );
        }else {
            $dados = array(
				'NomeFantasia' => $this->input->post('nomefantasia'),
				'RazaoSocial' => $this->input->post('razaosocial'),
				'CNPJ' => $this->input->post('cnpj'),
				'InscricaoEstadual' => $this->input->post('inscricaoestadual'),
				'Email' => $this->input->post('email'),
				'Rua' => $this->input->post('endereco'),
				'Numero' => $this->input->post('numero'),
				'Bairro' => $this->input->post('bairro'),
				'Cidade' => $this->input->post('municipio'),
				'CEP' => $this->input->post('cep')
            );

			$id = $this->Apl_model->insertFornecedor($dados);
			
			if($this->input->post('representante') != NULL){
				$representante = array(
					'FK_ID_Fornecedor' => $id, 
					'Representante' => $this->input->post('representante'), 
					'Telefone' => $this->input->post('celularrepresentante'), 
					'Email' => $this->input->post('emailrepresentante')
				);
	
				$this->Apl_model->insertRepresentante($representante);
			}

			if($this->input->post('celular') != NULL){
				$celular = array(
					'Telefone' => $this->input->post('celular'), 
					'FK_ID_Fornecedor' => $id
				);
				$this->Apl_model->insertTelefoneFornecedor($celular);
			}

			if($this->input->post('celular1') != NULL){
				$celular = array(
					'Telefone' => $this->input->post('celular1'), 
					'FK_ID_Fornecedor' => $id
				);
				$this->Apl_model->insertTelefoneFornecedor($celular);
			}

			if($this->input->post('celular2') != NULL){
				$celular = array(
					'Telefone' => $this->input->post('celular2'), 
					'FK_ID_Fornecedor' => $id
				);
				$this->Apl_model->insertTelefoneFornecedor($celular);
			}

			if($this->input->post('celular3') != NULL){
				$celular = array(
					'Telefone' => $this->input->post('celular3'), 
					'FK_ID_Fornecedor' => $id
				);
				$this->Apl_model->insertTelefoneFornecedor($celular);
			}

            $response = array(
                'status' => 'success',
                'message' => 'Informações salvas com sucesso'
            );
		}

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($response));


	}

	public function Atualizar(){
		$this->load->library('form_validation');
		$this->form_validation->set_rules('nomefantasia1', 'Nome Fantasia', 'trim|required');
		$this->form_validation->set_rules('razaosocial1', 'Razão Social', 'trim|required');
		$this->form_validation->set_rules('cnpj1', 'Cnpj', 'trim|required');
		$this->form_validation->set_rules('inscricaoestadual1', 'Inscrição Estadual', 'trim');
		$this->form_validation->set_rules('email1', 'E-mail', 'trim|required');
		$this->form_validation->set_rules('rua1', 'Endereço', 'trim|required');
		$this->form_validation->set_rules('numero1', 'Número', 'trim|required');
		$this->form_validation->set_rules('cep1', 'Cep', 'trim|required');
		$this->form_validation->set_rules('bairro1', 'Bairro', 'trim|required');
		$this->form_validation->set_rules('bairro1', 'Municipio', 'trim|required');
		$this->form_validation->set_rules('celular11', '1° Celular', 'trim|required');
		$this->form_validation->set_rules('celular21', '2° Celular', 'trim');
		$this->form_validation->set_rules('celular31', '3° Celular', 'trim');
		$this->form_validation->set_rules('celular41', '4° Celular', 'trim');
		$this->form_validation->set_rules('representante1', 'Representante', 'trim');
		$this->form_validation->set_rules('celularrepresentante1', 'Celular do Representante', 'trim');
		$this->form_validation->set_rules('emailrepresentante1', 'E-mail do Representante', 'trim');

		$id = $this->Apl_model->getID_Fornecedor($this->input->post('cnpj1'));

		if ($this->form_validation->run() == false) {
            $response = array(
                'status' => 'error',
                'message' => validation_errors()
            );
        }else {
            $dados = array(
				'NomeFantasia' => $this->input->post('nomefantasia1'),
				'RazaoSocial' => $this->input->post('razaosocial1'),
				'CNPJ' => $this->input->post('cnpj1'),
				'InscricaoEstadual' => $this->input->post('inscricaoestadual1'),
				'Email' => $this->input->post('email1'),
				'Rua' => $this->input->post('rua1'),
				'Numero' => $this->input->post('numero1'),
				'Bairro' => $this->input->post('bairro1'),
				'Cidade' => $this->input->post('municipio1'),
				'CEP' => $this->input->post('cep1')
			);
			
			$this->Apl_model->updateFornecedor($dados, $id);

			if($this->input->post('representante1') != NULL){
				$idRepresentante = $this->Apl_model->getID_Representante($id);
				
				$representante = array(
					'Representante' => $this->input->post('representante1'), 
					'Telefone' => $this->input->post('celularrepresentante1'), 
					'Email' => $this->input->post('emailrepresentante1')
				);

				$this->Apl_model->updateRepresentante($representante, $idRepresentante);
			}

			// if($this->input->post('celular11') != NULL){
			// 	$this->Apl_model->updateTelefoneFornecedor($this->input->post('celular11'), $id);
			// }

			// if($this->input->post('celular21') != NULL){
			// 	$this->Apl_model->updateTelefoneFornecedor($this->input->post('celular21'), $id);
			// }

			// if($this->input->post('celular31') != NULL){
			// 	$this->Apl_model->updateTelefoneFornecedor($this->input->post('celular31'), $id);
			// }

			// if($this->input->post('celular41') != NULL){ 
			// 	$this->Apl_model->updateTelefoneFornecedor($this->input->post('celular41'), $id);
			// }
			
            $response = array(
                'status' => 'success',
                'message' => 'Informações salvas com sucesso'
            );
		}

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($response));
	}

	public function Buscar(){
		$dados = array();
		$dados['fornecedores'] = $this->Apl_model->getFornecedores();
		$dados['lembrete'] = $this->Apl_model->getAtendimentosDiaAtual();	
		$this->load->view('default/top');
		$this->load->view('default/navbar', $dados);
		$this->load->view('modal/infoFornecedor');
		$this->load->view('modal/editarFornecedor');
		$this->load->view('listarFornecedores', $dados);
		$this->load->view('default/bot');
	}

	public function json_visualizarfornecedor($ID_Fornecedor){
		$fornecedor = $this->Apl_model->getFornecedor($ID_Fornecedor);
		$i = 1;
		foreach ($this->Apl_model->getTelefoneFornecedor($ID_Fornecedor) as $value) {
			$telefone[$i] = $value['Telefone'];
			$i++;
		}
		$representante = $this->Apl_model->getRepresentanteFornecedor($ID_Fornecedor);
		$dados = array(
			'NomeFantasia' => isset($fornecedor['NomeFantasia']) ? $fornecedor['NomeFantasia'] : '',
			'RazaoSocial' => isset($fornecedor['RazaoSocial']) ? $fornecedor['RazaoSocial'] : '',
			'CNPJ' => isset($fornecedor['CNPJ']) ? $fornecedor['CNPJ'] : '',
			'InscricaoEstadual' => isset($fornecedor['InscricaoEstadual']) ? $fornecedor['InscricaoEstadual'] : '',
			'Email' => isset($fornecedor['Email']) ? $fornecedor['Email'] : '',
			'Rua' => isset($fornecedor['Rua']) ? $fornecedor['Rua'] : '',
			'Numero' => isset($fornecedor['Numero']) ? $fornecedor['Numero'] : '',
			'Bairro' => isset($fornecedor['Bairro']) ? $fornecedor['Bairro'] : '',
			'Cidade' => isset($fornecedor['Cidade']) ? $fornecedor['Cidade'] : '',  
			'CEP' => isset($fornecedor['CEP']) ? $fornecedor['CEP'] : '',
			'Celular1' => isset($telefone[1]) ? $telefone[1] : '',
			'Celular2' => isset($telefone[2]) ? $telefone[2] : '',
			'Celular3' => isset($telefone[3]) ? $telefone[3] : '',
			'Celular4' => isset($telefone[4]) ? $telefone[4] : '',
			'Representante' => isset($representante['Representante']) ? $representante['Representante'] : '',
			'Telefone' => isset($representante['Telefone']) ? $representante['Telefone'] : '',
			'EmailRepresentante' => isset($representante['Email']) ?$representante['Email'] : ''
		);

		 echo json_encode($dados);
	}

	
}