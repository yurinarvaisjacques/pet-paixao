<?php 
defined('BASEPATH') OR exit('No direct script access allowed');


/**
 * Pets
 * 
 * Utilizado na manipulação de informações dos pets.
 * @author Yuri Jacques
 */
class Pets extends CI_Controller{
	
	
	function __construct(){
		parent::__construct();
		if (!$this->Apl_model->chkLogin()) {
			redirect("login");
		}
	}

	public function index(){
		$dados = array();
		$dados['lembrete'] = $this->Apl_model->getAtendimentosDiaAtual();
		$dados['proprietario'] = $this->Apl_model->BuscarProprietario();
		$this->load->view('default/top');
		$this->load->view('default/navbar', $dados);
		$this->load->view('pets', $dados);
		$this->load->view('default/bot');
	}

	public function Cadastrar(){
		$this->load->library('form_validation');
		$this->form_validation->set_rules('nome', 'Nome', 'trim|required');
		$this->form_validation->set_rules('proprietario', 'Proprietario', 'trim|required');
		$this->form_validation->set_rules('porte', 'Porte', 'trim|required');
		$this->form_validation->set_rules('especie', 'Espécie', 'trim|required');
		$this->form_validation->set_rules('idade', 'Idade', 'trim');
		$this->form_validation->set_rules('pelagem', 'Pelagem', 'trim|required');
		$this->form_validation->set_rules('cor', 'Cor', 'trim');
		$this->form_validation->set_rules('castrado', 'Castrado', 'trim|required');
		$this->form_validation->set_rules('sexo', 'Sexo', 'trim|required');

		if ($this->form_validation->run() == false) {
            $response = array(
                'status' => 'error',
                'message' => validation_errors()
            );
        }
        else {
            $dados = array(
				'Nome' => $this->input->post('nome'),
				'Porte' => $this->input->post('porte'),
				'Especie' => $this->input->post('especie'),
				'Idade' => $this->input->post('idade'),
				'Pelagem' => $this->input->post('pelagem'),
				'Cor' => $this->input->post('cor'),
				'Castrado' => $this->input->post('castrado'),
				'Sexo' => $this->input->post('sexo')
			);
			
			$id = $this->Apl_model->insertPet($dados);

			$proprietarios = array(
				'FK_ID_Pet' => $id,
				'FK_ID_Pessoa' => $this->input->post('proprietario')
			);

			$this->Apl_model->insertProprietarioPet($proprietarios);


            $response = array(
                'status' => 'success',
                'message' => 'Informações salvas com sucesso'
            );
		}

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($response));
	}

	public function Atualizar(){
		$this->load->library('form_validation');
		$this->form_validation->set_rules('nome1', 'Nome', 'trim|required');
		$this->form_validation->set_rules('porte1', 'Porte', 'trim|required');
		$this->form_validation->set_rules('especie1', 'Espécie', 'trim|required');
		$this->form_validation->set_rules('idade1', 'Idade', 'trim');
		$this->form_validation->set_rules('pelagem1', 'Pelagem', 'trim|required');
		$this->form_validation->set_rules('cor1', 'Cor', 'trim');
		$this->form_validation->set_rules('castrado1', 'Castrado', 'trim|required');
		$this->form_validation->set_rules('sexo1', 'Sexo', 'trim|required');

		if ($this->form_validation->run() == false) {
            $response = array(
                'status' => 'error',
                'message' => validation_errors()
            );
        }
        else {
            $dados = array(
				'Nome' => $this->input->post('nome1'),
				'Porte' => $this->input->post('porte1'),
				'Especie' => $this->input->post('especie1'),
				'Idade' => $this->input->post('idade1'),
				'Pelagem' => $this->input->post('pelagem1'),
				'Cor' => $this->input->post('cor1'),
				'Castrado' => $this->input->post('castrado1'),
				'Sexo' => $this->input->post('sexo1')
			);
			
			$this->Apl_model->updatePet($dados, $_POST['idpet']);
	
			// $proprietarios = array(
			// 	'FK_ID_Pet' => $id,
			// 	'FK_ID_Pessoa' => $this->input->post('proprietario')
			// );

			// $this->Apl_model->insertProprietarioPet($proprietarios);


            $response = array(
                'status' => 'success',
                'message' => 'Informações salvas com sucesso'
            );
		}

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($response));
	}

	public function Buscar(){
		$dados = array();
		$dados['pets'] = $this->Apl_model->getPets();
		$dados['lembrete'] = $this->Apl_model->getAtendimentosDiaAtual();	
		$this->load->view('default/top');
		$this->load->view('default/navbar', $dados);
		$this->load->view('modal/infoPet');
		$this->load->view('modal/editarPet');
		$this->load->view('listarPets', $dados);
		$this->load->view('default/bot');
	}

	public function json_visualizarpet($ID_Pet){
		$pet = $this->Apl_model->getPet($ID_Pet);
		
		$dados = array(
			'ID_Pet' => isset($pet['ID_Pet']) ? $pet['ID_Pet'] : '',
			'Nome' => isset($pet['Nome']) ? $pet['Nome'] : '',
			'Porte' => isset($pet['Porte']) ? $pet['Porte'] : '',
			'Especie' => isset($pet['Especie']) ? $pet['Especie'] : '',
			'Idade' => isset($pet['Idade']) ? $pet['Idade'] : '',
			'Pelagem' => isset($pet['Pelagem']) ? $pet['Pelagem'] : '',
			'Cor' => isset($pet['Cor']) ? $pet['Cor'] : '',
			'Castrado' => isset($pet['Castrado']) ? $pet['Castrado'] : '',
			'Sexo' => isset($pet['Sexo']) ? $pet['Sexo'] : ''
		);

		 echo json_encode($dados);
	}

}