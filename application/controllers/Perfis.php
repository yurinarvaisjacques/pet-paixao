<?php 
defined('BASEPATH') OR exit('No direct script access allowed');


/**
 * Perfis
 * 
 * Página principal do sistema utilizado para apresentar informações.
 * @author Yuri Jacques
 */
class  Perfis extends CI_Controller{
	
	function __construct(){
		parent::__construct();
		if (!$this->Apl_model->chkLogin()) {
			redirect("login");
		}
		
	}

	public function index(){
        $dados = array();
        $dados['perfis'] = $this->Apl_model->getPerfis();$dados = array();
		$dados['lembrete'] = $this->Apl_model->getAtendimentosDiaAtual();
		$this->load->view('default/top');
		$this->load->view('default/navbar'$dados);
		$this->load->view('perfis', $dados);
		$this->load->view('default/bot');
	}


}