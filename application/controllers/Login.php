<?php 
defined('BASEPATH') OR exit('No direct script access allowed');


/**
 * Login
 * 
 * Utilizado para verificar usuário que estão se logando
 * @author Yuri Jacques
 */
class Login extends CI_Controller{
	
	function __construct(){
		parent::__construct();
	}

	public function index(){
		$this->load->view('default/top');
		$this->load->view('login');
		$this->load->view('default/login_bot');
	}

	function logar(){

		$this->load->library('form_validation');
		$this->form_validation->set_rules('username', 'Nome', 'trim|required');
		$this->form_validation->set_rules('password', 'Senha', 'trim|required');

		if ($this->form_validation->run() == false) {
            $response = array(
                'status' => 'error',
                'message' => 'Login ou Senha inválidos.'
            );
        }
        else {
			if(isset($_POST['username'])){
				$d = $this->Apl_model->logar($_POST['username'], md5($_POST['password']));
				if($d == true){
					$response = array(
						'status' => 'success',
						'message' => 'Sucesso ao Logar.'
					);			
				}else{
					$response = array(
						'status' => 'error',
						'message' => 'Login ou Senha inválidos.'
					);
				}
			}else{
				$response = array(
					'status' => 'error',
					'message' => 'Login ou Senha inválidos.'
				);
			}
		}

        $this->output
            ->set_content_type('application/json')
			->set_output(json_encode($response));
	}

	/**
	* Faz o login utilizando a API do Google.
	*/
	public function login_google() {
		
		unset($_SESSION[session_prefix]);
		
		$this->load->model("default/login_google_model");

		if (!isset($_GET['code'])) {
			
			$url = $this->login_google_model->createUrl();
			redirect($url);

		}
		else {

			$session_google = $this->login_google_model->checkLogin($_GET['code']);
			if ($session_google) {
				$newdata = array(
					'NomeUsuario' => $session_google['name'],
					'EmailUsuario'   => $session_google['email'],
					'Foto' => $session_google['picture']
				);

				$this->session->set_userdata($newdata);
				$page = 'main';
			}
			else {
				$_SESSION[session_prefix]['msg_erro_login'] = 'Ocorreu um erro, tente novamente.';
			}

			if (!isset($page)) $page = 'login';
			redirect($page);

		}
		
	}
	
	/**
	* Quebra a sessão do sistema
	*/
	public function sair() {
		$this->session->sess_destroy();
		redirect('login');
	}


}