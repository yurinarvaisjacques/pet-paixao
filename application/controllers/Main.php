<?php 
defined('BASEPATH') OR exit('No direct script access allowed');


/**
 * Main
 * 
 * Página principal do sistema utilizado para apresentar informações.
 * @author Yuri Jacques
 */
class  Main extends CI_Controller{
	
	function __construct(){
		parent::__construct();
		if (!$this->Apl_model->chkLogin()) {
			redirect("login");
		}
		
	}

	public function index(){
		$dados = array();
		$dados['lembrete'] = $this->Apl_model->getAtendimentosDiaAtual();
		$dados['receber'] = $this->Apl_model->receber();
		$dados['caixa'] = $this->Apl_model->caixa();
		$this->load->view('default/top');
		$this->load->view('default/navbar', $dados);
		$this->load->view('main');
		$this->load->view('default/bot');
	}


}