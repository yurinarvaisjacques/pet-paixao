<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Consultas
 * 
 * Página principal do sistema utilizado para apresentar informações.
 * @author Yuri Jacques
 */
class  Consultas extends CI_Controller{
	
	function __construct(){
		parent::__construct();
		if (!$this->Apl_model->chkLogin()) {
			redirect("login");
		}
		
	}

	public function index(){
		$dados = array();
		$dados['servico'] = $this->Apl_model->getServicoClinico();
		$dados['pets'] = $this->Apl_model->getPetProprietario();	
		$dados['horarios'] =$this->Apl_model->getLocalHorariosConsulta('C');
		$dados['lembrete'] = $this->Apl_model->getAtendimentosDiaAtual();	
		$this->load->view('default/top');
		$this->load->view('default/navbar', $dados);
		$this->load->view('consultas', $dados);
		$this->load->view('default/bot');
	}

	public function BanhoETosa(){
		$dados = array();
		$dados['servico'] = $this->Apl_model->getServicoEstetico();
		$dados['pets'] =  $this->Apl_model->getPetProprietario();		
		$dados['horarios'] =$this->Apl_model->getLocalHorariosConsulta('E');
		$dados['lembrete'] = $this->Apl_model->getAtendimentosDiaAtual();	
		$this->load->view('default/top');
		$this->load->view('default/navbar', $dados);
		$this->load->view('banhoetosa', $dados);
		$this->load->view('default/bot');
	}

	public function Vacina(){
		$dados = array();
		$dados['servico'] = $this->Apl_model->getServicoVacinacao();
		$dados['pets'] = $this->Apl_model->getPetProprietario();	
		$dados['horarios'] =$this->Apl_model->getLocalHorariosConsulta('V');
		$dados['lembrete'] = $this->Apl_model->getAtendimentosDiaAtual();	
		$this->load->view('default/top');
		$this->load->view('default/navbar', $dados);
		$this->load->view('vacina', $dados);
		$this->load->view('default/bot');
	}

	public function Cadastrar(){
		$this->load->library('form_validation');
		$this->form_validation->set_rules('servico', 'Servico', 'trim|required');
		$this->form_validation->set_rules('pet', 'Pet', 'trim|required');
		$this->form_validation->set_rules('hora', 'Data e Hora', 'trim|required');
		$this->form_validation->set_rules('obs', 'Observação', 'trim');

		if ($this->form_validation->run() == false) {
            $response = array(
                'status' => 'error',
                'message' => validation_errors()
            );
        }
        else {
            $dados = array(
				'FK_ID_Servico' => $this->input->post('servico'),
				'fk_id_pet' => $this->input->post('pet'),
				'horario_atendimento' => $this->input->post('hora'),
				'observacao' => $this->input->post('obs')
			);

			$this->Apl_model->insertAtendimento($dados);

			$response = array(
                'status' => 'success',
                'message' => 'Informações atualizadas com sucesso'
			);
		}

		$this->output
		->set_content_type('application/json')
		->set_output(json_encode($response));
	}

	public function BuscarConsulta(){
		$dados = array();
		$dados['title'] = 'Consultas';
		$dados['atendimentos'] = $this->Apl_model->getAtendimentosConsultas();
		$dados['lembrete'] = $this->Apl_model->getAtendimentosDiaAtual();
		$this->load->view('default/top');
		$this->load->view('default/navbar', $dados);
		$this->load->view('modal/infoConsulta');
		$this->load->view('modal/editarConsulta');
		$this->load->view('modal/registrarAtendimento');
		$this->load->view('listarConsultas', $dados);
		$this->load->view('default/bot');
	}

	public function BuscarBanhoTosa(){
		$dados = array();
		$dados['title'] = 'Banho & Tosa';
		$dados['atendimentos'] = $this->Apl_model->getAtendimentosBanhoTosa();
		$dados['lembrete'] = $this->Apl_model->getAtendimentosDiaAtual();
		$this->load->view('default/top');
		$this->load->view('default/navbar', $dados);
		$this->load->view('modal/infoConsulta');
		$this->load->view('modal/editarConsulta');
		$this->load->view('modal/registrarAtendimento');
		$this->load->view('listarConsultas', $dados);
		$this->load->view('default/bot');
	}

	public function BuscarVacina(){
		$dados = array();
		$dados['title'] = 'Vacinação';
		$dados['atendimentos'] = $this->Apl_model->getAtendimentosVacinas();
		$dados['lembrete'] = $this->Apl_model->getAtendimentosDiaAtual();
		$this->load->view('default/top');
		$this->load->view('default/navbar', $dados);
		$this->load->view('modal/infoConsulta');
		$this->load->view('modal/editarConsulta');
		$this->load->view('modal/registrarAtendimento');
		$this->load->view('listarConsultas', $dados);
		$this->load->view('default/bot');
	}

	public function json_visualizarAtendimento($ID_Atendimento){
		$consulta = $this->Apl_model->getAtendimento($ID_Atendimento);
		
		$dados = array(
			'Atendimento' => isset($consulta['ID_Atendimento']) ? $consulta['ID_Atendimento'] : '',
			'ID_Pessoa' => isset($consulta['ID_Pessoa']) ? $consulta['ID_Pessoa'] : '',
			'Servico' => isset($consulta['Servico']) ? $consulta['Servico'] : '',
			'Pet' => isset($consulta['Pet']) ? $consulta['Pet'] : '',
			'horario_atendimento' => isset($consulta['horario_atendimento']) ? $consulta['horario_atendimento'] : '',
			'Proprietario' => isset($consulta['Proprietario']) ? $consulta['Proprietario'] : '',
			'Telefone' => isset($consulta['Telefone']) ? $consulta['Telefone'] : '',
			'Valor' => isset($consulta['Valor']) ? $consulta['Valor'] : '',
			'Obs'	=> isset($consulta['observacao']) ? $consulta['observacao'] : 'Nenhuma Observação'
		);

		 echo json_encode($dados);
	}

	public function json_inativarAtendimento($ID_Atendimento){
		$dados = array(
			'ativo' => 'N'
		);

		$verificacao = $this->Apl_model->updateAtendimentosInativar($dados, $ID_Atendimento);

		if($verificacao != true){
			$response = array(
				'status' => 'error',
				'message' => 'Atendimento não foi removido'
			);
		}else{
			$response = array(
				'status' => 'success',
				'message' => 'Atendimento removido com sucesso'
			);
		}
		

		$this->output
		->set_content_type('application/json')
		->set_output(json_encode($response));
	}

}