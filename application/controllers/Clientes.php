<?php 
defined('BASEPATH') OR exit('No direct script access allowed');


/**
 * Clientes
 * 
 * Utilizado na manipulação de informações dos clientes.
 * @author Yuri Jacques
 */
class  Clientes extends CI_Controller{
	
	function __construct(){
		parent::__construct();
		$this->load->model('Apl_model');
		if (!$this->Apl_model->chkLogin()) {
			redirect("login");
		}
	}

	public function index(){
		$dados = array();
		$dados['lembrete'] = $this->Apl_model->getAtendimentosDiaAtual();
		$this->load->view('default/top');
		$this->load->view('default/navbar', $dados);
		$this->load->view('clientes');
		$this->load->view('default/bot');
	}

	public function Cadastrar(){

		$this->load->library('form_validation');
		$this->form_validation->set_rules('nome', 'Nome', 'trim|required');
		$this->form_validation->set_rules('rg', 'Rg', 'trim|required');
		$this->form_validation->set_rules('cpf', 'Cpf', 'trim|required');
		$this->form_validation->set_rules('email', 'E-mail', 'trim|required|valid_email');
		$this->form_validation->set_rules('nascimento', 'Nascimento', 'trim');
		$this->form_validation->set_rules('endereco', 'Endereco', 'trim|required');
		$this->form_validation->set_rules('numero', 'Numero', 'trim|required');
		$this->form_validation->set_rules('cep', 'Cep', 'trim|required');
		$this->form_validation->set_rules('bairro', 'Bairro', 'trim|required');
		$this->form_validation->set_rules('municipio', 'Municipio', 'trim|required');
		$this->form_validation->set_rules('celular', '1° Celular', 'trim|required');
		$this->form_validation->set_rules('celular1', '2° Celular', 'trim');
		$this->form_validation->set_rules('celular2', '3° Celular', 'trim');
		$this->form_validation->set_rules('celular3', '4° Celular', 'trim');

		if ($this->form_validation->run() == false) {
            $response = array(
                'status' => 'error',
                'message' => validation_errors()
            );
        }
        else {
            $dados = array(
				'Nome' => $this->input->post('nome'),
				'CPF' => $this->input->post('cpf'),
				'RG' => $this->input->post('rg'),
				'Email' => $this->input->post('email'),
				'DataNascimento' => $this->input->post('nascimento'),
				'Rua' => $this->input->post('endereco'),
				'Numero' => $this->input->post('numero'),
				'Bairro' => $this->input->post('bairro'),
				'Cidade' => $this->input->post('municipio'),
				'CEP' => $this->input->post('cep')
            );

			$id = $this->Apl_model->insertPessoa($dados);
			
			$cliente = array(
				'FK_ID_Pessoa' => $id, 
				'FK_ID_Perfis' => 3
			);

			$this->Apl_model->insertPerfilPessoa($cliente);
			
			if($this->input->post('celular') != NULL){
				$celular = array(
					'Telefone' => $this->input->post('celular'), 
					'FK_ID_Pessoa' => $id
				);
				$this->Apl_model->insertTelefonePessoa($celular);
			}

			if($this->input->post('celular1') != NULL){
				$celular = array(
					'Telefone' => $this->input->post('celular1'), 
					'FK_ID_Pessoa' => $id
				);
				$this->Apl_model->insertTelefonePessoa($celular);
			}

			if($this->input->post('celular2') != NULL){
				$celular = array(
					'Telefone' => $this->input->post('celular2'), 
					'FK_ID_Pessoa' => $id
				);
				$this->Apl_model->insertTelefonePessoa($celular);
			}

			if($this->input->post('celular3') != NULL){
				$celular = array(
					'Telefone' => $this->input->post('celular3'), 
					'FK_ID_Pessoa' => $id
				);
				$this->Apl_model->insertTelefonePessoa($celular);
			}

            $response = array(
                'status' => 'success',
                'message' => 'Informações salvas com sucesso'
            );
		}

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($response));
	}

	public function Atualizar(){

		$this->load->library('form_validation');
		$this->form_validation->set_rules('nome1', 'Nome', 'trim|required');
		$this->form_validation->set_rules('rg1', 'Rg', 'trim|required');
		$this->form_validation->set_rules('cpf1', 'Cpf', 'trim|required');
		$this->form_validation->set_rules('email1', 'E-mail', 'trim|required|valid_email');
		$this->form_validation->set_rules('nascimento1', 'Nascimento', 'trim');
		$this->form_validation->set_rules('rua1', 'Rua', 'trim|required');
		$this->form_validation->set_rules('numero1', 'Numero', 'trim|required');
		$this->form_validation->set_rules('cep1', 'Cep', 'trim|required');
		$this->form_validation->set_rules('bairro1', 'Bairro', 'trim|required');
		$this->form_validation->set_rules('cidade1', 'Cidade', 'trim|required');
		$this->form_validation->set_rules('celular11', '1° Celular', 'trim');
		$this->form_validation->set_rules('celular21', '2° Celular', 'trim');
		$this->form_validation->set_rules('celular31', '3° Celular', 'trim');
		$this->form_validation->set_rules('celular41', '4° Celular', 'trim');

		$id = $this->Apl_model->getID_Pessoa($this->input->post('cpf1'));
		
		if ($this->form_validation->run() == false) {
            $response = array(
                'status' => 'error',
                'message' => validation_errors()
            );
        }
        else {
            $dados = array(
				'Nome' => $this->input->post('nome1'),
				'CPF' => $this->input->post('cpf1'),
				'RG' => $this->input->post('rg1'),
				'Email' => $this->input->post('email1'),
				'DataNascimento' => $this->input->post('nascimento1'),
				'Rua' => $this->input->post('rua1'),
				'Numero' => $this->input->post('numero1'),
				'Bairro' => $this->input->post('bairro1'),
				'Cidade' => $this->input->post('cidade1'),
				'CEP' => $this->input->post('cep1')
			);

			$this->Apl_model->updatePessoa($dados, $id);

            $response = array(
                'status' => 'success',
                'message' => 'Informações atualizadas com sucesso'
			);
			
		}


        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($response));
	}

	public function Buscar(){
		$dados = array();
		$dados['clientes'] = $this->Apl_model->getClientes();
		$dados['lembrete'] = $this->Apl_model->getAtendimentosDiaAtual();	
		$this->load->view('default/top');
		$this->load->view('default/navbar', $dados);
		$this->load->view('modal/infoCliente');
		$this->load->view('modal/editarCliente');
		$this->load->view('listarClientes', $dados);
		$this->load->view('default/bot');
	}

	public function json_visualizarcliente($ID_Pessoa){
		$cliente = $this->Apl_model->getCliente($ID_Pessoa);
		$i = 1;
		foreach ($this->Apl_model->getTelefonePessoa($ID_Pessoa) as $value) {
			$telefone[$i] = $value['Telefone'];
			$i++;
		}

		$dados = array(
			'Nome' => isset($cliente['Nome']) ? $cliente['Nome'] : '',
			'CPF' => isset($cliente['CPF']) ? $cliente['CPF'] : '',
			'RG' => isset($cliente['RG']) ? $cliente['RG'] : '',
			'Email' => isset($cliente['Email']) ? $cliente['Email'] : '',
			'DataNascimento' => isset($cliente['DataNascimento']) ? $cliente['DataNascimento'] : '',
			'Rua' => isset($cliente['Rua']) ? $cliente['Rua'] : '',
			'Numero' => isset($cliente['Numero']) ? $cliente['Numero'] : '',
			'Bairro' => isset($cliente['Bairro']) ? $cliente['Bairro'] : '',
			'Cidade' => isset($cliente['Cidade']) ? $cliente['Cidade'] : '',
			'CEP' => isset($cliente['CEP']) ? $cliente['CEP'] : '',
			'Celular1' => isset($telefone[1]) ? $telefone[1] : '',
			'Celular2' => isset($telefone[2]) ? $telefone[2] : '',
			'Celular3' => isset($telefone[3]) ? $telefone[3] : '',
			'Celular4' => isset($telefone[4]) ? $telefone[4] : ''
		);

		 echo json_encode($dados);
	}

}