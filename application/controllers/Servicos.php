<?php 
defined('BASEPATH') OR exit('No direct script access allowed');


/**
 * Serviços
 * 
 * Utilizado na manipulação de informações dos serviços prestados na Clinica.
 * @author Yuri Jacques
 */
class  Servicos extends CI_Controller{
	
	function __construct(){
		parent::__construct();
		if (!$this->Apl_model->chkLogin()) {
			redirect("login");
		}
	}

	public function index(){
		$dados = array();
		$dados['lembrete'] = $this->Apl_model->getAtendimentosDiaAtual();
		$this->load->view('default/top');
		$this->load->view('default/navbar', $dados);
		$this->load->view('servicos');
		$this->load->view('default/bot');
    }
    
    public function Cadastrar(){
		$this->load->library('form_validation');
		$this->form_validation->set_rules('nome', 'Nome', 'trim|required');
		$this->form_validation->set_rules('descricao', 'Descrição', 'trim|required');
		$this->form_validation->set_rules('valor', 'Valor', 'trim|required');
		$this->form_validation->set_rules('tipo_servico', 'Tipo Serviço', 'trim|required');

		if ($this->form_validation->run() == false) {
            $response = array(
                'status' => 'error',
                'message' => validation_errors()
            );
        }
        else {
            $dados = array(
				'Nome' => $this->input->post('nome'),
				'Descricao' => $this->input->post('descricao'),
				'Valor' => $this->input->post('valor'),
				'Tipo_Servico' => $this->input->post('tipo_servico')
            );

            $this->Apl_model->insertServico($dados);

            $response = array(
                'status' => 'success',
                'message' => 'Informações salvas com sucesso'
            );
		}

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($response));
    }

	public function Atualizar(){
		$this->load->library('form_validation');
		$this->form_validation->set_rules('nome', 'Nome', 'trim|required');
		$this->form_validation->set_rules('descricao', 'Descrição', 'trim|required');
		$this->form_validation->set_rules('valor', 'Valor', 'trim|required');
		$this->form_validation->set_rules('tipo_servico', 'Tipo Serviço', 'trim|required');

		if ($this->form_validation->run() == false) {
            $response = array(
                'status' => 'error',
                'message' => validation_errors()
            );
        }
        else {
            $dados = array(
				'Nome' => $this->input->post('nome'),
				'Descricao' => $this->input->post('descricao'),
				'Valor' => $this->input->post('valor'),
				'Tipo_Servico' => $this->input->post('tipo_servico')
            );

           $this->Apl_model->updateServico($dados, $_POST['idservico']);

            $response = array(
                'status' => 'success',
                'message' => 'Informações salvas com sucesso'
            );
		}

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($response));
	}

    public function Buscar(){
		$dados = array();
		$dados['servicos'] = $this->Apl_model->getServicos();
		$dados['lembrete'] = $this->Apl_model->getAtendimentosDiaAtual();	
		$this->load->view('default/top');
		$this->load->view('default/navbar', $dados);
		$this->load->view('modal/infoServico');
		$this->load->view('modal/editarServico');
		$this->load->view('listarServicos', $dados);
		$this->load->view('default/bot');
	}

	public function json_visualizarServico($ID_Servico){
		$servico = $this->Apl_model->getServico($ID_Servico);

		$dados = array(
			'Nome' => isset($servico['Nome']) ? $servico['Nome'] : '',
			'Descricao' => isset($servico['Descricao']) ? $servico['Descricao'] : '',
			'Valor' => isset($servico['Valor']) ? $servico['Valor'] : '',
			'Tipo_Servico' => isset($servico['Tipo_Servico']) ? $servico['Tipo_Servico'] : ''
		);

		 echo json_encode($dados);
	}
    
}