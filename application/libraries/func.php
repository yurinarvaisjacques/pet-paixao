<?php

function calc_age($date) {
	
	$date = new DateTime( $date ); // data e hora de nascimento
	$interval = $date->diff( new DateTime( ) ); // data e hora atual
	return $interval->format('%Y');
	
}

function echo_flush($str = null) {

	if ($str != null) echo $str.'<br/>';
	@ob_flush();
	@flush();
	
}

function between($str, $str1, $str2) {
	
	@list ($lixo, $str) = @explode($str1, $str, 2);
	@list ($str, $lixo) = @explode($str2, $str, 2);
	return trim($str);
	
}

function pre($array, $exit = false) {
	
	echo '<pre>';
	print_r($array);
	echo '</pre>';
	if ($exit) exit;
	
}

function is_date($str) {
		
	if (empty($str)) return false;
	
	$e = explode(' ', trim($str));
	if (strpos($e[0], '-') !== false)
		list($aa, $mm, $dd) = explode('-', $e[0]);
	else
		list($dd, $mm, $aa) = explode('/', $e[0]);
	
	if ($aa < 1800) return false;
	
	return (checkdate($mm , $dd, $aa));
	
}

function is_mail($mail) {
		
	return preg_match('/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/', $mail) == 1;
	
}

function is_cpf($valor) {
	
	$valor = preg_replace("/[^0-9]/", "", $valor);

	$cpf_val = str_split($valor);
	
	array_splice($cpf_val, 9);

	$d1 = 0;

	foreach($cpf_val as $i=>$c)
		$d1+= intval($c) * (10 - ($i));

	$cpf_val[] = ($d1 % 11) < 2 ? 0 : 11 - ($d1 % 11);
	
	$d1 = 0;
	
	foreach($cpf_val as $i=>$c)
		$d1+= intval($c) * (11 - ($i));
		
	$cpf_val[] = ($d1 % 11) < 2 ? 0 : 11 - ($d1 % 11);

	$array_invalid = array();

	for($i=0;$i<=9;$i++) {

		$array_invalid[] = "";

		for($j=0;$j<11;$j++)
			$array_invalid[$i].=$i."";
	}
	
	return implode("", $cpf_val) == $valor && strlen($valor) == 11 && !in_array($valor, $array_invalid);
}

function is_md5($md5) {
		
	return preg_match('/^[a-f0-9]{32}$/i', $md5);
	
}

function is_ajax_request() {
	return !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
}

function date_us($str) {

	if (strpos($str, ' ') !== false) {
		list($date, $time) = explode(' ', $str);
		list($dd, $mm, $aa) = explode('/', $date);
		return $aa.'-'.$mm.'-'.$dd.' '.$time.':00';
	}
	else if (strpos($str, '/') !== false) {
		list($dd, $mm, $aa) = explode('/', $str);
		return $aa.'-'.$mm.'-'.$dd;
	}
	else {
		return $str;
	}

}

function date_br($str) {
	
	if (empty($str)) return '';
	
	@list($date, $time) = explode(' ', $str);
	list($aa, $mm, $dd) = explode('-', $date);
	$date = $dd.'/'.$mm.'/'.$aa;
	if (!empty($time)) $date .= ' '.substr($time, 0, 5);
	return $date;
	
}

function remove_accent($value) {

	$ret = "";
	$vetor_1 = array(192,193,194,195,196,197,199,200,201,202,203,204,205,206,207,210,211,212,213,214,217,218,219,220,224,225,226,227,228,229,231,232,233,234,235,236,237,238,239,242,243,244,245,246,249,250,251,252);
	$vetor_2 = array("A","A","A","A","A","A","C","E","E","E","E","I","I","I","I","O","O","O","O","O","U","U","U","U","a","a","a","a","a","a","c","e","e","e","e","i","i","i","i","o","o","o","o","o","u","u","u","u");
	for ($cont=0;$cont <= (strlen($value)-1);$cont++) {
		$ord = ord($value[$cont]);
		if (($ord<32) or ($ord>126)) {
			$varx = "";
			for ($contj=0;$contj <= (count($vetor_1));$contj++) {
				if ($ord == @$vetor_1[$contj])
					$varx = $vetor_2[$contj];
			}
			$ret .= $varx;
		}
		else
			$ret .= $value[$cont];
	}
	return $ret;
	
}

function parse_email($email) {
	
	$e = explode('@', $email, 2);
	return substr($e[0], 0, 4).'...@'.$e[1];
	
}

function mask_cpf($cpf) {

	return mask($cpf, '###.###.###-##');
	
}

function mask_cnpj($cnpj) {

	return mask($cnpj, '##.###.###/####-##');
	
}

function mask($val, $mask) {
	
	$val = preg_replace("/[^0-9]/", "", $val);
	
	$maskared = '';
	$k = 0;
	for ($i = 0;$i <= strlen($mask)-1;$i++) {
		
		if ($mask[$i] == '#') {
			if (isset($val[$k]))
				$maskared .= $val[$k++];
		}
		else {
			if (isset($mask[$i]))
				$maskared .= $mask[$i];
		}
	
	}
	return $maskared;
 
}

function currence($number) {
	
	return number_format($number, 2, ',', '.');
	
}

function download($filename, $force = true) {
	
	if (file_exists($filename)) {
		
		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Cache-Control: private", false);
		header("Content-Transfer-Encoding: binary");
		header("Content-Length: ". filesize($filename));
		header('Content-Type: application/octet-stream');
		// header('Content-Type: '. $arquivo["myme_type"]);

		if ($force) {
			header('Content-Disposition: attachment; filename="'. basename($filename) .'";');
		}

		return @readfile($filename);
		exit;
		
	}
	
}

function filename($file) {
	
	$filename = remove_accent(utf8_decode($file));
	$filename = strtolower($filename);
	$filename = str_replace('  ', ' ', $filename);
	$filename = str_replace(' ', '_', $filename);
	
	return $filename;
	
}
