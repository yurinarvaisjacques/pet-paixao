<div class="container-fluid">
    <div class="card o-hidden border-0 shadow-lg my-5">
        <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="p-5">
                        <div class="text-center">
                            <h1 class="h4 text-gray-900 mb-4">Cadastro Produto</h1>
                        </div>
                        <?php echo form_open('produtos/Cadastrar', array('id' => 'produtoForm','onsubmit' => 'return false')) ?>
                        <div class="form-group row">
                            <div class="col-sm-6 mb-3 mb-sm-0">
                                <input type="text" class="form-control" id="nomeproduto" name="nomeproduto"
                                    placeholder="Nome do Produto">
                            </div>
                            <div class="col-sm-6 mb-3 mb-sm-0">
                                <input type="text" class="form-control" id="nomeabreviado" name="nomeabreviado"
                                    placeholder="Nome do Produto Abreviado">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-4 mb-3 mb-sm-0">
                                <input type="text" class="form-control" id="quantidade" name="quantidade"
                                    placeholder="Quantidade" onfocus="calcular()">
                            </div>
                            <div class="col-sm-4 mb-3 mb-sm-0">
                                <input type="text" class="form-control" id="valorunitario" name="valorunitario"
                                    placeholder="Valor Unitario Ex.: 2.50" onblur="calcular()">
                            </div>
                            <div class="col-sm-4 mb-3 mb-sm-0">
                                <input type="text" class="form-control" id="valortotal" name="valortotal"
                                    placeholder="Valor Total" disabled>
                            </div>
                        </div>
                        <div class="form-group row">
                        <div class="col-sm-4 mb-3">
                                <input type="text" class="form-control" id="valorvenda" name="valorvenda"
                                    placeholder="Valor Venda Ex.: R$ 5.50">
                            </div>
                            <div class="col-sm-4 mb-3 mb-sm-0">
                                <input type="email" class="form-control" id="tipo" name="tipo" placeholder="Tipo">
                            </div>
                            <div class="col-sm-4 mb-3">
                                <input type="text" class="form-control" id="categoria" name="categoria"
                                    placeholder="Categoria">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-12">
                                <textarea class="form-control" aria-label="With textarea" id="descricao"
                                    name="descricao" placeholder="Descricao do Produto"></textarea>

                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-12 mb-3 mb-sm-0">
                                <select class="form-control" id="fornecedor" name="fornecedor"
                                    data-live-search="true">
                                    <option value="">Selecione o Fornecedor</option>
                                    <?php
                                        foreach ($fornecedor as $value) {
                                            echo "<option value='".$value['ID_Fornecedor']."'>".$value['NomeFantasia']."</option>";
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-12 mb-3">
                                <button type="submit" class="btn btn-success  btn-block">Cadastrar</button>
                            </div>
                        </div>
                    </div>
                    <hr>
                </div>
                <?php echo form_close() ?>
            </div>
        </div>
    </div>
</div>