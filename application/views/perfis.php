<div class="container-fluid">
<table id='perfisLista'  class="table table-striped table-bordered" style="width:100%">
  <thead>
    <tr>
      <th scope="col">Código</th>
      <th scope="col">Nome</th>
      <th scope="col">Descrição</th>
      <th scope="col">Ações</th>
    </tr>
  </thead>
  <tbody>
  <?php
  foreach ($perfis as $value) {
    echo '<tr>
    <th scope="row">'.$value['ID_Perfis'].'</th>
    <td>'.$value['Nome'].'</td>
    <td>'.$value['Descricao'].'</td>
    <td>
    <a title="Visualizar" onclick="janelaVisualizarCliente('.$value['ID_Perfis'].')" class="btn btn-outline-primary" href="javascript:;"><i class="fas fa-eye"></i></a>
    <a title="Editar" onclick="janelaEditarCliente('.$value['ID_Perfis'].')" class="btn btn-outline-primary" href="javascript:;"><i class="far fa-edit"></i></a>
    </td>
    </tr>';
    }
  ?>
  </tbody>
</table>
</div>
