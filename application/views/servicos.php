<div class="container-fluid">

    <div class="card o-hidden border-0 shadow-lg my-5">
        <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="p-5">
                        <div class="text-center">
                            <h1 class="h4 text-gray-900 mb-4">Cadastro Serviços</h1>
                        </div>
                        <?php echo form_open('servicos/Cadastrar', array('id' => 'servicoForm','onsubmit' => 'return false')) ?>
                        <div class="form-group row">
                            <div class="col-sm-12 mb-3 mb-sm-0">
                                <input type="text" class="form-control" id="nome" name="nome"
                                    placeholder="Nome do Serviço">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-12 mb-3 mb-sm-0">
                                <textarea class="form-control" aria-label="With textarea" id="descricao"
                                    name="descricao" placeholder="Descricao do Serviço"></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-6 mb-3 mb-sm-0">
                                <input type="text" class="form-control" id="valor" name="valor" placeholder="Valor">
                            </div>
                            <div class="col-sm-6 mb-3 mb-sm-0">
                                <select title="Campo Obrigatório" class="form-control" id="tipo_servico"
                                    name="tipo_servico">
                                    <option value="">Tipo Serviço</option>
                                    <option value="C">Clínico</option>
                                    <option value="E">Estético</option>
                                    <option value="V">Vacina</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-12 mb-3">
                                <button type="submit" class="btn btn-success  btn-block">Cadastrar</button>
                            </div>
                        </div>
                    </div>
                    <hr>
                </div>
                <?php echo form_close() ?>
            </div>
        </div>
    </div>
</div>