<div class="container-fluid">
    <div class="text-center">
        <h1 class="h4 text-gray-900 mb-4"><?php echo $title; ?></h1>
    </div>
    <table id='consultaLista' class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th scope="col">Código</th>
                <th scope="col">Serviço</th>
                <th scope="col">Pet</th>
                <th scope="col">Data e Hora</th>
                <th scope="col">Proprietario</th>
                <th scope="col">Telefone</th>
                <th scope="col">Ações</th>
            </tr>
        </thead>
        <tbody>
            <?php
  foreach ($atendimentos as $value) {
    echo '<tr>
    <th scope="row">'.$value['ID_Atendimento'].'</th>
    <td>'.$value['Servico'].'</td>
    <td>'.$value['Pet'].'</td>
    <td>'.$value['horario_atendimento'].'</td>
    <td>'.$value['Proprietario'].'</td>
    <td>'.$value['Telefone'].'</td>
    <td>
    <a title="Visualizar" onclick="janelaVisualizarAtendimento('.$value['ID_Atendimento'].')" class="btn btn-outline-primary" href="javascript:;"><i class="fas fa-eye"></i></a>
    ';
    if($title == "Consultas"){
        if($this->session->ID_Perfil != 4){
            echo '<a title="Registrar Consulta" onclick="janelaRegistrarAtendimento('.$value['ID_Atendimento'].')" class="btn btn-outline-primary" href="javascript:;"><i class="far fa-save"></i></a>';
        }
    }elseif ($title == "Vacinação") {
        echo '<a title="Registrar Vacinacao" onclick="janelaRegistrarAtendimento('.$value['ID_Atendimento'].')" class="btn btn-outline-primary" href="javascript:;"><i class="far fa-save"></i></a>';
    }else{
        if($this->session->ID_Perfil != 2){
            echo '<a title="Registar Banho e Tosa" onclick="janelaRegistrarAtendimento('.$value['ID_Atendimento'].')" class="btn btn-outline-primary" href="javascript:;"><i class="far fa-save"></i></a>';
        }
    }
    echo '
    <a title="Inativar Consulta" onclick="janelaInativarAtendimento('.$value['ID_Atendimento'].')" class="btn btn-outline-primary" href="javascript:;"><i class="fas fa-ban"></i></i></a>    
    </td>
    </tr>';
    }
  ?>
        </tbody>
    </table>
</div>