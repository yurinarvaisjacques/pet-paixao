<div class="container-fluid">
    <div class="text-center">
        <h1 class="h4 text-gray-900 mb-4">Fornecedores</h1>
    </div>
    <table id='fornecedorLista' class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th scope="col">Código</th>
                <th scope="col">Nome Fantasia</th>
                <th scope="col">Razão Social</th>
                <th scope="col">E-mail</th>
                <th scope="col">Ações</th>
            </tr>
        </thead>
        <tbody>
            <?php
  foreach ($fornecedores as $value) {
    echo '<tr>
    <th scope="row">'.$value['ID_Fornecedor'].'</th>
    <td>'.$value['NomeFantasia'].'</td>
    <td>'.$value['RazaoSocial'].'</td>
    <td>'.$value['Email'].'</td>
    <td>
    <a title="Visualizar" onclick="janelaVisualizarFornecedor('.$value['ID_Fornecedor'].')" class="btn btn-outline-primary" href="javascript:;" data-toggle="modal" data-target="#ModalFornecedorInfo"><i class="fas fa-eye"></i></a>
    <a title="Editar" onclick="janelaEditarFornecedor('.$value['ID_Fornecedor'].')" class="btn btn-outline-primary" href="javascript:;"><i class="far fa-edit"></i></a>
    </td>
    </tr>';
  }
    ?>
        </tbody>
    </table>
</div>