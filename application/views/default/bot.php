      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; Your Website 2019</span>
          </div>
        </div>
      </footer>
    </div>
  </div>

  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- JavaScript -->
  <!-- Bootstrap core JavaScript-->  
  <script src="<?=base_url('public/vendor/jquery/jquery.js');?>"></script>
  <script src="<?=base_url('public/vendor/bootstrap/js/bootstrap.bundle.js');?>"></script>
  <!-- include datepicker -->
  <script src="<?= base_url('public/bootstrap-datepicker.js'); ?>"></script>
  <script src="<?= base_url('public/bootstrap-datepicker.pt-BR.min.js'); ?>"></script>
  <!-- Core plugin JavaScript-->
  <script src="<?=base_url('public/vendor/jquery-easing/jquery.easing.js');?>"></script>
  <!-- Custom scripts for all pages-->
  <script src="<?=base_url('public/js/sb-admin-2.js');?>"></script>
  <!-- include alertify script -->
  <script src="<?=base_url('public/alertifyjs/alertify.js') ?>"></script>
  <!-- Ajax criados -->
  <script src="<?=base_url('public/ajax/cliente.js');?>"></script>
  <script src="<?=base_url('public/ajax/fornecedor.js');?>"></script>
  <script src="<?=base_url('public/ajax/pet.js');?>"></script>
  <script src="<?=base_url('public/ajax/produto.js');?>"></script>
  <script src="<?=base_url('public/ajax/servico.js');?>"></script>
  <script src="<?=base_url('public/ajax/perfil.js');?>"></script>
  <script src="<?=base_url('public/ajax/consulta.js');?>"></script>
  <script src="<?=base_url('public/ajax/default.js');?>"></script>
  <script src="<?=base_url('public/ajax/usuario.js');?>"></script>
  <script src="<?=base_url('public/ajax/financeiro.js');?>"></script>

  <!-- include javascript dataTables -->
  <script type="text/javascript" charset="utf8" src="<?=base_url('public/DataTables/DataTables/js/jquery.dataTables.js')?>"></script>
  <!-- include token input -->
  <script type="text/javascript" charset="utf8" src="<?=base_url('public/tokeninput/src/jquery.tokeninput.js')?>"></script>

  <script src="<?=base_url('public/bootstrap-select/dist/js/bootstrap-select.js')?>"></script>  
  <!-- JavaScript  END -->
</body>
</html>