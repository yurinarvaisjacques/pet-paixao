<?php defined('BASEPATH') OR exit('No direct script access allowed') ?>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Pet Paixão</title>

  <!-- CSS -->
  <!-- Custom fonts for this template-->
  <link href="<?= base_url('public/vendor/fontawesome-free/css/all.css'); ?>" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
  <!-- Custom styles for this template-->
  <link href="<?= base_url('public/css/sb-admin-2.css'); ?>" rel="stylesheet">
  <!-- include alertify.css -->
  <link rel="stylesheet" href="<?= base_url('public/alertifyjs/css/alertify.css'); ?>">
  <!-- include semantic ui theme  -->
  <link rel="stylesheet" href="<?= base_url('public/alertifyjs/css/themes/semantic.css'); ?>">
  <!-- include dataTables -->
  <link rel="stylesheet" type="text/css" href="<?=base_url('public/DataTables/DataTables/css/jquery.dataTables.css')?>">
  <!-- include tokeninput -->
  <link rel="stylesheet" type="text/css" href="<?=base_url('public/tokeninput/styles/token-input.css')?>">
  <link rel="stylesheet" type="text/css" href="<?=base_url('public/tokeninput/styles/token-input-facebook.css')?>">
  <!-- include datepicker -->
  <link href="<?= base_url('public/bootstrap-datepicker.css'); ?>" rel="stylesheet" type="text/css">
  
  <link href="<?= base_url('public/bootstrap-select/dist/css/bootstrap-select.css'); ?>" rel="stylesheet" type="text/css">
  <!-- CSS end -->

  <script>baseUrl = '<?= base_url(); ?>';</script>
  
</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">