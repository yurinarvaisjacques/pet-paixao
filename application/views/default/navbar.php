<?php if($this->session->ID_Perfil == 1){ ?>
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?= base_url('main'); ?>">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-paw"></i>
        </div>

        <div class="sidebar-brand-text mx-3">Sivepet</div>
    </a>

    <hr class="sidebar-divider my-0">

    <li class="nav-item active">
        <a class="nav-link" href="<?= base_url('main'); ?>">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span></a>
    </li>

    <hr class="sidebar-divider">
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true"
            aria-controls="collapseTwo">
            <i class="fas fa-file-signature"></i>
            <span>Cadastrar</span>
        </a>

        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item" href="<?= base_url('clientes') ?>">Cliente</a>
                <a class="collapse-item" href="<?= base_url('pets') ?>">Pets</a>
                <a class="collapse-item" href="<?= base_url('fornecedores') ?>">Fornecedores</a>
                <a class="collapse-item" href="<?= base_url('servicos') ?>">Serviços</a>
            </div>
        </div>
    </li>

    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities"
            aria-expanded="true" aria-controls="collapseUtilities">
            <i class="fas fa-fw fa-search"></i>
            <span>Buscar</span>
        </a>

        <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item" href="<?= base_url('clientes/buscar') ?>">Clientes</a>
                <a class="collapse-item" href="<?= base_url('pets/buscar') ?>">Pets</a>
                <a class="collapse-item" href="<?= base_url('fornecedores/buscar') ?>">Fornecedores</a>
                <a class="collapse-item" href="<?= base_url('servicos/buscar') ?>">Serviços</a>
            </div>
        </div>
    </li>

    <hr class="sidebar-divider">

    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true"
            aria-controls="collapsePages">
            <i class="fas fa-user-md"></i>
            <span>Marcar Procedimento</span>
        </a>

        <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item" href="<?= base_url('consultas') ?>">Consulta</a>
                <a class="collapse-item" href="<?= base_url('consultas/BanhoETosa') ?>">Banho e Tosa</a>
                <a class="collapse-item" href="<?= base_url('consultas/Vacina') ?>">Vacinas</a>
            </div>
        </div>
    </li>

    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseConsulta"
            aria-expanded="true" aria-controls="collapseConsulta">
            <i class="fas fa-user-md"></i>
            <span>Atendimentos</span>
        </a>

        <div id="collapseConsulta" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item" href="<?= base_url('consultas/BuscarConsulta') ?>">Consulta</a>
                <a class="collapse-item" href="<?= base_url('consultas/BuscarBanhoTosa') ?>">Banho e Tosa</a>
                <a class="collapse-item" href="<?= base_url('consultas/BuscarVacina') ?>">Vacinas</a>
            </div>
        </div>
    </li>
    <hr class="sidebar-divider">
    <li class="nav-item">
        <a class="nav-link" href="<?= base_url('usuarios') ?>">
            <i class="fas fa-users"></i>
            <span>Usuários</span></a>
    </li>

    <li class="nav-item">
        <a class="nav-link" href="<?= base_url('financeiro/Buscar') ?>">
            <i class="fas fa-cash-register"></i>
            <span>Financeiro</span></a>
    </li>

   

    <hr class="sidebar-divider d-none d-md-block">

    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>
<?php } ?>
<?php if($this->session->ID_Perfil == 2){ ?>
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?= base_url('main'); ?>">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-paw"></i>
        </div>

        <div class="sidebar-brand-text mx-3">Sivepet</div>
    </a>

    <hr class="sidebar-divider my-0">

    <li class="nav-item active">
        <a class="nav-link" href="<?= base_url('main'); ?>">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span></a>
    </li>

    <hr class="sidebar-divider">
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true"
            aria-controls="collapseTwo">
            <i class="fas fa-file-signature"></i>
            <span>Cadastrar</span>
        </a>

        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item" href="<?= base_url('clientes') ?>">Cliente</a>
                <a class="collapse-item" href="<?= base_url('pets') ?>">Pets</a>
            </div>
        </div>
    </li>

    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities"
            aria-expanded="true" aria-controls="collapseUtilities">
            <i class="fas fa-fw fa-search"></i>
            <span>Buscar</span>
        </a>

        <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item" href="<?= base_url('clientes/buscar') ?>">Clientes</a>
                <a class="collapse-item" href="<?= base_url('pets/buscar') ?>">Pets</a>
            </div>
        </div>
    </li>

    <hr class="sidebar-divider">

    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true"
            aria-controls="collapsePages">
            <i class="fas fa-user-md"></i>
            <span>Marcar Procedimento</span>
        </a>

        <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item" href="<?= base_url('consultas') ?>">Consulta</a>
                <a class="collapse-item" href="<?= base_url('consultas/Vacina') ?>">Vacinas</a>
            </div>
        </div>
    </li>

    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseConsulta"
            aria-expanded="true" aria-controls="collapseConsulta">
            <i class="fas fa-user-md"></i>
            <span>Atendimentos</span>
        </a>

        <div id="collapseConsulta" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item" href="<?= base_url('consultas/BuscarConsulta') ?>">Consulta</a>
                <a class="collapse-item" href="<?= base_url('consultas/BuscarVacina') ?>">Vacinas</a>
            </div>
        </div>
    </li>
    <hr class="sidebar-divider">
    <li class="nav-item">
        <a class="nav-link" href="#" title="Em desenvolvimento">
            <i class="fas fa-fw fa-chart-area"></i>
            <span>Relatórios</span></a>
    </li>

    <hr class="sidebar-divider d-none d-md-block">

    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>
<?php } ?>
<?php if($this->session->ID_Perfil == 3){ ?>
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?= base_url('main'); ?>">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-paw"></i>
        </div>

        <div class="sidebar-brand-text mx-3">Sivepet</div>
    </a>

    <hr class="sidebar-divider my-0">

    <li class="nav-item active">
        <a class="nav-link" href="<?= base_url('main'); ?>">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span></a>
    </li>

    <hr class="sidebar-divider">
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" title="Em desenvolvimento" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true"
            aria-controls="collapseTwo">
            <i class="fas fa-file-signature"></i>
            <span>Relatórios</span>
        </a>

    </li>
    <hr class="sidebar-divider d-none d-md-block">

    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>
<?php } ?>
<?php if($this->session->ID_Perfil == 4){ ?>
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?= base_url('main'); ?>">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-paw"></i>
        </div>

        <div class="sidebar-brand-text mx-3">Sivepet</div>
    </a>

    <hr class="sidebar-divider my-0">

    <li class="nav-item active">
        <a class="nav-link" href="<?= base_url('main'); ?>">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span></a>
    </li>

    <hr class="sidebar-divider">
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true"
            aria-controls="collapseTwo">
            <i class="fas fa-file-signature"></i>
            <span>Cadastrar</span>
        </a>

        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item" href="<?= base_url('clientes') ?>">Cliente</a>
                <a class="collapse-item" href="<?= base_url('pets') ?>">Pets</a>
                <a class="collapse-item" href="<?= base_url('fornecedores') ?>">Fornecedores</a>
            </div>
        </div>
    </li>

    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities"
            aria-expanded="true" aria-controls="collapseUtilities">
            <i class="fas fa-fw fa-search"></i>
            <span>Buscar</span>
        </a>

        <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item" href="<?= base_url('clientes/buscar') ?>">Clientes</a>
                <a class="collapse-item" href="<?= base_url('pets/buscar') ?>">Pets</a>
                <a class="collapse-item" href="<?= base_url('fornecedores/buscar') ?>">Fornecedores</a>
            </div>
        </div>
    </li>

    <hr class="sidebar-divider">

    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true"
            aria-controls="collapsePages">
            <i class="fas fa-user-md"></i>
            <span>Marcar Procedimento</span>
        </a>

        <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item" href="<?= base_url('consultas') ?>">Consulta</a>
                <a class="collapse-item" href="<?= base_url('consultas/BanhoETosa') ?>">Banho e Tosa</a>
                <a class="collapse-item" href="<?= base_url('consultas/Vacina') ?>">Vacinas</a>
            </div>
        </div>
    </li>

    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseConsulta"
            aria-expanded="true" aria-controls="collapseConsulta">
            <i class="fas fa-user-md"></i>
            <span>Atendimentos</span>
        </a>

        <div id="collapseConsulta" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item" href="<?= base_url('consultas/BuscarConsulta') ?>">Consulta</a>
                <a class="collapse-item" href="<?= base_url('consultas/BuscarBanhoTosa') ?>">Banho e Tosa</a>
                <a class="collapse-item" href="<?= base_url('consultas/BuscarVacina') ?>">Vacinas</a>
            </div>
        </div>
    </li>
    <hr class="sidebar-divider">
    <li class="nav-item">
        <a class="nav-link" href="#" title="Em desenvolvimento">
            <i class="fas fa-fw fa-chart-area"></i>
            <span>Relatórios</span></a>
    </li>

    <hr class="sidebar-divider d-none d-md-block">

    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>
<?php } ?>
<div id="content-wrapper" class="d-flex flex-column">
    <div id="content">

        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">
            <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                <i class="fa fa-bars"></i>
            </button>
            <ul class="navbar-nav ml-auto">
            <?php if($this->session->ID_Perfil != 3){ ?>
                <li class="nav-item dropdown no-arrow mx-1">
                    <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-bell fa-fw"></i>
                        <span class="badge badge-danger badge-counter"><?php echo sizeof($lembrete) ?></span>
                    </a>
                    <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in"
                        aria-labelledby="alertsDropdown">
                        <h6 class="dropdown-header">
                            Alerts Center
                        </h6>
                        <?php
                foreach ($lembrete as $value) {
                
                echo '<a class="dropdown-item d-flex align-items-center" href="#">
                        <div class="mr-3">
                          <div class="icon-circle bg-primary">
                            <i class="fas fa-file-alt text-white"></i>
                          </div>
                        </div>
                        <div>
                          <div class="small text-gray-500">'.$value['horario_atendimento'].'</div>
                          <span class="font-weight-bold">'.$value['Proprietario'].' - '.$value['Pet'].'</span>
                          <div class="mediun text-gray-500">'.$value['Servico'].'</div>
                          </div>
                      </a>';
                }
                ?>
                        <a class="dropdown-item text-center small text-gray-500" href="#">Show All Alerts</a>
                    </div>
                </li>
              <?php } ?>
                <div class="topbar-divider d-none d-sm-block"></div>
                <li class="nav-item dropdown no-arrow">
                    <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">
                        <?php echo '<span class="mr-2 d-none d-lg-inline text-gray-600 small">'.$this->session->NomeUsuario.'</span>'; ?>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in"
                        aria-labelledby="userDropdown">
                        
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="<?= base_url('login/sair') ?>">
                            <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                            Logout
                        </a>
                    </div>
                </li>
            </ul>
        </nav>