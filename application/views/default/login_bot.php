  <!-- JavaScript -->
  <!-- Bootstrap core JavaScript-->  
  <script src="<?=base_url('public/vendor/jquery/jquery.js');?>"></script>
  <script src="<?=base_url('public/vendor/bootstrap/js/bootstrap.bundle.js');?>"></script>
  <!-- Core plugin JavaScript-->
  <script src="<?=base_url('public/vendor/jquery-easing/jquery.easing.js');?>"></script>
  <!-- Custom scripts for all pages-->
  <script src="<?=base_url('public/js/sb-admin-2.js');?>"></script>
  <!-- include alertify script -->
  <script src="<?=base_url('public/alertifyjs/alertify.js') ?>"></script>
  <!-- Ajax criados -->
  <script src="<?=base_url('public/ajax/default.js');?>"></script>
  <!-- JavaScript  END -->