<div class="container-fluid">
  <div class="card o-hidden border-0 shadow-lg my-5">
    <div class="card-body p-0">
      <!-- Nested Row within Card Body -->
      <div class="row">
        <div class="col-lg-12">
          <div class="p-5">
            <div class="text-center">
              <h1 class="h4 text-gray-900 mb-4">Cadastro Fornecedores</h1>
            </div>
            <?php echo form_open('fornecedores/Cadastrar', array('id' => 'fornecedorForm','onsubmit' => 'return false')) ?>
              <div class="form-group row">
                <div class="col-sm-6 mb-3 mb-sm-0">
                  <input type="text" title="Campo Obrigatório" class="form-control form-control-user" id="nomefantasia" name="nomefantasia" placeholder="Nome Fantasia">
                </div>
                <div class="col-sm-6 mb-3">
                  <input type="text" title="Campo Obrigatório" class="form-control form-control-user" id="razaosocial" name="razaosocial" placeholder="Razão Social">
                </div>
              </div>
              <div class="form-group row">
                <div class="col-sm-3 mb-3 mb-sm-0">
                  <input type="text" title="Campo Obrigatório" class="form-control form-control-user" id="cnpj" name="cnpj" placeholder="CNPJ">
                </div>
                <div class="col-sm-3 mb-3">
                  <input type="text" title="Campo Opcional" class="form-control form-control-user" id="inscricaoestadual" name="inscricaoestadual" placeholder="Inscrição Estadual">
                </div>
                <div class="col-sm-6">
                  <input type="email" title="Campo Obrigatório" class="form-control form-control-user" id="email" name="email" placeholder="E-mail">
                </div>
              </div>
              <div class="form-group row">
                <div class="col-sm-6 mb-3 mb-sm-0">
                  <input type="text" title="Campo Obrigatório" class="form-control form-control-user" id="endereco" name="endereco" placeholder="Endereço">
                </div>
                <div class="col-sm-3 mb-3">
                  <input type="text" title="Campo Obrigatório" class="form-control form-control-user" id="numero" name="numero" placeholder="Número">
                </div>
                <div class="col-sm-3">
                  <input type="text" title="Campo Obrigatório" class="form-control form-control-user" id="cep" name="cep" placeholder="Cep">
                </div>
              </div>
              <div class="form-group row">
                <div class="col-sm-4 mb-3 mb-sm-0">
                  <input type="text" title="Campo Obrigatório" class="form-control form-control-user" id="bairro" name="bairro" placeholder="Bairro">
                </div>
                <div class="col-sm-4 mb-3">
                  <input type="text" title="Campo Obrigatório" class="form-control form-control-user" id="municipio" name="municipio" placeholder="Municipio">
                </div>
                <div class="col-sm-4">
                    <input type="text" title="Campo Obrigatório" class="form-control" id="celular" name="celular" placeholder="1° Celular">
                  </div>
              </div>
              <div class="form-group row">
                  <div class="col-sm-4">
                    <input type="text" title="Campo Opcional" class="form-control" id="celular1" name="celular1" placeholder="2° Celular" >
                  </div>
                  <div class="col-sm-4">
                    <input type="text" title="Campo Opcional" class="form-control" id="celular2" name="celular2" placeholder="3° Celular" >
                  </div>
                  <div class="col-sm-4">
                    <input type="text" title="Campo Opcional" class="form-control" id="celular3" name="celular3" placeholder="4° Celular" >
                  </div>
              </div>
              <div class="form-group row">
                  <div class="col-sm-4">
                    <input type="text" title="Campo Opcional" class="form-control" id="representante" name="representante" placeholder="Nome do Representante" >
                  </div>
                  <div class="col-sm-4">
                    <input type="text" title="Campo Opcional" class="form-control" id="celularrepresentante" name="celularrepresentante" placeholder="Celular do Representante" >
                  </div>
                  <div class="col-sm-4">
                    <input type="email" title="Campo Opcional" class="form-control" id="emailrepresentante" name="emailrepresentante" placeholder="E-mail do Representante" >
                  </div>
              </div>
              <div class="form-group row">
                <div class="col-sm-12 mb-3">
                <button type="submit" class="btn btn-success  btn-block">Cadastrar</button>
                </div>
              </div>
              <hr>
              <?php echo form_close() ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>