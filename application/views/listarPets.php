<div class="container-fluid">
    <div class="text-center">
        <h1 class="h4 text-gray-900 mb-4">Pets</h1>
    </div>
    <table id='petLista' class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th scope="col">Nome Pet</th>
                <th scope="col">Especie</th>
                <th scope="col">Proprietário</th>
                <th scope="col">Ações</th>
            </tr>
        </thead>
        <tbody>
            <?php
  foreach ($pets as $value) {
    echo '<tr>
    <th scope="row">'.$value['Nome'].'</th>
    <td>'.$value['Especie'].'</td>
    <td>'.$value['Proprietario'].'</td>
    <td>
    <a title="Visualizar" onclick="janelaVisualizarPet('.$value['ID_Pet'].')" class="btn btn-outline-primary" href="javascript:;"><i class="fas fa-eye"></i></a>
    <a title="Editar" onclick="janelaEditarPet('.$value['ID_Pet'].')" class="btn btn-outline-primary" href="javascript:;"><i class="far fa-edit"></i></a>
    </td>
    </tr>';
    }
  ?>
        </tbody>
    </table>
</div>