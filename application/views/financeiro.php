<div class="container-fluid">
    <div class="text-center">
        <h1 class="h4 text-gray-900 mb-4">Financeiro</h1>
    </div>
    <table id='vendasLista' class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th scope="col">Cliente</th>
                <th scope="col">Valor Parcela</th>
                <th scope="col">Valor Total</th>
                <th scope="col">Parcela</th>
                <th scope="col">Numero Parcelas</th>
                <th scope="col">Forma Pagamento</th>
                <th scope="col">Pago</th>
                <th scope="col">Data Pagamento</th>
                <th scope="col">Ações</th>
            </tr>
        </thead>
        <tbody>
            <?php
  foreach ($vendas as $value) {
    echo '<tr>
    <td scope="row">'.$value['Cliente'].'</td>
    <td>'.$value['Valor_Parcela'].'</td>
    <td>'.$value['Valor_Total'].'</td>
    <td>'.$value['Parcela'].'</td>
    <td>'.$value['Numero_Parcelas'].'</td>
    <td>'.$value['Forma_Pagamento'].'</td>
    <td>'.$value['Pago'].'</td>
    <td>'.$value['Data_Pagamento'].'</td>
    <td>';

    if($value['Pago'] != 'S'){
        echo '<a title="Confirmar Pagamento" onclick="janelaConfirmarPagamento('.$value['ID_Venda'].')" class="btn btn-outline-primary" href="javascript:;"><i class="far fa-save"></i></a>';
    }
    echo '</td></tr>';
    }
  ?>
        </tbody>
    </table>
</div>