<body class="bg-gradient-primary">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-xl-10 col-lg-12 col-md-9">
        <div class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0">
            <div class="row">
            <br>
            <br>
            <br>
            <br>
              <div class="col-lg-6 d-none d-lg-block bg-login-image"></div>
              <div class="col-lg-6">
                <div class="p-5">
                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">Seja Bem-Vindo!</h1>
                  </div>
                  <?php echo form_open('login/logar', array('id' => 'loginForm','onsubmit' => 'return false')) ?>
                    <div class="form-group">
                      <input type="text" class="form-control form-control-user" id="username" name="username" placeholder="Login">
                    </div>
                    <div class="form-group">
                      <input type="password" class="form-control form-control-user" id="password" name="password" placeholder="Senha">
                    </div>
                    <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-user btn-block" >Login</button>                     
                    </div>
                    <br>
                    <br>
                    <br>
                    <br>
                    <?php echo form_close() ?>
                  <hr>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</body>