<div class="container-fluid">
  <div class="card o-hidden border-0 shadow-lg my-5">
    <div class="card-body p-0">
      <div class="row">
        <div class="col-lg-12">
          <div class="p-5">
            <div class="text-center">
              <h1 class="h4 text-gray-900 mb-4">Marcar Consultas</h1>
            </div>
            <?php echo form_open('consultas/Cadastrar', array('id' => 'consultaForm','onsubmit' => 'return false')) ?>
            <div class="form-group row">
              <div class="col-sm-12 mb-3 mb-sm-0">
                <select class="form-control" id="servico" name="servico" data-live-search="true">
                  <option value="">Selecione o Serviço</option>
                  <?php 
                    foreach ($servico as $value) {
                      echo "<option value='".$value['ID_Servico']."'>".$value['Nome']."</option>";
                    } 
                  ?>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-sm-12 mb-3 mb-sm-0">
                <select class="form-control" id="pet" name="pet" data-live-search="true">
                  <option value="">Selecione o Pet</option>
                  <?php 
                    foreach ($pets as $value) {
                      echo "<option value='".$value['ID_Pet']."'>".$value['Pet']."</option>";
                    } 
                  ?>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-sm-12 mb-3 mb-sm-0">
                <select class="form-control" id="hora" name="hora" data-live-search="true">
                  <option value="">Selecione o Horario</option>
                  <?php 
                    foreach ($horarios as $value) {
                      echo "<option value='".$value['data_consulta']." - ".$value['hora']." - ".$value['dia_nome']."'>".$value['data_consulta']." - ".$value['hora']." - ".$value['dia_nome']."</option>";
                    } 
                  ?>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-sm-12 mb-3 mb-sm-0">
                <textarea class="form-control" aria-label="With textarea" id="obs" name="obs" placeholder="Observação"></textarea>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-sm-12 mb-3">
                <button type="submit" class="btn btn-success  btn-block">Cadastrar</button>
              </div>
            </div>
          </div>
        </div>
        <?php echo form_close() ?>
      </div>
    </div>
  </div>
</div>
