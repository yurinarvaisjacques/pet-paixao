<div class="container-fluid">
    <div class="text-center">
        <h1 class="h4 text-gray-900 mb-4">Serviços</h1>
    </div>
    <table id='servicoLista' class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th scope="col">Código</th>
                <th scope="col">Nome</th>
                <th scope="col">Valor</th>
                <th scope="col">Tipo</th>
                <th scope="col">Ações</th>
            </tr>
        </thead>
        <tbody>
            <?php
  foreach ($servicos as $value) {
    echo '<tr>
    <th scope="row">'.$value['ID_Servico'].'</th>
    <td>'.$value['Nome'].'</td>
    <td>'.$value['Valor'].'</td>
    <td>'.$value['Tipo_Servico'].'</td>
    <td>
    <a title="Visualizar" onclick="janelaVisualizarServico('.$value['ID_Servico'].')" class="btn btn-outline-primary" href="javascript:;"><i class="fas fa-eye"></i></a>
    <a title="Editar" onclick="janelaEditarServico('.$value['ID_Servico'].')" class="btn btn-outline-primary" href="javascript:;"><i class="far fa-edit"></i></a>
    </td>
    </tr>';
    }
  ?>
        </tbody>
    </table>
</div>