<div class="container-fluid">
    <div class="text-center">
        <h1 class="h4 text-gray-900 mb-4">Clientes</h1>
    </div>
    <table id='clienteLista' class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th scope="col">Código</th>
                <th scope="col">Nome</th>
                <th scope="col">E-mail</th>
                <th scope="col">Ações</th>
            </tr>
        </thead>
        <tbody>
            <?php
  foreach ($clientes as $value) {
    echo '<tr>
    <th scope="row">'.$value['ID_Pessoa'].'</th>
    <td>'.$value['Nome'].'</td>
    <td>'.$value['Email'].'</td>
    <td>
    <a title="Visualizar" onclick="janelaVisualizarCliente('.$value['ID_Pessoa'].')" class="btn btn-outline-primary" href="javascript:;"><i class="fas fa-eye"></i></a>
    <a title="Editar" onclick="janelaEditarCliente('.$value['ID_Pessoa'].')" class="btn btn-outline-primary" href="javascript:;"><i class="far fa-edit"></i></a>
    </td>
    </tr>';
    }
  ?>
        </tbody>
    </table>
</div>