<!-- Modal -->
<div class="modal fade bd-example-modal-xl" id="ModalPetEdit" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Editar Informações do Pet</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php echo form_open('pets/Atualizar', array('id' => 'petFormAtualizar','onsubmit' => 'return false')) ?>
            <div class="modal-body">
                <div class="form-group row">
                    <div class="col-md-2">Nome :</div>
                    <div class="col-md-4 ml-auto"><input type="text" class="form-control" id="nome1" name="nome1"></div>
                    <div class="col-md-2">Porte :</div>
                    <div class="col-md-4 ml-auto">
                        <select class="form-control" id="porte1" name="porte1">
                            <option value="">Porte</option>
                            <option value="P">Pequeno</option>
                            <option value="M">Médio</option>
                            <option value="G">Grande</option>
                        </select>
                    </div>
                </div>
                <hr>
                <div class="form-group row">
                    <div class="col-md-2">Especie :</div>
                    <div class="col-md-4 ml-auto"><input type="text" class="form-control" id="especie1" name="especie1">
                    </div>
                    <div class="col-md-2">Idade :</div>
                    <div class="col-md-4 ml-auto"><input type="text" class="form-control" id="idade1" name="idade1">
                    </div>
                </div>
                <hr>
                <div class="form-group row">
                    <div class="col-md-2">Pelagem :</div>
                    <div class="col-md-4 ml-auto">
                        <select class="form-control" id="pelagem1" name="pelagem1">
                            <option value="">Pelagem</option>
                            <option value="C">Curto</option>
                            <option value="M">Médio</option>
                            <option value="L">Longo</option>
                        </select>
                    </div>
                    <div class="col-md-2">Cor :</div>
                    <div class="col-md-4 ml-auto"><input type="text" class="form-control" id="cor1" name="cor1"></div>
                </div>
                <hr>
                <div class="form-group row">
                    <div class="col-md-2">Castrado :</div>
                    <div class="col-md-4 ml-auto" >
                        <select class="form-control" id="castrado1" name="castrado1">
                            <option value="">Castrado</option>
                            <option value="S">Sim</option>
                            <option value="N">Não</option>
                        </select>
                    </div>
                    <div class="col-md-2">Sexo :</div>
                    <div class="col-md-4 ml-auto">
                        <select id="sexo1" name="sexo1">
                            <option value="">Sexo</option>
                            <option value="F">Femea</option>
                            <option value="M">Macho</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-4 ml-auto"><input type="hidden" class="form-control" id="idpet" name="idpet">
                </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success">Salvar</button>
                <button class="btn btn-danger" data-dismiss="modal">Cancelar</button>
            </div>
            <?php echo form_close() ?>
        </div>
    </div>
</div>