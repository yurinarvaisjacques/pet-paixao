<!-- Modal -->
<div class="modal fade bd-example-modal-xl" id="ModalUsuarioEditar" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Editar Informações do Usuario</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <?php echo form_open('usuarios/Atualizar', array('id' => 'usuarioFormAtualizar','onsubmit' => 'return false')) ?>
      <div class="modal-body">
        <div class="form-group row">
          <div class="col-md-2">Nome :</div>
          <div class="col-md-4 ml-auto"><input type="text" class="form-control" id="nome1" name="nome1"></div>
          <div class="col-md-2">CPF :</div>
          <div class="col-md-4 ml-auto"><input type="text" class="form-control" id="cpf1" name="cpf1"></div>
        </div>
        <hr>
        <div class="form-group row">
          <div class="col-md-2">RG :</div>
          <div class="col-md-4 ml-auto"><input type="text" class="form-control" id="rg1" name="rg1"></div>
          <div class="col-md-2">E-mail :</div>
          <div class="col-md-4 ml-auto"><input type="text" class="form-control" id="email1" name="email1"></div>
        </div>
        <hr>
        <div class="form-group row">
          <div class="col-md-2">Data Nascimento :</div>
          <div class="col-md-4 ml-auto"><input type="text" class="form-control" id="nascimento1" name="nascimento1"></div>
          <div class="col-md-2">Rua :</div>
          <div class="col-md-4 ml-auto"><input type="text" class="form-control" id="rua1" name="rua1"></div>
        </div>
        <hr>
        <div class="form-group row">
          <div class="col-md-2">Numero :</div>
          <div class="col-md-4 ml-auto"><input type="text" class="form-control" id="numero1" name="numero1"></div>
          <div class="col-md-2">Bairro :</div>
         <div class="col-md-4 ml-auto"><input type="text" class="form-control" id="bairro1" name="bairro1"></div>
        </div>
        <hr>
        <div class="form-group row">
         <div class="col-md-2">Cidade :</div>
        <div class="col-md-4 ml-auto"><input type="text" class="form-control" id="cidade1" name="cidade1"></div>
        <div class="col-md-2">CEP :</div>
       <div class="col-md-4 ml-auto"><input type="text" class="form-control" id="cep1" name="cep1"></div>
      </div>
      <hr>
      <div class="form-group row">
       <div class="col-md-2">1° Celular :</div>
       <div class="col-md-4 ml-auto"><input type="text" class="form-control" id="celular11" name="celular11"></div>
       <div class="col-md-2">2° Celular :</div>
       <div class="col-md-4 ml-auto"><input type="text" class="form-control" id="celular21" name="celular21"></div>
     </div>
     <hr>
      <div class="form-group row">
       <div class="col-md-2">3° Celular :</div>
       <div class="col-md-4 ml-auto"><input type="text" class="form-control" id="celular31" name="celular31"></div>
       <div class="col-md-2">4° Celular :</div>
       <div class="col-md-4 ml-auto"><input type="text" class="form-control" id="celular41" name="celular41"></div>
     </div>
     <hr>
     <div class="form-group row">
       <div class="col-md-2">Login :</div>
       <div class="col-md-4 ml-auto"><input type="text" class="form-control" id="login1" name="login1"></div>
       <div class="col-md-2">Senha :</div>
       <div class="col-md-4 ml-auto"><input type="password" class="form-control" id="senha1" name="senha1"></div>
     </div>
     <hr>
     <div class="form-group row">
       <div class="col-md-2">Perfil :</div>
       <div class="col-md-10 ml-auto">
            <select class="form-control" id="perfil1" name="perfil1">
                <option value="1">Administrador</option>
                <option value="2">Veterinário</option>
                <option value="3">Cliente</option>
                <option value="4">Atendente</option>
            </select>
     </div>
   </div>
   <div class="modal-footer">
    <button type="submit" class="btn btn-success" >Salvar</button>
    <button class="btn btn-danger" data-dismiss="modal">Cancelar</button>
  </div>
  <?php echo form_close() ?>
</div>
</div>
</div>

   </div>
