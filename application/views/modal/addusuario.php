<!-- Modal -->
<div class="modal fade bd-example-modal-xl" id="ModalUsuarioAdd" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Criar Novo Usuario</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <?php echo form_open('usuarios/Cadastrar', array('id' => 'usuaruioForm','onsubmit' => 'return false')) ?>
      <div class="modal-body">
        <div class="form-group row">
          <div class="col-md-2">Nome :</div>
          <div class="col-md-4 ml-auto"><input type="text" class="form-control" id="1nome" name="1nome"></div>
          <div class="col-md-2">CPF :</div>
          <div class="col-md-4 ml-auto"><input type="text" class="form-control" id="1cpf" name="1cpf"></div>
        </div>
        <hr>
        <div class="form-group row">
          <div class="col-md-2">RG :</div>
          <div class="col-md-4 ml-auto"><input type="text" class="form-control" id="1rg" name="1rg"></div>
          <div class="col-md-2">E-mail :</div>
          <div class="col-md-4 ml-auto"><input type="text" class="form-control" id="1email" name="1email"></div>
        </div>
        <hr>
        <div class="form-group row">
          <div class="col-md-2">Data Nascimento :</div>
          <div class="col-md-4 ml-auto"><input type="text" class="form-control" id="1nascimento" name="1nascimento"></div>
          <div class="col-md-2">Rua :</div>
          <div class="col-md-4 ml-auto"><input type="text" class="form-control" id="1rua" name="1rua"></div>
        </div>
        <hr>
        <div class="form-group row">
          <div class="col-md-2">Numero :</div>
          <div class="col-md-4 ml-auto"><input type="text" class="form-control" id="1numero" name="1numero"></div>
          <div class="col-md-2">Bairro :</div>
         <div class="col-md-4 ml-auto"><input type="text" class="form-control" id="1bairro" name="1bairro"></div>
        </div>
        <hr>
        <div class="form-group row">
         <div class="col-md-2">Cidade :</div>
        <div class="col-md-4 ml-auto"><input type="text" class="form-control" id="1cidade" name="1cidade"></div>
        <div class="col-md-2">CEP :</div>
       <div class="col-md-4 ml-auto"><input type="text" class="form-control" id="1cep" name="1cep"></div>
      </div>
      <hr>
      <div class="form-group row">
       <div class="col-md-2">1° Celular :</div>
       <div class="col-md-4 ml-auto"><input type="text" class="form-control" id="1celular1" name="1celular1"></div>
       <div class="col-md-2">2° Celular :</div>
       <div class="col-md-4 ml-auto"><input type="text" class="form-control" id="1celular2" name="1celular2"></div>
     </div>
     <hr>
      <div class="form-group row">
       <div class="col-md-2">3° Celular :</div>
       <div class="col-md-4 ml-auto"><input type="text" class="form-control" id="1celular3" name="1celular3"></div>
       <div class="col-md-2">4° Celular :</div>
       <div class="col-md-4 ml-auto"><input type="text" class="form-control" id="1celular4" name="1celular4"></div>
     </div>
     <hr>
     <div class="form-group row">
       <div class="col-md-2">Login :</div>
       <div class="col-md-4 ml-auto"><input type="text" class="form-control" id="1login" name="1login"></div>
       <div class="col-md-2">Senha :</div>
       <div class="col-md-4 ml-auto"><input type="password" class="form-control" id="1senha" name="1senha"></div>
     </div>
     <hr>
     <div class="form-group row">
       <div class="col-md-2">Perfil :</div>
       <div class="col-md-10 ml-auto">
            <select class="form-control" id="1perfil" name="1perfil">
                <option value="">Perfil</option>
                <option value="1">Administrador</option>
                <option value="2">Veterinário</option>
                <option value="3">Cliente</option>
                <option value="4">Atendente</option>
            </select>
     </div>
   </div>
   <div class="modal-footer">
    <button type="submit" class="btn btn-success" >Salvar</button>
    <button class="btn btn-danger" data-dismiss="modal">Cancelar</button>
  </div>
  <?php echo form_close() ?>
</div>
</div>
</div>

   </div>
