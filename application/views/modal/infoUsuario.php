<!-- Modal -->
<div class="modal fade bd-example-modal-xl" id="ModalUsuarioInfo" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Visualizar Informações do Cliente</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-group row">
          <div class="col-md-2">Nome :</div>
          <div class="col-md-4 ml-auto"><input type="text" class="form-control" disabled  id="nome" name="nome"></div>
          <div class="col-md-2">CPF :</div>
          <div class="col-md-4 ml-auto"><input type="text" class="form-control"  disabled  id="cpf" name="cpf"></div>
        </div>
        <hr>
        <div class="form-group row">
          <div class="col-md-2">RG :</div>
          <div class="col-md-4 ml-auto"><input type="text" class="form-control"  disabled  id="rg" name="rg"></div>
          <div class="col-md-2">E-mail :</div>
          <div class="col-md-4 ml-auto"><input type="text" class="form-control"  disabled  id="email" name="email"></div>
        </div>
        <hr>
        <div class="form-group row">
          <div class="col-md-2">Data Nascimento :</div>
          <div class="col-md-4 ml-auto"><input type="text" class="form-control"  disabled  id="nascimento" name="nascimento"></div>
          <div class="col-md-2">Rua :</div>
          <div class="col-md-4 ml-auto"><input type="text" class="form-control"  disabled  id="rua" name="rua"></div>
        </div>
        <hr>
        <div class="form-group row">
          <div class="col-md-2">Numero :</div>
          <div class="col-md-4 ml-auto"><input type="text" class="form-control"  disabled  id="numero" name="numero"></div>
          <div class="col-md-2">Bairro :</div>
         <div class="col-md-4 ml-auto"><input type="text" class="form-control"  disabled  id="bairro" name="bairro"></div>
        </div>
        <hr>
        <div class="form-group row">
         <div class="col-md-2">Cidade :</div>
        <div class="col-md-4 ml-auto"><input type="text" class="form-control"  disabled  id="cidade" name="cidade"></div>
        <div class="col-md-2">CEP :</div>
       <div class="col-md-4 ml-auto"><input type="text" class="form-control"  disabled  id="cep" name="cep"></div>
      </div>
      <hr>
      <div class="form-group row">
       <div class="col-md-2">1° Celular :</div>
       <div class="col-md-4 ml-auto"><input type="text" class="form-control"  disabled  id="celular1" name="celular1"></div>
       <div class="col-md-2">2° Celular :</div>
       <div class="col-md-4 ml-auto"><input type="text" class="form-control"  disabled  id="celular2" name="celular2"></div>
     </div>
     <hr>
      <div class="form-group row">
       <div class="col-md-2">3° Celular :</div>
       <div class="col-md-4 ml-auto"><input type="text" class="form-control"  disabled  id="celular3" name="celular3"></div>
       <div class="col-md-2">4° Celular :</div>
       <div class="col-md-4 ml-auto"><input type="text" class="form-control"  disabled  id="celular4" name="celular4"></div>
     </div>
     <hr>
      <div class="form-group row">
       <div class="col-md-2">Login :</div>
       <div class="col-md-4 ml-auto"><input type="text" class="form-control"  disabled  id="login" name="login"></div>
       <div class="col-md-2">Senha :</div>
       <div class="col-md-4 ml-auto"><input type="password" class="form-control"  disabled  id="senha" name="senha"></div>
     </div>
     <div class="form-group row">
       <div class="col-md-2">Perfil :</div>
       <div class="col-md-10 ml-auto">
            <select class="form-control" id="perfil" name="perfil" disabled>
                <option value="1">Administrador</option>
                <option value="2">Veterinário</option>
                <option value="3">Cliente</option>
                <option value="4">Atendente</option>
            </select>
     </div>
   </div>
   </div>
   <div class="modal-footer">
    <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Fechar</button>
  </div>
</div>
</div>
</div>
