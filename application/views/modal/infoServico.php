<!-- Modal -->
<div class="modal fade bd-example-modal-xl" id="ModalServicoInfo" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Visualizar Informações do Serviço</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group row">
                    <div class="col-sm-12 mb-3 mb-sm-0">
                        <input type="text" class="form-control" id="nome1" name="nome1" placeholder="Nome do Serviço" disabled>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-12 mb-3 mb-sm-0">
                        <textarea class="form-control" aria-label="With textarea" id="descricao1" name="descricao1" placeholder="Descricao do Serviço" disabled></textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-6 mb-3 mb-sm-0">
                        <input type="text" class="form-control" id="valor1" name="valor1" placeholder="Valor1" disabled>
                    </div>
                    <div class="col-sm-6 mb-3 mb-sm-0">
                        <select class="form-control" id="tipo_servico1" name="tipo_servico1" disabled>
                            <option value="">Tipo Serviço</option>
                            <option value="C">Clínico</option>
                            <option value="E">Estético</option>
                            <option value="V">Vacina</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Fechar</button>
            </div> 
        </div>
    </div>
</div>
