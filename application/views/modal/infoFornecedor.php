<!-- Modal -->
<div class="modal fade bd-example-modal-xl" id="ModalFornecedorInfo" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Visualizar Informações do Fornecedor</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-group row">
          <div class="col-md-2">Nome Fantasia:</div>
          <div class="col-md-4 ml-auto"><input type="text" class="form-control" id="nomefantasia" name="nomefantasia" disabled></div>
          <div class="col-md-2">Razão Social :</div>
          <div class="col-md-4 ml-auto"><input type="text" class="form-control" id="razaosocial" name="razaosocial" disabled></div>
        </div>
        <hr>
        <div class="form-group row">
          <div class="col-md-2">CNPJ :</div>
          <div class="col-md-4 ml-auto"><input type="text" class="form-control" id="cnpj" name="cnpj" disabled></div>
          <div class="col-md-2">Inscrição Estatual :</div>
          <div class="col-md-4 ml-auto"><input type="text" class="form-control" id="inscricaoestadual" name="inscricaoestadual" disabled></div>
        </div>
        <hr>
        <div class="form-group row">
          <div class="col-md-2">E-mail :</div>
          <div class="col-md-4 ml-auto"><input type="text" class="form-control" id="email" name="email" disabled></div>
          <div class="col-md-2">Rua :</div>
          <div class="col-md-4 ml-auto"><input type="text" class="form-control" id="rua" name="rua" disabled></div>
        </div>
        <hr>
        <div class="form-group row">
          <div class="col-md-2">Numero :</div>
          <div class="col-md-4 ml-auto"><input type="text" class="form-control" id="numero" name="numero" disabled></div>
          <div class="col-md-2">Bairro :</div>
         <div class="col-md-4 ml-auto"><input type="text" class="form-control" id="bairro" name="bairro" disabled></div>
        </div>
        <hr>
        <div class="form-group row">
         <div class="col-md-2">Cidade :</div>
        <div class="col-md-4 ml-auto"><input type="text" class="form-control" id="cidade" name="cidade" disabled></div>
        <div class="col-md-2">CEP :</div>
       <div class="col-md-4 ml-auto"><input type="text" class="form-control" id="cep" name="cep" disabled></div>
      </div>
      <hr>
      <div class="form-group row">
       <div class="col-md-2">1° Celular :</div>
       <div class="col-md-4 ml-auto"><input type="text" class="form-control" id="celular1" name="celular1" disabled></div>
       <div class="col-md-2">2° Celular :</div>
       <div class="col-md-4 ml-auto"><input type="text" class="form-control" id="celular2" name="celular2" disabled></div>
     </div>
     <hr>
      <div class="form-group row">
       <div class="col-md-2">3° Celular :</div>
       <div class="col-md-4 ml-auto"><input type="text" class="form-control" id="celular3" name="celular3" disabled></div>
       <div class="col-md-2">4° Celular :</div>
       <div class="col-md-4 ml-auto"><input type="text" class="form-control" id="celular4" name="celular4" disabled></div>
     </div>
     <hr>
      <div class="form-group row">
       <div class="col-md-2">Representante :</div>
       <div class="col-md-4 ml-auto"><input type="text" class="form-control" id="representante" name="representante" disabled></div>
       <div class="col-md-2">Celular Representante :</div>
       <div class="col-md-4 ml-auto"><input type="text" class="form-control" id="celularrepresentante" name="celularrepresentante" disabled></div>
     </div>
     <hr>
     <div class="form-group row">
       <div class="col-md-2">E-mail Representante :</div>
       <div class="col-md-4 ml-auto"><input type="text" class="form-control" id="emailrepresentante" name="emailrepresentante" disabled></div>
     </div>
   </div>
   <div class="modal-footer">
    <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Fechar</button>
  </div>
</div>
</div>
</div>
