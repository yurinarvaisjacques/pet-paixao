<!-- Modal -->
<div class="modal fade bd-example-modal-xl" id="ModalConsultaInfo" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Visualizar Informações do Atendimento</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group row">
                    <div class="col-md-2">Serviço :</div>
                    <div class="col-md-4 ml-auto"><input type="text" class="form-control" disabled id="servico"
                            name="servico"></div>
                    <div class="col-md-2">Pet :</div>
                    <div class="col-md-4 ml-auto"><input type="text" class="form-control" disabled id="pet" name="pet">
                    </div>
                </div>
                <hr>
                <div class="form-group row">
                    <div class="col-md-2">Data e Hora :</div>
                    <div class="col-md-4 ml-auto"><input type="text" class="form-control" disabled id="datahora"
                            name="datahora"></div>
                    <div class="col-md-2">Propeietário :</div>
                    <div class="col-md-4 ml-auto"><input type="text" class="form-control" disabled id="proprietarionome"
                            name="proprietarionome"></div>
                </div>
                <hr>
                <div class="form-group row">
                    <div class="col-md-2">Telefone :</div>
                    <div class="col-md-4 ml-auto"><input type="text" class="form-control" disabled id="telefone"
                            name="telefone"></div>
                    <div class="col-md-2">Valor :</div>
                    <div class="col-md-4 ml-auto"><input type="text" class="form-control" disabled id="valor"
                            name="valor"></div>
                </div>
                <div class="form-group row">
                    <div class="col-md-2">Observação :</div>
                    <div class="col-md-10 ml-auto">
                        <textarea class="form-control" aria-label="With textarea" id="obs" name="obs"
                            disabled></textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>