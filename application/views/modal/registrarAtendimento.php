<!-- Modal -->
<div class="modal fade bd-example-modal-xl" id="ModalRegistrarAtendimento" tabindex="-1" role="dialog"
    aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Registrar Atendimento</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php echo form_open('financeiro/Registrar', array('id' => 'FormRegistrarAtendimento','onsubmit' => 'return false')) ?>
            <div class="modal-body">
                <div class="form-group row">
                    <div class="col-md-2">Nome Cliente:</div>
                    <div class="col-md-10 ml-auto"><input type="text" class="form-control" id="nome_pessoa"
                            name="nome_pessoa" disabled></div>
                </div>
                <hr>
                <div class="form-group row">
                    <div class="col-md-2">Valor Parcela :</div>
                    <div class="col-md-4 ml-auto"><input type="text" class="form-control" readonly="readonly"
                            id="valor_parcela" name="valor_parcela"></div>
                    <div class="col-md-2">Valor Total :</div>
                    <div class="col-md-4 ml-auto"><input type="text" class="form-control" id="valor_total"
                            name="valor_total" onfocus="calcular()" readonly="readonly"></div>
                </div>
                <hr>
                <div class="form-group row">
                    <div class="col-md-2">Número de Parcelas :</div>
                    <div class="col-md-4 ml-auto"><input type="text" class="form-control" id="numero_parcelas"
                            name="numero_parcelas" onblur="calcular()"> </div>
                    <div class="col-md-2">Forma Pagamento :</div>
                    <div class="col-md-4 ml-auto">
                        <select class="form-control" id="forma_pagamento" name="forma_pagamento">
                            <option value="">Forma Pagamento</option>
                            <option value="D">Dinheiro</option>
                            <option value="C">Cartao</option>
                            <option value="CH">Cheque</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-4 ml-auto">
                        <input type="hidden" class="form-control" id="atendimento" name="atendimento">
                    </div>
                    <div class="col-md-4 ml-auto">
                        <input type="hidden" class="form-control" id="idpessoa" name="idpessoa">
                    </div>
                </div>
                <?php if($this->session->ID_Perfil == 2 || $this->session->ID_Perfil == 1){ ?>
                <div class="form-group row">
                    <div class="col-md-2">Observação :</div>
                    <div class="col-md-10 ml-auto">
                        <textarea class="form-control" aria-label="With textarea" id="obs" name="obs"></textarea>
                    </div>
                </div>
                <?php } ?>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success">Registrar</button>
                <button class="btn btn-danger" data-dismiss="modal">Cancelar</button>
            </div>
            <?php echo form_close() ?>
        </div>
    </div>
</div>