<!-- Modal -->
<div class="modal fade bd-example-modal-xl" id="ModalPetInfo" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Visualizar Informações do Pet</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group row">
                    <div class="col-md-2">Nome :</div>
                    <div class="col-md-4 ml-auto"><input type="text" class="form-control" id="nome" name="nome"
                            disabled></div>
                    <div class="col-md-2">Porte :</div>
                    <div class="col-md-4 ml-auto">
                        <select class="form-control" id="porte" name="porte" disabled>
                            <option value="">Porte</option>
                            <option value="P">Pequeno</option>
                            <option value="M">Médio</option>
                            <option value="G">Grande</option>
                        </select>
                    </div>
                </div>
                <hr>
                <div class="form-group row">
                    <div class="col-md-2">Especie :</div>
                    <div class="col-md-4 ml-auto"><input type="text" class="form-control" id="especie" name="especie"
                            disabled></div>
                    <div class="col-md-2">Idade :</div>
                    <div class="col-md-4 ml-auto"><input type="text" class="form-control" id="idade" name="idade"
                            disabled></div>
                </div>
                <hr>
                <div class="form-group row">
                    <div class="col-md-2">Pelagem :</div>
                    <div class="col-md-4 ml-auto">
                        <select class="form-control" id="pelagem" name="pelagem" disabled>
                            <option value="">Pelagem</option>
                            <option value="C">Curto</option>
                            <option value="M">Médio</option>
                            <option value="L">Longo</option>
                        </select>
                    </div>
                    <div class="col-md-2">Cor :</div>
                    <div class="col-md-4 ml-auto"><input type="text" class="form-control" id="cor" name="cor" disabled>
                    </div>
                </div>
                <hr>
                <div class="form-group row">
                    <div class="col-md-2">Castrado :</div>
                    <div class="col-md-4 ml-auto">
                        <select class="form-control" id="castrado" name="castrado" disabled>
                            <option value="">Castrado</option>
                            <option value="S">Sim</option>
                            <option value="N">Não</option>
                        </select>
                    </div>
                    <div class="col-md-2">Sexo :</div>
                    <div class="col-md-4 ml-auto">
                        <select id="sexo" name="sexo" disabled>
                            <option value="">Sexo</option>
                            <option value="F">Femea</option>
                            <option value="M">Macho</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>