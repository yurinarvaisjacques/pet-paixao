<!-- Modal -->
<div class="modal fade bd-example-modal-xl" id="ModalConsultaEdit" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Editar Informações do Atendimento</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-group row">
          <div class="col-md-2">Serviço :</div>
          <div class="col-md-4 ml-auto"><input type="text" class="form-control" disabled  id="servico1" name="servico1"></div>
          <div class="col-md-2">Pet :</div>
          <div class="col-md-4 ml-auto"><input type="text" class="form-control"  disabled  id="pet1" name="pet1"></div>
        </div>
        <hr>
        <div class="form-group row">
          <div class="col-md-2">Data e Hora :</div>
          <div class="col-md-4 ml-auto"><input type="text" class="form-control"  disabled  id="datahora1" name="datahora1"></div>
          <div class="col-md-2">Propeietário :</div>
          <div class="col-md-4 ml-auto"><input type="text" class="form-control"  disabled  id="proprietarionome1" name="proprietarionome1"></div>
        </div>
        <hr>
        <div class="form-group row">
          <div class="col-md-2">Telefone :</div>
          <div class="col-md-4 ml-auto"><input type="text" class="form-control"  disabled  id="telefone1" name="telefone1"></div>
          <div class="col-md-2">Valor :</div>
          <div class="col-md-4 ml-auto"><input type="text" class="form-control"  disabled  id="valor1" name="valor1"></div>
        </div>
        <div class="form-group row">
          <div class="col-sm-12 mb-3 mb-sm-0">
            <textarea class="form-control" aria-label="With textarea" id="obs1" name="obs1" placeholder="Observação"></textarea>
          </div>
        </div>
   </div>
   <div class="modal-footer">
    <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Fechar</button>
  </div>
</div>
</div>
</div>
