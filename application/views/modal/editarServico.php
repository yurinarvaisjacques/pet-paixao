<!-- Modal -->
<div class="modal fade bd-example-modal-xl" id="ModalServicoEditar" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Editar Informações do Serviço</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php echo form_open('servicos/Atualizar', array('id' => 'servicoFormAtualizar','onsubmit' => 'return false')) ?>
            <div class="modal-body">
                <div class="form-group row">
                    <div class="col-sm-12 mb-3 mb-sm-0">
                        <input type="text" class="form-control" id="nome" name="nome" placeholder="Nome do Serviço">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-12 mb-3 mb-sm-0">
                        <textarea class="form-control" aria-label="With textarea" id="descricao" name="descricao" placeholder="Descricao do Serviço"></textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-6 mb-3 mb-sm-0">
                        <input type="text" class="form-control" id="valor" name="valor" placeholder="Valor">
                    </div>
                    <div class="col-sm-6 mb-3 mb-sm-0">
                        <select class="form-control" id="tipo_servico" name="tipo_servico">
                            <option value="">Tipo Serviço</option>
                            <option value="C">Clínico</option>
                            <option value="E">Estético</option>
                            <option value="V">Vacina</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-4 ml-auto"><input type="hidden" class="form-control" id="idservico" name="idservico"></div> 
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success" >Salvar</button>
                <button class="btn btn-danger" data-dismiss="modal">Cancelar</button>
            </div> 
        </div>
        <?php echo form_close() ?>
    </div>
</div>
