<div class="container-fluid">
    <div class="card o-hidden border-0 shadow-lg my-5">
        <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="p-5">
                        <div class="text-center">
                            <h1 class="h4 text-gray-900 mb-4">Cadastro Cliente</h1>
                        </div>
                        <?php echo form_open('clientes/Cadastrar', array('id' => 'clienteForm','onsubmit' => 'return false')) ?>
                        <div class="form-group row">
                            <div class="col-sm-6 mb-3 mb-sm-0">
                                <input type="text" title="Campo Obrigatório" class="form-control" id="nome" name="nome"
                                    placeholder="Nome Completo">
                            </div>
                            <div class="col-sm-3">
                                <input type="text" title="Campo Obrigatório" class="form-control" id="cpf" name="cpf"
                                    placeholder="CPF">
                            </div>
                            <div class="col-sm-3 mb-3">
                                <input type="text" title="Campo Obrigatório" class="form-control" id="rg" name="rg"
                                    placeholder="RG">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-9 mb-3 mb-sm-0">
                                <input type="email" title="Campo Obrigatório" class="form-control" id="email"
                                    name="email" placeholder="E-mail">
                            </div>
                            <div class="col-sm-3">
                                <input type="date" title="Campo Opcional" class="form-control" id="nascimento"
                                    name="nascimento" placeholder="Data Nascimento">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-6 mb-3 mb-sm-0">
                                <input type="text" title="Campo Obrigatório" class="form-control" id="endereco"
                                    name="endereco" placeholder="Endereço">
                            </div>
                            <div class="col-sm-3 mb-3">
                                <input type="text" title="Campo Obrigatório" class="form-control" id="numero"
                                    name="numero" placeholder="Número">
                            </div>
                            <div class="col-sm-3 mb-3">
                                <input type="text" title="Campo Obrigatório" class="form-control" id="cep" name="cep"
                                    placeholder="CEP">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-4 mb-3 mb-sm-0">
                                <input type="text" title="Campo Obrigatório" class="form-control" id="bairro"
                                    name="bairro" placeholder="Bairro">
                            </div>
                            <div class="col-sm-4 mb-3">
                                <input type="text" title="Campo Obrigatório" class="form-control" id="municipio"
                                    name="municipio" placeholder="Municipio">
                            </div>

                            <div class="col-sm-4">
                                <input type="text" title="Campo Obrigatório" class="form-control" id="celular"
                                    name="celular" placeholder="1° Celular">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-4">
                                <input type="text" title="Campo Opcional" class="form-control" id="celular1"
                                    name="celular1" placeholder="2° Celular">
                            </div>
                            <div class="col-sm-4">
                                <input type="text" title="Campo Opcional" class="form-control" id="celular2"
                                    name="celular2" placeholder="3° Celular">
                            </div>
                            <div class="col-sm-4">
                                <input type="text" title="Campo Opcional" class="form-control" id="celular3"
                                    name="celular3" placeholder="4° Celular">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-12 mb-3">
                                <button type="submit" class="btn btn-success  btn-block">Cadastrar</button>
                            </div>
                        </div>
                        <hr>
                    </div>
                    <?php echo form_close() ?>
                </div>
            </div>
        </div>
    </div>
</div>