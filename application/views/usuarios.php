<div class="container-fluid">
<button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#ModalUsuarioAdd">Adicionar Usuário</button>
    <div class="text-center">
        <h1 class="h4 text-gray-900 mb-4">Usuários</h1>
    </div>
    <table id='usuarioLista' class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th scope="col">Nome</th>
                <th scope="col">E-mail</th>
                <th scope="col">Perfill</th>
                <th scope="col">Ações</th>
            </tr>
        </thead>
        <tbody>
            <?php
  foreach ($usuarios as $value) {
    echo '<tr>
    <th scope="row">'.$value['Nome'].'</th>
    <td>'.$value['Email'].'</td>
    <td>'.$value['Perfil'].'</td>
    <td>
    <a title="Visualizar" onclick="janelaVisualizarUsuario('.$value['ID_Pessoa'].')" class="btn btn-outline-primary" href="javascript:;"><i class="fas fa-eye"></i></a>
    <a title="Editar" onclick="janelaEditarUsuario('.$value['ID_Pessoa'].')" class="btn btn-outline-primary" href="javascript:;"><i class="far fa-edit"></i></a>
    </td>
    </tr>';
    }
  ?>
        </tbody>
    </table>
</div>