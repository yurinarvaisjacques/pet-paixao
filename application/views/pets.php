<div class="container-fluid">
  <div class="card o-hidden border-0 shadow-lg my-5">
    <div class="card-body p-0">
      <!-- Nested Row within Card Body -->
      <div class="row">
        <div class="col-lg-12">
          <div class="p-5">
            <div class="text-center">
              <h1 class="h4 text-gray-900 mb-4">Cadastro Pets</h1>
            </div>  
            <?php echo form_open('pets/Cadastrar', array('id' => 'petForm','onsubmit' => 'return false')) ?>
                <div class="form-group row">
                  <div class="col-sm-6 mb-3 mb-sm-0">
                    <input type="text" title="Campo Obrigatório" class="form-control" id="nome" name="nome" placeholder="Nome">
                  </div>
                  <div class="col-sm-6 mb-3 mb-sm-0">
                    <select class="form-control" id="proprietario" name="proprietario" data-live-search="true">
                      <option value="">Selecione o Proprietario</option>
                      <?php
                        foreach ($proprietario as $value) {
                          echo "<option value='".$value['ID_Pessoa']."'>".$value['Nome']."</option>";
                        }
                      ?>
                    </select>
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-sm-3">
                    <select class="form-control" id="porte" name="porte">
                    <option value="">Porte</option>
                    <option value="P">Pequeno</option>
                    <option value="M">Médio</option>
                    <option value="G">Grande</option>
                    </select>
                  </div>
                  <div class="col-sm-3 mb-3 mb-sm-0">
                    <input type="text" title="Campo Obrigatório" class="form-control" id="especie" name="especie" placeholder="Espécie">
                  </div>
                  <div class="col-sm-3 mb-3">
                    <input type="text" title="Campo Opcional" class="form-control" id="idade" name="idade" placeholder="Idade">
                  </div>
                  <div class="col-sm-3">
                  <select  class="form-control" id="pelagem" name="pelagem">
                    <option value="">Pelagem</option>
                    <option value="C">Curto</option>
                    <option value="M">Médio</option>
                    <option value="L">Longo</option>
                    </select>
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-sm-4 mb-3 mb-sm-0">
                    <input type="text" title="Campo Opcional" class="form-control" id="cor" name="cor" placeholder="Cor">
                  </div>
                  <div class="col-sm-4 mb-3">
                    <select  class="form-control" id="castrado" name="castrado">
                    <option value="">Castrado</option>
                    <option value="S">Sim</option>
                    <option value="N">Não</option>
                    </select>
                  </div>
                  <div class="col-sm-4 mb-3 mb-sm-0">
                  <select class="form-control" id="sexo" name="sexo">
                    <option value="">Sexo</option>
                    <option value="F">Femea</option>
                    <option value="M">Macho</option>
                    </select>
                  </div>
                </div>
                <div class="form-group row">
                      <div class="col-sm-12 mb-3">
                      <button type="submit" class="btn btn-success  btn-block">Cadastrar</button>
                      </div>
                </div>
                </div>
                <hr>
              </div>
            <?php echo form_close() ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>