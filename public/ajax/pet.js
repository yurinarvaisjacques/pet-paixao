$(function() {
    $("#petForm").on('submit', function(e) {
        e.preventDefault();
        
        var petForm = $(this);
        
        $.ajax({
            url: petForm.attr('action'),
            type: 'post',
            data: petForm.serialize(),
            success: function(response){
                console.log(response);
                if(response.status == 'success') {
                    alertify.success(response.message);
                    setTimeout(function(){
                        location.reload();
                    },3000);
                }else{
                    alertify.notify(response.message, 'custom', 5, function(){console.log('dismissed');});
                }
            }
        });
    });
});

$(function() {
    $("#petFormAtualizar").on('submit', function(e) {
        e.preventDefault();
        
        var petFormAtualizar = $(this);
        
        $.ajax({
            url: petFormAtualizar.attr('action'),
            type: 'post',
            data: petFormAtualizar.serialize(),
            success: function(response){
                console.log(response);
                if(response.status == 'success') {
                    alertify.success(response.message);
                    setTimeout(function(){
                        location.reload();
                    },3000);
                }else{
                    alertify.notify(response.message, 'custom', 5, function(){console.log('dismissed');});
                }
            }
        });
    });
});


$(document).ready(function() {
    $('#petLista').DataTable({
        "language": {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "search":         "Filtrar na tabela:",
            "zeroRecords":    "Nenhum registro encontrado",
            "info": "Mostrando _PAGE_ de _PAGES_ páginas de _MAX_ registros",
            "infoEmpty": "Mostrando 0 registros",
            "infoFiltered": "(Filtrado do total de _MAX_ registros)",
            "paginate": {
                "next":       "Próxima",
                "previous":   "Anterior"
            },
            "aria": {
                "sortAscending":  ": activate to sort column ascending",
                "sortDescending": ": activate to sort column descending"
            }
        }
    });
});

function visualizaDadosPetJSon(ID_Pet){   
    $.post(baseUrl+'pets/json_visualizarpet/'+ID_Pet, function(response){
        $('#nome').val(response.Nome);
        $('#porte').val(response.Porte).change();
        $('#especie').val(response.Especie);
        $('#idade').val(response.Idade);
        $('#pelagem').val(response.Pelagem).change();
        $('#cor').val(response.Cor);
        $('#castrado').val(response.Castrado).change();
        $('#sexo').val(response.Sexo).change();
    },'json');
}

function janelaVisualizarPet(ID_Pet){
    visualizaDadosPetJSon(ID_Pet);
    $('#ModalPetInfo').modal('show');
}

function editaDadosPetJSon(ID_Pet){   
    $.post(baseUrl+'pets/json_visualizarpet/'+ID_Pet, function(response){
        $('#nome1').val(response.Nome);
        $('#idpet').val(response.ID_Pet);
        $('#porte1').val(response.Porte).change();
        $('#especie1').val(response.Especie);
        $('#idade1').val(response.Idade);
        $('#pelagem1').val(response.Pelagem).change();
        $('#cor1').val(response.Cor);
        $('#castrado1').val(response.Castrado).change();
        $('#sexo1').val(response.Sexo).change();
    },'json');
}

function janelaEditarPet(ID_Pet){
editaDadosPetJSon(ID_Pet);
$('#ModalPetEdit').modal('show');
}
