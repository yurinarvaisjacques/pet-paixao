$(function() {
    $("#FormRegistrarAtendimento").on('submit', function(e) {
        e.preventDefault();
        
        var FormRegistrarAtendimento = $(this);
        
        $.ajax({
            url: FormRegistrarAtendimento.attr('action'),
            type: 'post',
            data: FormRegistrarAtendimento.serialize(),
            success: function(response){
                console.log(response);
                if(response.status == 'success') {
                    alertify.success(response.message);
                    setTimeout(function(){
                        location.reload();
                    },1000);
                }else{
                    alertify.notify(response.message, 'custom', 5, function(){console.log('dismissed');});
                }
            }
        });
    });
});


$(document).ready(function() {
    $('#vendasLista').DataTable({
        "language": {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "search":         "Filtrar na tabela:",
            "zeroRecords":    "Nenhum registro encontrado",
            "info": "Mostrando _PAGE_ de _PAGES_ páginas de _MAX_ registros",
            "infoEmpty": "Mostrando 0 registros",
            "infoFiltered": "(Filtrado do total de _MAX_ registros)",
            "paginate": {
                "next":       "Próxima",
                "previous":   "Anterior"
            },
            "aria": {
                "sortAscending":  ": activate to sort column ascending",
                "sortDescending": ": activate to sort column descending"
            }
        }
    });
});

function janelaConfirmarPagamento(ID_Vendas){

    alertify.confirm('Confirmar Pagamento', 'Você tem certeza que deseja confirmar pagamento', 
        function(){ 
            $.post(baseUrl+'financeiro/json_ConfirmarPagamento/'+ID_Vendas, function(response){
                if(response.status == 'success') {
                    alertify.success(response.message);
                    setTimeout(function(){
                        location.reload();
                    },3000);
                }
            },'json'); 
        }, 
        function(){ 
            alertify.error('Status de pagamento não foi atualizado', 'custom', 5, function(){console.log('dismissed');});
        });

    
}