$(function() {
    $("#fornecedorForm").on('submit', function(e) {
        e.preventDefault();
        
        var fornecedorForm = $(this);
        
        $.ajax({
            url: fornecedorForm.attr('action'),
            type: 'post',
            data: fornecedorForm.serialize(),
            success: function(response){
                console.log(response);
                if(response.status == 'success') {
                    alertify.success(response.message);
                    setTimeout(function(){
                        location.reload();
                    },3000);
                }else{
                    alertify.notify(response.message, 'custom', 5, function(){console.log('dismissed');});
                }
            }
        });
    });
});

$(function() {
    $("#fornecedorFormAtualizar").on('submit', function(e) {
        e.preventDefault();
        
        var fornecedorFormAtualizar = $(this);
        
        $.ajax({
            url: fornecedorFormAtualizar.attr('action'),
            type: 'post',
            data: fornecedorFormAtualizar.serialize(),
            success: function(response){
                console.log(response);
                if(response.status == 'success') {
                    alertify.success(response.message);
                    setTimeout(function(){
                        location.reload();
                    },3000);
                }else{
                    alertify.notify(response.message, 'custom', 5, function(){console.log('dismissed');});
                }
            }
        });
    });
});


$(document).ready(function() {
    $('#fornecedorLista').DataTable({
        "language": {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "search":         "Filtrar na tabela:",
            "zeroRecords":    "Nenhum registro encontrado",
            "info": "Mostrando _PAGE_ de _PAGES_ páginas de _MAX_ registros",
            "infoEmpty": "Mostrando 0 registros",
            "infoFiltered": "(Filtrado do total de _MAX_ registros)",
            "paginate": {
                "next":       "Próxima",
                "previous":   "Anterior"
            },
            "aria": {
                "sortAscending":  ": activate to sort column ascending",
                "sortDescending": ": activate to sort column descending"
            }
        }
    });
});

function visualizaDadosFornecedorJSon(ID_Fornecedor){   
    $.post(baseUrl+'fornecedores/json_visualizarfornecedor/'+ID_Fornecedor, function(response){
        $('#nomefantasia').val(response.NomeFantasia);
        $('#razaosocial').val(response.RazaoSocial);
        $('#cnpj').val(response.CNPJ);
        $('#inscricaoestadual').val(response.InscricaoEstadual);
        $('#email').val(response.Email);
        $('#rua').val(response.Rua);
        $('#numero').val(response.Numero);
        $('#bairro').val(response.Bairro);
        $('#cidade').val(response.Cidade);
        $('#cep').val(response.CEP);
        $('#celular1').val(response.Celular1);
        $('#celular2').val(response.Celular2);
        $('#celular3').val(response.Celular3);
        $('#celular4').val(response.Celular4);
        $('#representante').val(response.Representante);
        $('#celularrepresentante').val(response.Telefone);
        $('#emailrepresentante').val(response.EmailRepresentante);
    },'json');
}

function janelaVisualizarFornecedor(ID_Fornecedor){
    visualizaDadosFornecedorJSon(ID_Fornecedor);
    $('#ModalFornecedorInfo').modal('show');
}

function editaDadosFornecedorJSon(ID_Fornecedor){   
    $.post(baseUrl+'fornecedores/json_visualizarfornecedor/'+ID_Fornecedor, function(response){
        $('#nomefantasia1').val(response.NomeFantasia);
        $('#razaosocial1').val(response.RazaoSocial);
        $('#cnpj1').val(response.CNPJ);
        $('#inscricaoestadual1').val(response.InscricaoEstadual);
        $('#email1').val(response.Email);
        $('#rua1').val(response.Rua);
        $('#numero1').val(response.Numero);
        $('#bairro1').val(response.Bairro);
        $('#cidade1').val(response.Cidade);
        $('#cep1').val(response.CEP);
        $('#celular11').val(response.Celular1);
        $('#celular21').val(response.Celular2);
        $('#celular31').val(response.Celular3);
        $('#celular41').val(response.Celular4);
        $('#representante1').val(response.Representante);
        $('#celularrepresentante1').val(response.Telefone);
        $('#emailrepresentante1').val(response.EmailRepresentante);
    },'json');
}

function janelaEditarFornecedor(ID_Fornecedor){
    editaDadosFornecedorJSon(ID_Fornecedor);
    $('#ModalFornecedorEditar').modal('show');
}