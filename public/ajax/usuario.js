$(function() {
    $("#usuaruioForm").on('submit', function(e) {
        e.preventDefault();
        
        var usuaruioForm = $(this);
        
        $.ajax({
            url: usuaruioForm.attr('action'),
            type: 'post',
            data: usuaruioForm.serialize(),
            success: function(response){
                console.log(response);
                if(response.status == 'success') {
                    alertify.success(response.message);
                    setTimeout(function(){
                        location.reload();
                    },1000);
                }else{
                    alertify.notify(response.message, 'custom', 5, function(){console.log('dismissed');});
                }
            }
        });
    });
});

$(function() {
    $("#usuarioFormAtualizar").on('submit', function(e) {
        e.preventDefault();
        
        var usuarioFormAtualizar = $(this);
        
        $.ajax({
            url: usuarioFormAtualizar.attr('action'),
            type: 'post',
            data: usuarioFormAtualizar.serialize(),
            success: function(response){
                console.log(response);
                if(response.status == 'success') {
                    alertify.success(response.message);
                    setTimeout(function(){
                        location.reload();
                    },3000);
                }else{
                    alertify.notify(response.message, 'custom', 5, function(){console.log('dismissed');});
                }
            }
        });
    });
});

$(document).ready(function() {
    $('#usuarioLista').DataTable({
        "language": {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "search":         "Filtrar na tabela:",
            "zeroRecords":    "Nenhum registro encontrado",
            "info": "Mostrando _PAGE_ de _PAGES_ páginas de _MAX_ registros",
            "infoEmpty": "Mostrando 0 registros",
            "infoFiltered": "(Filtrado do total de _MAX_ registros)",
            "paginate": {
                "next":       "Próxima",
                "previous":   "Anterior"
            },
            "aria": {
                "sortAscending":  ": activate to sort column ascending",
                "sortDescending": ": activate to sort column descending"
            }
        }
    });
});

function visualizaDadosUsuarioJSon(ID_Pessoa){   
        $.post(baseUrl+'usuarios/json_visualizarusuario/'+ID_Pessoa, function(response){
            $('#nome').val(response.Nome);
            $('#cpf').val(response.CPF);
            $('#rg').val(response.RG);
            $('#email').val(response.Email);
            $('#nascimento').val(response.DataNascimento);
            $('#rua').val(response.Rua);
            $('#numero').val(response.Numero);
            $('#bairro').val(response.Bairro);
            $('#cidade').val(response.Cidade);
            $('#cep').val(response.CEP);
            $('#celular1').val(response.Celular1);
            $('#celular2').val(response.Celular2);
            $('#celular3').val(response.Celular3);
            $('#celular4').val(response.Celular4);
            $('#login').val(response.Login);
            $('#senha').val(response.Senha);
            $('#perfil').val(response.Perfil).change();
        },'json');
}

function janelaVisualizarUsuario(ID_Pessoa){
    visualizaDadosUsuarioJSon(ID_Pessoa);
    $('#ModalUsuarioInfo').modal('show');
}

function editaDadosUsuarioJSon(ID_Pessoa){   
    $.post(baseUrl+'usuarios/json_visualizarusuario/'+ID_Pessoa, function(response){
        $('#nome1').val(response.Nome);
        $('#cpf1').val(response.CPF);
        $('#rg1').val(response.RG);
        $('#email1').val(response.Email);
        $('#nascimento1').val(response.DataNascimento);
        $('#rua1').val(response.Rua);
        $('#numero1').val(response.Numero);
        $('#bairro1').val(response.Bairro);
        $('#cidade1').val(response.Cidade);
        $('#cep1').val(response.CEP);
        $('#celular11').val(response.Celular1);
        $('#celular21').val(response.Celular2);
        $('#celular31').val(response.Celular3);
        $('#celular41').val(response.Celular4);
        $('#login1').val(response.Login);
        $('#senha1').val(response.Senha);
        $('#perfil1').val(response.Perfil).change();
    },'json');
}

function janelaEditarUsuario(ID_Pessoa){
    editaDadosUsuarioJSon(ID_Pessoa);
    $('#ModalUsuarioEditar').modal('show');
}

