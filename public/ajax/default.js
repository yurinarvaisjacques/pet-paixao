$(function() {
    $("#loginForm").on('submit', function(e) {
        e.preventDefault();
        
        var loginForm = $(this);
        
        $.ajax({
            url: loginForm.attr('action'),
            type: 'post',
            data: loginForm.serialize(),
            success: function(response){
                console.log(response);
                if(response.status == 'success') {
                    alertify.success(response.message);
                    setTimeout(function(){
                        window.location.href = 'main';
                    },1000);
                }else{
                    alertify.error(response.message, 'custom', 5, function(){console.log('dismissed');});
                }
            }
        });
    });
});

$(function () {
    $('select').selectpicker();
});