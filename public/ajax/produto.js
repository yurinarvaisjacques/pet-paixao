$(function() {
    $("#fornecedorForm").on('submit', function(e) {
        e.preventDefault();
        
        var fornecedorForm = $(this);
        
        $.ajax({
            url: fornecedorForm.attr('action'),
            type: 'post',
            data: fornecedorForm.serialize(),
            success: function(response){
                console.log(response);
                if(response.status == 'success') {
                    alertify.success(response.message);
                    setTimeout(function(){
                        location.reload();
                    },3000);
                }else{
                    alertify.notify(response.message, 'custom', 5, function(){console.log('dismissed');});
                }
            }
        });
    });
});

$(function() {
    $("#fornecedorFormAtualizar").on('submit', function(e) {
        e.preventDefault();
        
        var fornecedorFormAtualizar = $(this);
        
        $.ajax({
            url: fornecedorFormAtualizar.attr('action'),
            type: 'post',
            data: fornecedorFormAtualizar.serialize(),
            success: function(response){
                console.log(response);
                if(response.status == 'success') {
                    alertify.success(response.message);
                    setTimeout(function(){
                        location.reload();
                    },3000);
                }else{
                    alertify.notify(response.message, 'custom', 5, function(){console.log('dismissed');});
                }
            }
        });
    });
});


$(document).ready(function() {
    $('#produtoLista').DataTable({
        "language": {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "search":         "Filtrar na tabela:",
            "zeroRecords":    "Nenhum registro encontrado",
            "info": "Mostrando _PAGE_ de _PAGES_ páginas de _MAX_ registros",
            "infoEmpty": "Mostrando 0 registros",
            "infoFiltered": "(Filtrado do total de _MAX_ registros)",
            "paginate": {
                "next":       "Próxima",
                "previous":   "Anterior"
            },
            "aria": {
                "sortAscending":  ": activate to sort column ascending",
                "sortDescending": ": activate to sort column descending"
            }
        }
    });
});

function calcular() {
    var valor1 = parseFloat(document.getElementById('quantidade').value);
    var valor2 = parseFloat(document.getElementById('valorunitario').value);
    document.getElementById('valortotal').value = valor1 * valor2;
}