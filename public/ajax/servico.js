$(function() {
    $("#servicoForm").on('submit', function(e) {
        e.preventDefault();
        
        var servicoForm = $(this);
        
        $.ajax({
            url: servicoForm.attr('action'),
            type: 'post',
            data: servicoForm.serialize(),
            success: function(response){
                console.log(response);
                if(response.status == 'success') {
                    alertify.success(response.message);
                    setTimeout(function(){
                        location.reload();
                    },3000);
                }else{
                    alertify.notify(response.message, 'custom', 5, function(){console.log('dismissed');});
                }
            }
        });
    });
});

$(function() {
    $("#servicoFormAtualizar").on('submit', function(e) {
        e.preventDefault();
        
        var servicoFormAtualizar = $(this);
        
        $.ajax({
            url: servicoFormAtualizar.attr('action'),
            type: 'post',
            data: servicoFormAtualizar.serialize(),
            success: function(response){
                console.log(response);
                if(response.status == 'success') {
                    alertify.success(response.message);
                    setTimeout(function(){
                        location.reload();
                    },3000);
                }else{
                    alertify.notify(response.message, 'custom', 5, function(){console.log('dismissed');});
                }
            }
        });
    });
});

$(document).ready(function() {
    $('#servicoLista').DataTable({
        "language": {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "search":         "Filtrar na tabela:",
            "zeroRecords":    "Nenhum registro encontrado",
            "info": "Mostrando _PAGE_ de _PAGES_ páginas de _MAX_ registros",
            "infoEmpty": "Mostrando 0 registros",
            "infoFiltered": "(Filtrado do total de _MAX_ registros)",
            "paginate": {
                "next":       "Próxima",
                "previous":   "Anterior"
            },
            "aria": {
                "sortAscending":  ": activate to sort column ascending",
                "sortDescending": ": activate to sort column descending"
            }
        }
    });
});

function visualizaDadosServicoJSon(ID_Servico){   
    $.post(baseUrl+'servicos/json_visualizarservico/'+ID_Servico, function(response){
        $('#nome1').val(response.Nome);
        $('#descricao1').val(response.Descricao);
        $('#valor1').val(response.Valor);
        $('#tipo_servico1').val(response.Tipo_Servico).change();;
    },'json');
}

function janelaVisualizarServico(ID_Servico){
    visualizaDadosServicoJSon(ID_Servico);
    $('#ModalServicoInfo').modal('show');
}

function editaDadosServicoJSon(ID_Servico){   
    $.post(baseUrl+'servicos/json_visualizarservico/'+ID_Servico, function(response){
        $('#idservico').val(ID_Servico);
        $('#nome').val(response.Nome);
        $('#descricao').val(response.Descricao);
        $('#valor').val(response.Valor);
        $('#tipo_servico').val(response.Tipo_Servico).change();;
    },'json');
}

function janelaEditarServico(ID_Servico){
    editaDadosServicoJSon(ID_Servico);
    $('#ModalServicoEditar').modal('show');
}