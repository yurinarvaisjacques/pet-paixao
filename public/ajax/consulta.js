$(function() {
    $("#consultaForm").on('submit', function(e) {
        e.preventDefault();
        
        var consultaForm = $(this);
        
        $.ajax({
            url: consultaForm.attr('action'),
            type: 'post',
            data: consultaForm.serialize(),
            success: function(response){
                console.log(response);
                if(response.status == 'success') {
                    alertify.success(response.message);
                    setTimeout(function(){
                        location.reload();
                    },3000);
                }else{
                    alertify.notify(response.message, 'custom', 5, function(){console.log('dismissed');});
                }
            }
        });
    });
});

$('#calendario').datepicker({
    format: 'dd/mm/yyyy',
    language: 'pt-BR',
    startDate: '+0d',
    daysOfWeekDisabled: "0",
    todayHighlight: true,
});



$(document).ready(function() {
    $('#consultaLista').DataTable({
        "language": {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "search":         "Filtrar na tabela:",
            "zeroRecords":    "Nenhum registro encontrado",
            "info": "Mostrando _PAGE_ de _PAGES_ páginas de _MAX_ registros",
            "infoEmpty": "Mostrando 0 registros",
            "infoFiltered": "(Filtrado do total de _MAX_ registros)",
            "paginate": {
                "next":       "Próxima",
                "previous":   "Anterior"
            },
            "aria": {
                "sortAscending":  ": activate to sort column ascending",
                "sortDescending": ": activate to sort column descending"
            }
        }
    });
});

function visualizaDadosConsultaJSon(ID_Atendimento){   
    $.post(baseUrl+'consultas/json_visualizarAtendimento/'+ID_Atendimento, function(response){
        $('#servico').val(response.Servico);
        $('#pet').val(response.Pet);
        $('#datahora').val(response.horario_atendimento);
        $('#proprietarionome').val(response.Proprietario);
        $('#telefone').val(response.Telefone);
        $('#valor').val(response.Valor);
        $('#obs').val(response.Obs);
    },'json');
}

function janelaVisualizarAtendimento(ID_Atendimento){
visualizaDadosConsultaJSon(ID_Atendimento);
$('#ModalConsultaInfo').modal('show');
}

function RegistraAtendimentoJSon(ID_Atendimento){   
    $.post(baseUrl+'consultas/json_visualizarAtendimento/'+ID_Atendimento, function(response){
        $('#atendimento').val(response.Atendimento);
        $('#idpessoa').val(response.ID_Pessoa);
        $('#valor_total').val(response.Valor);
        $('#nome_pessoa').val(response.Proprietario);
    },'json');
}

function janelaRegistrarAtendimento(ID_Atendimento){
    RegistraAtendimentoJSon(ID_Atendimento);
    $('#ModalRegistrarAtendimento').modal('show');
    }

function janelaInativarAtendimento(ID_Atendimento){

    alertify.confirm('Remover Atendimento', 'Você tem certeza que deseja remover o atentimento', 
        function(){ 
            $.post(baseUrl+'consultas/json_inativarAtendimento/'+ID_Atendimento, function(response){
                if(response.status == 'success') {
                    alertify.success(response.message);
                    setTimeout(function(){
                        location.reload();
                    },3000);
                }
            },'json'); 
        }, 
        function(){ 
            alertify.error('Atendimento não foi removido', 'custom', 5, function(){console.log('dismissed');});
        });

    
}

function calcular() {
    var valor1 = parseFloat(document.getElementById('valor_total').value);
    var valor2 = parseFloat(document.getElementById('numero_parcelas').value);
    document.getElementById('valor_parcela').value = valor1 / valor2;
}